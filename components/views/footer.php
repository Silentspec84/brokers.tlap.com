<?php
use app\assets\AppAsset;
AppAsset::register($this);
?>

<footer>
    <div class="bg-dark pt-2 pb-2">
        <div class="row mt-2">
            <div class="col-3">
                <a class="nav-link text-gray" style="color:#ffffff;" href="/"><h5 class="text-left m-0 pt-2 font-weight-bold">Брокеры tlap.com</h5></a>
                <ul class="nav footer-nav">
                    <li class="nav-item">

                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://vk.com/public29468299"><i class="fab fa-vk"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://www.instagram.com/tradelikeaproru/"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://www.facebook.com/tradelikeapro.ru"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://twitter.com/pavlus777"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://tele.click/tradelikeaproru"><i class="fab fa-telegram"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://ok.ru/tradelikeapro"><i class="fab fa-odnoklassniki"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="btn-circle" href="https://www.youtube.com/channel/UCSl8w0FU3RXCuJt-8XxXYlQ"><i class="fab fa-youtube"></i></a>
                    </li>

                </ul>
            </div>
            <div class="col-3">
                <ul class="nav flex-column">
                    <li class="nav-item"><a style="color:#ffffff;" href="/">Главная</a></li>
                    <li class="nav-item"><a style="color:#ffffff;" href="/info/info">О сервисе</a></li>
                    <li class="nav-item"><a style="color:#ffffff;" href="/info/faq">FAQ</a></li>

                </ul>
            </div>
            <div class="col-3">
                <ul class="nav flex-column">
                    <li><a style="color:#ffffff;" href="http://tlap.com/">Блог</a></li>
                    <li><a style="color:#ffffff;" href="http://tlap.com/forum/">Форум</a></li>
                    <li><a style="color:#ffffff;" href="/info/support">Поддержка</a></li>
                </ul>
            </div>
            <div class="col-3">
                <ul class="nav flex-column">

                </ul>
            </div>
        </div>
    </div>
</footer>