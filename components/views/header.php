<?php

use app\models\User;
use yii\bootstrap\ActiveForm;

$user = Yii::$app->user->identity;

/** @var User $user */
?>

<div class="navbar-custom shadow-sm" xmlns:v-on="http://www.w3.org/1999/xhtml">

    <!-- LOGO -->
    <div class="logo-box mt-2 ml-4">
        <a class="nav-link text-gray" style="color:#ffffff;" href="/"><h5 class="text-left m-0 pt-2 font-weight-bold">Брокеры tlap.com</h5></a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <a target="_blank" href="http://tlap.com/" class="nav-link">Блог</a>
        </li>
        <li>
            <a target="_blank" href="http://tlap.com/forum/" class="nav-link">Форум</a>
        </li>
        <li class="dropdown d-none d-lg-block">
            <a class="nav-link dropdown-toggle <?php if(Yii::$app->controller->route === 'brokers'):?>active-link<?php endif;?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                Брокеры форекс
                <i class="mdi mdi-chevron-down"></i>
            </a>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 70px, 0px);">
                <a class="dropdown-item<?php if(Yii::$app->controller->id === 'all'):?>active<?php endif;?>" href="/brokers/all">Рейтинг форекс брокеров</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'compare'):?> active<?php endif;?>" href="/brokers/compare">Сравнение форекс брокеров</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'reviews'):?> active<?php endif;?>" href="/brokers/reviews">Отзывы о форекс брокерах</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'test'):?> active<?php endif;?>" href="/brokers/test">Пройти тест для подбора брокера</a>
            </div>
        </li>
        <li class="dropdown d-none d-lg-block">
            <a class="nav-link dropdown-toggle <?php if(Yii::$app->controller->id === 'info'):?>active-link<?php endif;?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                Информация
                <i class="mdi mdi-chevron-down"></i>
            </a>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 70px, 0px);">

                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'info'):?>active<?php endif;?>" href="/info/info">О сервисе</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'faq'):?> active<?php endif;?>" href="/info/faq">F.A.Q.</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->route === 'support'):?>active<?php endif;?>" href="/info/support">Техподдержка</a>
                <div class="dropdown-divider"></div>
                <a target="_blank" class="dropdown-item" href="http://tlap.com/">Блог</a>
                <a target="_blank" class="dropdown-item" href="http://tlap.com/forum/">Форум</a>
            </div>
        </li>
    </ul>
    <?php if (!Yii::$app->user->isGuest): ?>
        <?php if ($user->canAccessAdmin()): ?>
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <a href="/admin" class="nav-link<?php if(Yii::$app->controller->route === '/admin'):?> active-link<?php endif;?>">Админка</a>
                </li>
            </ul>
        <?php endif?>

        <?php if ($user->canWriteBlogs()): ?>
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <a href="/blog-author" class="nav-link<?php if(Yii::$app->controller->route === '/blog-author'):?> active-link<?php endif;?>">Опубликовать новость</a>
                </li>
            </ul>
        <?php endif?>
    <?php endif?>

    <?php if (Yii::$app->user->isGuest): ?>
        <div class="list-unstyled topnav-menu float-right">
            <li>
                <a class="nav-link<?php if(Yii::$app->requestedAction->id === 'login'):?> active-link<?php endif;?>" href="/user/security/login">Вход</a>
            </li>
            <li>
                <a class="nav-link<?php if(Yii::$app->requestedAction->id === 'register'):?> active-link<?php endif;?>" href="/user/registration/register">Регистрация</a>
            </li>
        </div>
    <?php else : ?>
    <div class="list-unstyled topnav-menu float-right mb-0">
        <li class="dropdown d-none d-lg-block">
            <div class="dropdown">
                <a class="nav-link text-light dropdown-toggle d-flex align-items-center" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="/img/user-icon.png" class="rounded-circle" style="width: 25px;" alt="">
                    <span class="pro-user-name ml-1"><?= $user ? $user->username : ''?></span>
                    <i class="mdi mdi-chevron-down ml-1"></i>
                </a>
                <div class="user-menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <div class="user-menu__section">
                        <div class="user-menu__account text-center">
                            <img src="/img/user-icon.png" style="width: 50px;" class="user-avatar rounded-circle" alt="">
                            <div><?= $user ? $user->username : ''?></div>
                            <div class="text-secondary small">
                                <?= $user ? $user->email : ''?>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="user-menu__section text-center">
                        <a class="dropdown-item" href="/user/settings/profile">Кабинет</a>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="user-menu__section text-center">
                        <?php $form = ActiveForm::begin([
                            'id'                     => 'logout-form',
                            'enableAjaxValidation'   => true,
                            'enableClientValidation' => false,
                            'action' => Yii::$app->urlManager->createUrl('/user/security/logout'),
                        ]); ?>
        <li class="dropdown-item">
            <button class="btn btn-link dropdown-item">Выход</button>
        </li>
        <?php ActiveForm::end(); ?>
    </div>
</div>
    </div>
    </li>
    </div>
<?php endif?>
</div>