<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property string $title
 * @property int $date_created
 * @property int $date_modified
 */
class MobileDevice extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['date_created', 'date_modified'], 'integer'],
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Название устройства',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}