<?php

namespace app\models;

use dektrium\user\Finder;
use dektrium\user\Mailer;
use dektrium\user\models\Token;
use yii\base\Model;
use dektrium\user\models\RecoveryForm as BaseRecoveryForm;

/**
 * Model for collecting data on password recovery.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RecoveryForm extends BaseRecoveryForm
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => \Yii::t('user', 'Email'),
            'password' => \Yii::t('user', 'Пароль'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'emailTrim' => ['email', 'trim'],
            'emailRequired' => ['email', 'required', 'message' => 'Поле email не должно быть пустым'],
            'emailPattern' => ['email', 'email', 'message' => 'Введите корректный email'],
            'passwordRequired' => ['password', 'required', 'message' => 'Поле пароль не должно быть пустым'],
            'passwordLength' => ['password', 'string', 'max' => 72, 'min' => 6],
        ];
    }

    /**
     * Sends recovery message.
     *
     * @return bool
     */
    public function sendRecoveryMessage()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->finder->findUserByEmail($this->email);

        if ($user instanceof User) {
            /** @var Token $token */
            $token = \Yii::createObject([
                'class' => Token::className(),
                'user_id' => $user->id,
                'type' => Token::TYPE_RECOVERY,
            ]);

            if (!$token->save(false)) {
                return false;
            }

            if (!$this->mailer->sendRecoveryMessage($user, $token)) {
                return false;
            }
        }

        \Yii::$app->session->setFlash(
            'info',
            'Письмо со ссылкой на восстановление пароля было выслано на Email, указанный при регистрации'
        );

        return true;
    }

    /**
     * Resets user's password.
     *
     * @param Token $token
     *
     * @return bool
     */
    public function resetPassword(Token $token)
    {
        if (!$this->validate() || $token->user === null) {
            return false;
        }

        if ($token->user->resetPassword($this->password)) {
            \Yii::$app->session->setFlash('success', 'Ваш пароль был успешно изменен');
            $token->delete();
        } else {
            \Yii::$app->session->setFlash(
                'danger',
                'Возникла ошибка и ваш пароль не был изменен. Пожалуйста, попробуйте позже.'
            );
        }

        return true;
    }
}
