<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property integer $broker_id
 * @property integer $lang_id
 * @property int $date_created
 * @property int $date_modified
 */
class BrokerSiteLang extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers_site_langs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'lang_id'], 'required'],
            [['date_created', 'date_modified', 'broker_id', 'lang_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'broker_id' => 'id брокера',
            'lang_id' => 'id языка',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}