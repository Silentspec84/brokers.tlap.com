<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property string $symbol
 * @property int $date_created
 * @property int $date_modified
 */
class Currency extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'iso', 'symbol'], 'required'],
            [['date_created', 'date_modified'], 'integer'],
            [['title', 'iso', 'symbol'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Название валюты',
            'iso' => 'Код валюты',
            'symbol' => 'Символ валюты',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}