<?php

namespace app\models;
use dektrium\user\models\User as BaseUser;
use yii\web\IdentityInterface;

class User extends BaseUser  implements IdentityInterface
{
    public $settings = null;

    public function attributeLabels()
    {
        return [
            'username'          => 'Имя пользователя',
            'email'             => 'Email',
            'registration_ip'   => 'Registration ip',
            'unconfirmed_email' => 'Новый email',
            'password'          => 'Пароль',
            'created_at'        => 'Время регистрации',
            'last_login_at'     => 'Последний вход',
            'confirmed_at'      => 'Время подтверждения',
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            // username rules
            'usernameTrim'     => ['username', 'trim'],
            'usernameRequired' => ['username', 'required', 'on' => ['register', 'create', 'connect', 'update']],
            'usernameMatch'    => ['username', 'match', 'pattern' => static::$usernameRegexp],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernameUnique'   => [
                'username',
                'unique',
                'message' => 'Такое имя пользователя уже есть'
            ],

            // email rules
            'emailTrim'     => ['email', 'trim'],
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern'  => ['email', 'email'],
            'emailLength'   => ['email', 'string', 'max' => 255],
            'emailUnique'   => [
                'email',
                'unique',
                'message' => 'Такой адрес email уже используется'
            ],

            // password rules
            'passwordRequired' => ['password', 'required', 'on' => ['register']],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72, 'on' => ['register', 'create']],
        ];
    }

    public function getSettings()
    {
        if (!$this->settings) {
            $this->settings = UserSettings::find()->where(['user_id' => $this->id])->one();
        }
        return $this->settings;
    }

    public function getRole()
    {
        return $this->getSettings()->role;
    }

    public function isAdmin()
    {
        return $this->getSettings()->role === UserSettings::ROLE_ADMIN;
    }

    public function isModer()
    {
        return $this->getSettings()->role === UserSettings::ROLE_ADMIN;
    }

    public function isBroker()
    {
        return $this->getSettings()->role === UserSettings::ROLE_ADMIN;
    }

    public function canAccessAdmin()
    {
        return $this->getSettings()->role === UserSettings::ROLE_ADMIN
            || $this->getSettings()->role === UserSettings::ROLE_MODERATOR;
    }

    public function canWriteBlogs()
    {
        return $this->getSettings()->role === UserSettings::ROLE_ADMIN
            || $this->getSettings()->role === UserSettings::ROLE_MODERATOR
            || $this->getSettings()->role === UserSettings::ROLE_BROKER;
    }
}
