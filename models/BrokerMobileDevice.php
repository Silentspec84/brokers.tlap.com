<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property integer $broker_id
 * @property integer $mobile_device_id
 * @property int $date_created
 * @property int $date_modified
 */
class BrokerMobileDevice extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers_mobile_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'mobile_device_id'], 'required'],
            [['date_created', 'date_modified', 'broker_id', 'mobile_device_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'broker_id' => 'id брокера',
            'mobile_device_id' => 'id мобильного устройства',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}