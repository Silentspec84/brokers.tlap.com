<?php

namespace app\models;
use dektrium\user\helpers\Password;
use dektrium\user\models\LoginForm as BaseLoginForm;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends BaseLoginForm
{
    public function attributeLabels()
    {
        return [
            'login'      => Yii::t('user', 'Логин'),
            'password'   => Yii::t('user', 'Пароль'),
            'rememberMe' => Yii::t('user', 'Запомнить меня'),
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        $rules = [
            'loginTrim' => ['login', 'trim'],
            'requiredFields' => [['login'], 'required'],
            'confirmationValidate' => [
                'login',
                function ($attribute) {
                    if ($this->user !== null) {
                        $confirmationRequired = $this->module->enableConfirmation
                            && !$this->module->enableUnconfirmedLogin;
                        if ($confirmationRequired && !$this->user->getIsConfirmed()) {
                            $this->addError($attribute, 'Сначала вы должны подтвердить свой Email адрес');
                        }
                        if ($this->user->getIsBlocked()) {
                            $this->addError($attribute, 'Ваш аккаунт был заблокирован');
                        }
                    }
                }
            ],
            'rememberMe' => ['rememberMe', 'boolean'],
        ];

        if (!$this->module->debug) {
            $rules = array_merge($rules, [
                'requiredFields' => [['login', 'password'], 'required'],
                'passwordValidate' => [
                    'password',
                    function ($attribute) {
                        if ($this->user === null || !Password::validate($this->password, $this->user->password_hash)) {
                            $this->addError($attribute, 'Неверный логин или пароль');
                        }
                    }
                ]
            ]);
        }

        return $rules;
    }
}
