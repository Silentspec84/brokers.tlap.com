<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property integer $broker_id
 * @property integer $investing_type_id
 * @property int $date_created
 * @property int $date_modified
 */
class BrokerInvestingType extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers_investing_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'investing_type_id'], 'required'],
            [['date_created', 'date_modified', 'broker_id', 'investing_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'broker_id' => 'id брокера',
            'investing_type_id' => 'id типа инвест счетов',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}