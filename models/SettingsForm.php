<?php
/**
 * Created by PhpStorm.
 * User: Silentspec
 * Date: 08.03.2019
 * Time: 20:01
 */

namespace app\models;

use dektrium\user\models\SettingsForm as BaseSettingsForm;

class SettingsForm extends BaseSettingsForm
{
    public function attributeLabels()
    {
        return [
            'email'            => 'Email',
            'username'         => 'Логин',
            'new_password'     => 'Новый пароль',
            'current_password' => 'Текущий пароль',
        ];
    }
}