<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property string $title
 * @property string $name_iso
 * @property string $flag_img
 * @property int $date_created
 * @property int $date_modified
 */
class Country extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'name_iso', 'flag_img'], 'required'],
            [['date_created', 'date_modified'], 'integer'],
            [['title', 'name_iso', 'flag_img'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Название',
            'name_iso' => 'Международный код страны',
            'flag_img' => 'Лого флага',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}