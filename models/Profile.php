<?php

namespace app\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{
    public function attributeLabels()
    {
        return [
            'name'           => 'Имя',
            'public_email'   => 'Email',
            'gravatar_email' => 'Gravatar email',
            'location'       => 'Страна',
            'website'        => 'Вебсайт',
            'bio'            => 'Описание',
            'timezone'       => 'Временная зона',
        ];
    }
}