<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель брокеров.
 *
 * @property integer $id
 * @property string $title
 * @property string $creation_year
 * @property string $site
 * @property int $broker_price_type_id
 * @property string $broker_logo
 * @property string $partners_program
 * @property string $description
 * @property string $address
 * @property string $phone
 * @property string $docs_link
 * @property string $forum_link
 * @property int $date_created
 * @property int $date_modified
 */
class Broker extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'creation_year', 'site', 'broker_logo', 'partners_program', 'description',
                'address', 'phone', 'docs_link', 'forum_link'], 'required'],
            [['broker_price_type_id', 'date_created', 'date_modified'], 'integer'],
            [['title', 'creation_year', 'site', 'broker_logo', 'partners_program', 'description',
                'address', 'phone', 'docs_link', 'forum_link'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Название',
            'creation_year' => 'Год создания',
            'site' => 'Ссылка на сайт (партнерская)',
            'broker_price_type_id' => 'id типа ценообразования',
            'broker_logo' => 'Путь до логотипа',
            'partners_program' => 'Партнерская программа',
            'description' => 'Описание',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'docs_link' => 'Ссылка на регламенты брокера',
            'forum_link' => 'Ссылка на форум',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}