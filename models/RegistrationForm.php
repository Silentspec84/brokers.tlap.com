<?php

namespace app\models;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use Yii;

class RegistrationForm extends BaseRegistrationForm
{
    public function rules()
    {
        $user = $this->module->modelMap['User'];

        return [
            // username rules
            'usernameTrim'     => ['username', 'trim'],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernamePattern'  => ['username', 'match', 'pattern' => $user::$usernameRegexp],
            'usernameRequired' => ['username', 'required'],
            'usernameUnique'   => [
                'username',
                'unique',
                'targetClass' => $user,
                'message' => 'Такое имя пользователя уже есть'
            ],
            // email rules
            'emailTrim'     => ['email', 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern'  => ['email', 'email'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => 'Такой адрес email уже используется'
            ],
            // password rules
            'passwordRequired' => ['password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);

        if (!$user->register()) {
            Yii::$app->getSession()->setFlash('error', 'Регистрация не была произведена');
            return false;
        }

        $user_settings = new UserSettings();
        $user_settings->user_id = $user->id;
        $user_settings->role = UserSettings::ROLE_USER;
        $user_settings->save();

        Yii::$app->getSession()->setFlash('info', 'Ваш аккаунт был успешно создан. На почту, указанную при регистрации выслано письмо с дальнейшими инструкциями');

        return true;
    }
}