<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель регуляторов.
 *
 * @property integer $id
 * @property string $title
 * @property string $creation_year
 * @property int $country_id
 * @property string $text
 * @property string $description
 * @property string $keywords
 * @property string $logo
 * @property string $site
 * @property string $email
 * @property string $tel
 * @property string $address
 * @property int $date_created
 * @property int $date_modified
 */
class Regulator extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regulators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'creation_year', 'country_id', 'text', 'description', 'keywords',
                'logo', 'site', 'email', 'tel', 'address'], 'required'],
            [['date_created', 'date_modified', 'country_id'], 'integer'],
            [['title', 'creation_year', 'text', 'description', 'keywords', 'logo', 'site', 'email',
                'tel', 'address'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Название регулятора',
            'creation_year' => 'Год создания',
            'country_id' => 'id страны',
            'text' => 'Описание',
            'description' => 'Описание страницы',
            'keywords' => 'Ключевики',
            'logo' => 'Логотип',
            'site' => 'Сайт',
            'email' => 'Почта',
            'tel' => 'Телефон',
            'address' => 'Адрес',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}