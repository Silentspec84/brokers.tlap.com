<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $title
 * @property string $type
 * @property string $server_name
 * @property string $min_depo
 * @property string $stopout
 * @property string $min_lot
 * @property string $max_lot
 * @property string $max_deals
 * @property string $execution_tec
 * @property integer $swap_free
 * @property integer $spread_type
 * @property integer $symbols_type
 * @property string $margin_call
 * @property string $execution_speed
 * @property string $commission
 * @property integer $phone_dealing
 * @property string $lock_margin
 * @property string $percent_income
 * @property int $date_created
 * @property int $date_modified
 */
class BrokerAccountType extends ActiveRecord
{
    use DatesSaveTrait;

    const SWAP_FREE_TYPE = 1;
    const NO_SWAP_FREE_TYPE = 0;

    const FIX_SPREAD_TYPE = 1;
    const NON_FIX_SPREAD_TYPE = 2;

    const THREE_DIGITS_TYPE = 3;
    const FIVE_DIGITS_TYPE = 5;

    const PHONE_DEALING_TYPE = 1;
    const NO_PHONE_DEALING_TYPE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers_account_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'title', 'type', 'server_name', 'min_depo', 'stopout',
                'min_lot', 'max_lot', 'max_deals', 'execution_tec', 'swap_free', 'spread_type',
                'symbols_type', 'margin_call', 'execution_speed', 'commission', 'phone_dealing',
                'lock_margin', 'percent_income'], 'required'],
            [['date_created', 'date_modified', 'swap_free', 'spread_type', 'symbols_type',
                'phone_dealing', 'broker_id'], 'integer'],
            [['title', 'type', 'server_name', 'server_address', 'min_depo', 'stopout',
                'min_lot', 'max_lot', 'max_deals', 'execution_tec', 'margin_call', 'execution_speed',
                'commission', 'lock_margin', 'percent_income'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'broker_id' => 'id брокера',
            'title' => 'Название',
            'type' => 'Тип счета',
            'server_name' => 'Название сервера',
            'min_depo' => 'Минимальный депозит',
            'stopout' => 'Уровень Stop Out',
            'min_lot' => 'Минимальный лот',
            'max_lot' => 'Максимальный лот',
            'max_deals' => 'Максимальное количество сделок',
            'execution_tec' => 'Технология исполнения ордеров',
            'swap_free' => 'Swap Free',
            'spread_type' => 'Тип спреда',
            'symbols_type' => 'Знаков после запятой',
            'margin_call' => 'Уровень Margin Call',
            'execution_speed' => 'Скорость исполнения',
            'commission' => 'Комиссия',
            'phone_dealing' => 'Телефонный дилинг',
            'lock_margin' => 'Локированная маржа',
            'percent_income' => 'Процентная ставка',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}