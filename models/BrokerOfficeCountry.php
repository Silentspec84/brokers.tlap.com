<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель стран.
 *
 * @property integer $id
 * @property integer $broker_id
 * @property integer $country_id
 * @property integer $office_type
 * @property string $office_address
 * @property string $office_phone
 * @property int $date_created
 * @property int $date_modified
 */
class BrokerOfficeCountry extends ActiveRecord
{
    use DatesSaveTrait;

    const OFFICE_TYPE_MAIN = 10;
    const OFFICE_TYPE_SECONDARY = 20;

    const OFFICE_TYPE_MAIN_TEXT = 'Основной офис';
    const OFFICE_TYPE_SECONDARY_TEXT = 'Представительство';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers_offices_countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['broker_id', 'country_id', 'office_type', 'office_address', 'office_phone'], 'required'],
            [['date_created', 'date_modified', 'broker_id', 'country_id', 'office_type'], 'integer'],
            [['office_address', 'office_phone'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'broker_id' => 'id брокера',
            'country_id' => 'id страны',
            'office_type' => 'Тип офиса',
            'office_address' => 'Адрес офиса',
            'office_phone' => 'Телефон офиса',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}