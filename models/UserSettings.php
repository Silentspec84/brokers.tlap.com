<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель пользователя.
 *
 * @property integer $id 	        int(11)
 * @property integer $user_id 	    int(11)
 * @property int $role 	            int(11)
 * @property int $date_modified 	int(11)
 * @property int $date_created 	    int(11)
 */
class UserSettings extends ActiveRecord
{
    use DatesSaveTrait;

    public const ROLE_ADMIN = 1;
    public const ROLE_MODERATOR = 2;
    public const ROLE_BROKER = 3;
    public const ROLE_USER = 4;

    public static $role_names = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_MODERATOR => 'Модератор',
        self::ROLE_BROKER => 'Представитель брокера',
        self::ROLE_USER => 'Пользователь',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'role'], 'required'],
            [['user_id', 'role'], 'integer'],
            [['date_created', 'date_modified'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'Эл. почта',
            'role' => 'Имя',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }
}