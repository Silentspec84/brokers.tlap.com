<?php

namespace app\models;

use app\classes\helpers\DatesSaveTrait;
use yii\db\ActiveRecord;

/**
 * Модель языков.
 *
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property int $date_created
 * @property int $date_modified
 */
class Lang extends ActiveRecord
{
    use DatesSaveTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'langs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'iso'], 'required'],
            [['date_created', 'date_modified'], 'integer'],
            [['title', 'iso'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Язык',
            'iso' => 'Код языка',
            'country_id' => 'id страны',
            'date_modified' => 'Дата редактирования',
            'date_created' => 'Дата регистрации',
        ];
    }

}