<?php

namespace app\classes\helpers;

/**
 * Хелпер для обработки строк
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
class StringFormatter
{
    /**
     * @param string $string
     * @return mixed
     */
    public static function replaceCommaWithDot(string $string)
    {
        if (preg_match('/^[0-9]+,[0-9]+$/', trim($string))) {
            return str_replace(',' , '.', $string);
        }

        return $string;
    }

    /**
     * Проеряет входящий $value на json формат если да
     * то форматирует строку в $value1 $key1 $delimiter $value2 $key2 ...
     * Применяется для обработки вывода мультивалютных полей в виде
     * 10RUB + 20USD
     *
     * @param string $value
     * @param string $delimiter
     * @return string
     */
    public static function renderKeyValueString(string $value, string $delimiter): string
    {
        $json_value = json_decode($value, true);
        if (!is_array($json_value)) {
            return $value;
        }

        $result = [];
        foreach ($json_value as $key => $value) {
            $result[] = "$value $key";
        }

        return implode($result, $delimiter);
    }

    /**
     * Возвращает строку в скобках, если строка не пустая.
     * @param string $str
     * @return string
     */
    public static function wrapInParenthesesIfExists(string $str): string
    {
        return (string)$str ? '('.$str.')' : '';
    }

    /**
     * @param array $subtotals // [ (string)currency_sign => (float)subtotal_value ]
     * @return string
     */
    public static function renderTotalFromSubtotals(array $subtotals): string
    {
        $total = '';
        foreach ($subtotals as $currency_sign => $subtotal_value) {
            if (!$total) {
                $total = $subtotal_value . ' ' . $currency_sign;
                continue;
            }
            $math_sign = $subtotal_value < 0 ? ' - ' : ' + ';
            $total .= $math_sign . abs($subtotal_value) . ' ' . $currency_sign;
        }

        return $total;
    }
}