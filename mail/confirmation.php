<?php
use yii\helpers\Html;

/** @var array $content */

?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Приветствуем вас, <?=$content['login_name']?>!
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Спасибо за регистрацию на сайте <?=Yii::$app->name?><br>
    Чтобы завершить регистрацию, пожалуйста, пройдите по ссылке ниже:
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= Html::a(Html::encode($content['link']), $content['link']) ?>
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не можете перейти по ссылке, кликнув на нее, пожалуйста, скопируйте ее в новое окно браузера.
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не проходили регистрацию на нашем ресурсе, пожалуйста, просто проигнорируйте это письмо.
</p>
