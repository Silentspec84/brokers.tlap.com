<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Здравствуйте, <?=$content['user']?>!
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Теперь у вашего аккаунта на сайте <?=Yii::$app->name?> новый пароль.<br>
    Мы сгенерировали вам пароль: <?=$content['password']?><br>
    Вы всегда можете изменить его на свой в <?= Html::a('личном кабинете', Url::to($content['link'], true)); ?>
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не проходили регистрацию на нашем ресурсе, пожалуйста, просто проигнорируйте это письмо.
</p>