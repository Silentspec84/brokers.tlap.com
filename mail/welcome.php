<?php

use yii\helpers\Html;

/** @var array $content */

?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Здравствуйте, <?=$content['user']?>!
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Ваш аккаунт на сайте <?=Yii::$app->name?> успешно создан. Добро пожаловать!
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Теперь вы можете войти на сайт. Чтобы вы не забыли ваш логин и пароль, мы высылаем их вам в этом письме:<br>
    Ваш логин: <?=$content['user']?><br>
    Ваш пароль: <?=$content['password']?><br>
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Пожалуйста, сохраните это письмо, так как мы не сможем напомнить вам ваш пароль - с этого момента он зашифрован в одностороннем порядке.
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не делали данный запрос, пожалуйста, просто проигнорируйте это письмо
</p>
