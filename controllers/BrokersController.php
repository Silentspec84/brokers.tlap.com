<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\AccessRule;

class BrokersController extends BaseController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'error', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'error', 'logout', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    \Yii::$app->getSession()->setFlash('error', 'Недостаточно прав для просмотра раздела по адресу: '. \Yii::$app->request->url);
                    $this->goHome();
                }
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionAll()
    {
        return $this->render('all_brokers');
    }
}