<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\AccessRule;

class SiteController extends BaseController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'error', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'error', 'logout', 'recover', 'register-success', 'confirm-email', 'recovery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    \Yii::$app->getSession()->setFlash('error', 'Недостаточно прав для просмотра раздела по адресу: '. \Yii::$app->request->url);
                    $this->goHome();
                }
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionError() {
        /** Exception $exception */
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            $error_message = 'На окружении произошла фатальная ошибка!'."\n";
            $error_message .= $exception->getFile().':'.$exception->getLine()."\n";
            $error_message .= 'Error code: '. $exception->getCode()."\n";
            $error_message .= 'Error msg: '.$exception->getMessage();

            return $this->render('error', ['message' => $exception, 'name' => 'Фатальная ошибка']);
        }
    }

}
