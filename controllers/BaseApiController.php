<?php

namespace app\controllers;

use app\classes\api\Response;
use app\classes\ErrorHandler;
use app\classes\validators\RequestValidator;
use app\components\AccessRule;
use app\controllers\adapters\Paginator;
use app\models\User;
use Yii;
use yii\filters\AccessControl;

/**
 * Class BaseController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 *
 */
class BaseApiController extends BaseController {

    public const AJAX_ONLY_PERMITTED_ERROR = 'only ajax request are permitted';
    public const POST_IS_EMPTY_ERROR = 'post data is empty';
    public const REQUEST_PARAMETERS_ERROR_TEXT = 'Неверные параметры запроса';
    public const AUTH_ERROR = 'У вас недостаточно прав для совершения данной операции';
    public const NOT_ENOUGH_PARAMETERS_ERROR_TEXT = 'Not enough parameters';
    public const DATA_SAVE_ERROR = 'Ошибка сохранения данных';

    const REQUEST_KEY_ERROR = 'request';

    /** @var Response */
    public $response;

    /** @var RequestValidator */
    private $request_validator;

    /** @var Paginator */
    private $paginator;

    protected $request_post;

    protected $request_get;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN, User::ROLE_USER],
                    ]
                ],
                'denyCallback' => function() {
                    if ($this->user->status === User::STATUS_INACTIVE) {
                        Yii::$app->user->logout();
                        $this->redirect('/');
                    }
                    if ($this->user->role === User::STATUS_UNCONFIRMED) {
                        $this->redirect('/');
                    }
                    $this->redirect('/');
                }
            ],
        ];
    }

    public function validateRequestWithOnlyCsrf(): bool
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            $this->response->addError('Ошибка данных формы', 'all_fields');
            return false;
        }

        return true;
    }

    public function beforeAction($action)
    {
        $this->response = new Response();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!\Yii::$app->request->isAjax) {
            $this->response->addError(self::AJAX_ONLY_PERMITTED_ERROR, self::REQUEST_KEY_ERROR);
        }

        $this->request_get = \Yii::$app->request->get();
        $this->request_post = \Yii::$app->request->post();
        $this->request_validator = new RequestValidator($this->response);

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $this->response->setNotices(ErrorHandler::$ajax_exceptions_for_developer_mode);
        return parent::afterAction($action, $result);
    }

    public function getRequestValidator(): \app\classes\validators\RequestValidator
    {
        return $this->request_validator;
    }

    /**
     * @param $page
     * @param int $limit
     * @param $collection
     * @return Paginator
     * @throws \Exception
     */
    protected function getPaginator($page, $collection, $sort_params = [], $limit = Paginator::DEFAULT_LIMIT)
    {
        if (!$this->paginator) {
            $this->paginator = (new Paginator())->init($page, $collection, $sort_params, $limit);
            if (null === $this->paginator) {
                throw new \Exception('Collection instance Error');
            }
        }

        return  $this->paginator;
    }

    /**
     * Возвращает массив вида:
     * [
     *   'field_order' => val1,
     *   'sort_direction' => val2
     * ];
     * Если параметры сортировки получить не удалось, val1 и val2 будут NULL.
     * @param string $field_prefix
     * @param bool $is_mongo
     * @return array
     */
    protected function getSortParams($field_prefix = '', $is_mongo = false): array
    {
        $request = \Yii::$app->request->get();
        $field_order = !empty($request['field_order']) ? $request['field_order'] : null;
        $sort_direction = !empty($request['sort_direction']) ? (int)$request['sort_direction'] : null;
        if ($sort_direction === 1 && !$is_mongo) {
            $sort_direction = SORT_DESC;
        } elseif ($sort_direction === -1 && !$is_mongo) {
            $sort_direction = SORT_ASC;
        } elseif (!in_array($sort_direction, [-1,1])) {
            $sort_direction = null;
        }

        if (!$field_order || !$sort_direction) {
            return [
                'field_order' => null,
                'sort_direction' => null
            ];
        }
        return [
            'field_order' => $field_prefix.$field_order,
            'sort_direction' => $sort_direction
        ];

    }

}
