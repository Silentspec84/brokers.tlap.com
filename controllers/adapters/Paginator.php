<?php

namespace app\controllers\adapters;


use app\classes\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\db\ActiveQuery;


class Paginator
{
    const ACTIVE_QUERY_INSTANCE = 'ActiveQuery';
    const DEFAULT_LIMIT = 20;

    /** @var Pagination */
    private $paginator;

    /** @var array */
    private $rows = [];

    /** @var int */
    private $page;

    /** @var array */
    private $sort_params = [];


    /**
     * @param int $page
     * @param array | ActiveQuery| \yii\db\ActiveQuery $collection
     * @return Paginator | null
     */
    public function init(int $page, $collection, $sort_params, $limit): ?self
    {
        $this->page = $page;
        $this->sort_params = $sort_params;

        if ($collection instanceof \yii\db\ActiveQuery || $collection instanceof ActiveQuery) {
            /** @var  \yii\db\ActiveQuery $collection */
            $this->paginator = new Pagination(['totalCount' => $collection->count(), 'defaultPageSize' => $limit]);
            $this->rows = $this->getRowsFromQuery($collection);
        } elseif (is_array($collection)) {
            /** @var array $collection */
            $this->paginator = new Pagination(['totalCount' => count($collection), 'defaultPageSize' => $limit]);
            $this->rows = $this->getRowsFromArray($collection);
        } else {

            return null;
        }

        return $this;
    }

    /**
     * @param \yii\db\ActiveQuery| ActiveQuery $collection
     * @return array
     */
    private function getRowsFromQuery($collection):array
    {
        $limit = $this->paginator->getLimit();

        $result_query = $collection->limit($limit)->offset($this->paginator->getOffset());
        $sort_direction = $this->sort_params['sort_direction'] ?? null;
        $field_order = $this->sort_params['field_order'] ?? null;

        if (!$sort_direction || !$field_order) {
            return $result_query->all();
        }

        return $result_query->orderBy([$field_order => $sort_direction])->all();
    }


    private function getRowsFromArray(array $collection):array
    {
        $this->rows = $this->getSortedRows($collection);
        $this->rows = array_slice($this->rows, $this->paginator->offset, $this->paginator->limit);

        return $this->rows;
    }

    public function getLinks():array
    {
        if (!$this->paginator) {
            return [];
        }

        return $this->paginator->getLinks();
    }


    public function getRows():array
    {
        return $this->rows;
    }

    /**
     * Возвращает отсортированные строки, согласно полю и порядку сортировки, пришедших в get или post запросе
     * @param array $rows
     * @param array $sort_params
     * @return array
     */
    protected function getSortedRows(array $rows): array
    {
        $field_order = $this->sort_params['field_order'] ?? null;
        $sort_direction = $this->sort_params['sort_direction'] ?? null;
        if ($field_order && $sort_direction) {
            return ArrayHelper::sortTwoDimensionArray($rows, $field_order, $sort_direction);
        }

        return $rows;
    }

    public static function renderNavBar():string
    {
        $html = <<<HTML
                <nav>
                    <ul class="pagination mt-2">
                        <li v-for="(page, index) in pages" class="page-item">
                            <a v-bind:class="getPageLinkClass(page, current_page)" v-on:click="loadDataOnPageClick(page, raw_page_link)">
                            {{page}}
                            </a>
                        </li>
                    </ul>
                </nav>
HTML;
        return $html;
    }
}