<?php

namespace app\controllers;

use dektrium\user\commands\ConfirmController as BaseConfirmController;
use yii\helpers\Console;

class ConfirmController extends BaseConfirmController
{
    /**
     * Confirms a user by setting confirmed_at field to current time.
     *
     * @param string $search Email or username
     */
    public function actionIndex($search)
    {
        $user = $this->finder->findUserByUsernameOrEmail($search);
        if ($user === null) {
            $this->stdout('Ошибка! Данный пользователь не найден, обратитесь в службу технической поддержки' . "\n", Console::FG_RED);
        } else {
            if ($user->confirm()) {
                $this->stdout('Ваш аккаунт успешно активирован. Добро пожаловать!' . "\n", Console::FG_GREEN);
            } else {
                $this->stdout('Возникла неизвестная ошибка! Возможно, ваша ссылка на подтверждение аккаунта устарела, запросите новую или обратитесь в службу технической поддержки' . "\n", Console::FG_RED);
            }
        }
    }
}