<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use app\models\User;
use app\components\Header;
use app\components\Footer;

AppAsset::register($this);

/** @var User $user */
$user = Yii::$app->getUser()->identity;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>

        <title><?= Html::encode($this->title) ?></title>
        <? if (isset($this->title)): ?>
            <meta name="title" content="<?= $this->title ?>">
        <? endif; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta name="csrf-token" content="<?= Yii::$app->request->csrfToken?>">
        <?php $this->head(); ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

        <!--Виджет кастомного хедера-->
        <?php Header::begin();?>
        <?php Header::end();?>

        <div class="main">
            <?= $content; ?>
        </div>

        <div class="footer">
            <!--Виджет кастомного футера -->
            <?php Footer::begin();?>
            <?php Footer::end();?>
        </div>

        <?php $this->endBody()?>
    </body>
    <script>
        $.notifyDefaults({
            allow_dismiss: true
        });
    </script>
    <?php if (!empty(Yii::$app->getSession()->getFlash('error'))): ?>
        <script>
            $.notify({
                title: '<strong>Ошибка!</strong>',
                message: "<?php echo Yii::$app->getSession()->getFlash('error'); ?>"
            },{
                type: 'pastel-error',
                delay: 10000,
                template: '<div data-notify="container" class="col-4 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span>' +
                    '<span data-notify="title">{1}</span>' +
                    '<span data-notify="message">{2}</span>' +
                    '</div>'
            });
        </script>
    <?php endif; ?>
    <?php if (!empty(Yii::$app->getSession()->getFlash('warning'))): ?>
        <script>
            $.notify({
                title: '<strong>Предупреждение:</strong>',
                message: "<?php echo Yii::$app->getSession()->getFlash('warning'); ?>"
            },{
                type: 'pastel-warning',
                delay: 10000,
                template: '<div data-notify="container" class="col-4 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span>' +
                    '<span data-notify="title">{1}</span>' +
                    '<span data-notify="message">{2}</span>' +
                    '</div>'
            });
        </script>
    <?php endif; ?>
    <?php if (!empty(Yii::$app->getSession()->getFlash('info'))): ?>
        <script>
            $.notify({
                title: '<strong>Уведомление:</strong>',
                message: "<?php echo Yii::$app->getSession()->getFlash('info'); ?>"
            },{
                type: 'pastel-notice',
                delay: 10000,
                template: '<div data-notify="container" class="col-4 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span>' +
                    '<span data-notify="title">{1}</span>' +
                    '<span data-notify="message">{2}</span>' +
                    '</div>'
            });
        </script>
    <?php endif; ?>
</html>
<?php $this->endPage() ?>