<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Ошибка!';
?>
<div style="background:url('../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 95%;">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="grey_box rounded-lg">
                <div class="jumbotron">
                    <h1>Внимание</h1>
                    <p><?= nl2br(Html::encode($message)) ?></p>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
