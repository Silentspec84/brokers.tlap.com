<?php

/* @var $this yii\web\View */

$this->title = 'Брокеры';
?>
<div style="background:url('../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 95%;">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="grey_box rounded-lg">
                <div class="jumbotron">
                    <h1>Все о форекс брокерах</h1>
                    <p>Рейтинги и сравнение брокеров по любым параметрам, данные о свопах, спредах, типах счетов и многое другое</p>
                    <p>
                        <a class="btn btn-primary" href="<?php if (Yii::$app->user->isGuest): ?>/user/registration/register<?php else: ?>/load<?php endif?>">Попробовать</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="transparent_box rounded-lg">
                <div class="row">
                    <div class="col-12 mb-4">
                        <h2 class="text-center">Болит голова от выбора лучшего брокера?</h2><br>
                        <img src="/img/template_illustratio.svg" class="rounded float-left" width="30%">
                        <h5>Тут вы сможете практически моментально подобрать брокера по важным для вас параметрам</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h2 class="text-center">Не можете выбрать между несколькими брокерами?</h2><br>
                        <img src="/img/template_illustratio2.svg" class="rounded float-right" width="30%">
                        <h5>Функция сравнения брокеров поможет вам определиться с выбором</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
