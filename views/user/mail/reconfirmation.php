<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var dektrium\user\models\Token $token
 */
?>

<table width="100%" bgcolor="#1abc9c" cellpadding="0" cellspacing="0" border="0">
    <tbody>
    <tr>
        <td style="background: #e3eaea; padding: 60px; font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;">
            <span style="color: #fc6d26;">Брокеры tlap.com</span>
        </td>
    </tr>
    </tbody>
</table>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Здравствуйте,
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Мы получили запрос на изменения адреса электронной почты к вашему аккаунту на сервисе <?=Yii::$app->name?>
    Пожалуйста, перейдите по ссылке снизу для завершения вашего запроса:
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= Html::a(Html::encode($token->getUrl()), $token->getUrl()); ?>
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не можете перейти по ссылке, пожалуйста, попробуйте скопировать ссылку и вставить ее в адресную строку вашего браузера.
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Если вы не проходили регистрацию в нашем сервисе, просто проигнорируйте данное сообщение.
</p>
