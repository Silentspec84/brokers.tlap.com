<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div style="background:url('../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 100%;">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="panel panel-default  rounded-lg" style="margin-top: 25%">
                <div class="panel-heading">
                    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnType' => false,
                        'validateOnChange' => false,
                    ]) ?>

                    <?php if ($module->debug): ?>
                        <?= $form->field($model, 'login', [
                            'inputOptions' => [
                                'autofocus' => 'autofocus',
                                'class' => 'form-control',
                                'tabindex' => '1']])->dropDownList(LoginForm::loginList())->label('Логин');
                        ?>

                    <?php else: ?>

                        <?= $form->field($model, 'login',
                            ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
                        )->label('Логин');
                        ?>

                    <?php endif ?>

                    <?php if ($module->debug): ?>
                        <div class="alert alert-warning">
                            <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                        </div>
                    <?php else: ?>
                        <?= $form->field(
                            $model,
                            'password',
                            ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])
                            ->passwordInput()
                            ->label(
                                'Пароль'
                                . ($module->enablePasswordRecovery ?
                                    ' (' . Html::a(
                                        'Забыли пароль?',
                                        ['/user/recovery/request'],
                                        ['tabindex' => '5']
                                    )
                                    . ')' : '')
                            ) ?>
                    <?php endif ?>

                    <p>
                        <input type="checkbox" name="agree" id="rememberMe" class="check_input" style="display: none;" onchange="check()">
                        <label for="rememberMe" class="check mr-2 mt-3">
                            <svg width="18px" height="18px" viewBox="0 0 18 18">
                                <path d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z"></path>
                                <polyline points="1 9 7 14 15 4"></polyline>
                            </svg>
                        </label>
                        <strong>Запомнить меня</strong>
                    </p>

                    <?= Html::submitButton(
                        'Вход',
                        ['class' => 'btn btn-primary btn-block', 'tabindex' => '4']
                    ) ?>
                    <br>
                    <?php if ($module->enableConfirmation): ?>
                        <p class="text-center">
                            <?= Html::a('Не получили подтверждение на email?', ['/user/registration/resend']) ?>
                        </p>
                    <?php endif ?>
                    <?php if ($module->enableRegistration): ?>
                        <p class="text-center">
                            <?= Html::a('Еще нет аккаунта? Зарегистрируйтесь!', ['/user/registration/register']) ?>
                        </p>
                    <?php endif ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>