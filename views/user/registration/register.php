<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View              $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\Module      $module
 */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="background:url('../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 100%;">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="panel panel-default rounded-lg" style="margin-top: 25%">
                <div class="panel-heading">
                    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id'                     => 'registration-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                    ]); ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'username')->label('Логин') ?>

                    <?php if ($module->enableGeneratingPassword == false): ?>
                        <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
                    <?php endif ?>

                    <div class="dropdown-divider"></div>
                    <br>
                    <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary btn-block']) ?>

                    <?php ActiveForm::end(); ?>
                </div>
                <p class="text-center">
                    <?= Html::a('Уже зарегистрированы? Войдите!', ['/user/security/login']) ?>
                </p>
            </div>
        </div>
    </div>
</div>