<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\RecoveryForm $model
 */

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="background:url('../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 100%;">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="panel panel-default rounded-lg" style="margin-top: 35%">
                <div class="panel-heading">
                    <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'password-recovery-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                    ]); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary btn-block']) ?><br>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>