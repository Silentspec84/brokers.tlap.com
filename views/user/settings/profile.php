<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
?>

<div style="background:url('../../img/dices-over-newspaper.jpg') center no-repeat; background-size: 100% 100%; height: 100%;">
    <div class="row">
        <div class="col-2 ml-4">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-9 ml-2 mr-2">
            <div class="box rounded-lg">
                <h3 class="text-center"><?= Html::encode($this->title) ?></h3><br>
                <div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'profile-form',
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                            'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnBlur' => false,
                    ]); ?>

                    <?= $form->field($model, 'name') ?>

                    <?= $form->field($model, 'public_email') ?>

                    <?= $form->field($model, 'website') ?>

                    <?= $form
                        ->field($model, 'timezone')
                        ->dropDownList(
                            ArrayHelper::map(
                                Timezone::getAll(),
                                'timezone',
                                'name'
                            )
                        ); ?>

                    <?= $form->field($model, 'bio')->textarea() ?>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn my-button btn-block btn-primary']) ?>
                            <br>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>