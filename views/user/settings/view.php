<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use app\models\Smeta;
use yii\helpers\Html;

/**
 * @var Smeta $smeta_item
 */

$this->title = 'Смета';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<div class="site-index uk-margin-top uk-margin-bottom">
    <div uk-grid>
        <div class="uk-width-1-5">
            <?= $this->render('_menu') ?>
        </div>
        <div class="uk-width-4-5">
            <div class="body-content">
                <article class="uk-article">
                    <div class="uk-card uk-card-default uk-card-body uk-background-muted uk-box-shadow-medium uk-margin-right">
                        <h3 class="uk-article-title uk-text-center"><?= Html::encode($this->title) ?></h3>
                        <div>
                            <div class="uk-overflow-auto">
                                <table class="uk-table uk-table-hover uk-table-middle uk-table-divider">
                                    <thead>
                                        <tr>
                                            <th class="uk-table-shrink">№</th>
                                            <th class="uk-table-expand">Наименование</th>
                                            <th>Марка</th>
                                            <th>Производитель</th>
                                            <th>Ед. изм.</th>
                                            <th>Кол-во</th>
                                            <th>Цена</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($archive_smeta as $smeta_item): ?>
                                        <tr>
                                            <td><?=$smeta_item->number?></td>
                                            <td><?=$smeta_item->title?></td>
                                            <td><?=$smeta_item->model?></td>
                                            <td><?=$smeta_item->factory?></td>
                                            <td><?=$smeta_item->measure?></td>
                                            <td><?=$smeta_item->count?></td>
                                            <td><?=$smeta_item->price?></td>
                                        </tr>
                                    <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </article>
            </div>
        </div>
    </div>
</div>