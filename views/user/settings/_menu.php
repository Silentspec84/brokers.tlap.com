<?php

use yii\helpers\Html;

$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;
$user = Yii::$app->user->identity;
?>

<div class="panel panel-default rounded-lg" style="margin-top: 33%">
    <div class="panel-heading rounded-lg">
        <h3 class="text-center"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body rounded-lg">
        <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
            <a class="nav-link <?php if(Yii::$app->controller->route === 'user/settings/account'):?>active<?php endif;?>"
               role="tab" href="/user/settings/account">
                Аккаунт
            </a>
            <a class="nav-link <?php if(Yii::$app->controller->route === 'user/settings/profile'):?>active<?php endif;?>"
               role="tab" href="/user/settings/profile">
                Профиль
            </a>
        </div>
    </div>
</div>
