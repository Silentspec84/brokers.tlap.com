<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = 'Тарифы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div style="background:url('../../img/architecture-2256489.jpg') center no-repeat; background-size: 100% 100%; height: 95%;">
    <div class="row">
        <div class="col-2 ml-4">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-9 ml-2 mr-2">
            <div class="box">
                <h3 class="text-center"><?= Html::encode($this->title) ?></h3><br>
                <div>
                    Gimme ya money man!
                </div>
            </div>
        </div>
    </div>
</div>