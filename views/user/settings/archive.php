<?php

/* @var $this yii\web\View */

use app\models\Smeta;
use yii\helpers\Html;

/* @var array $archive_list */
/* @var Smeta $smeta */

$this->title = 'Архив смет';
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>
<?php $this->registerCsrfMetaTags() ?>

<div style="background:url('../../img/architecture-2256489.jpg') center no-repeat; background-size: 100% 100%; height: 95%;">
    <div class="row">
        <div class="col-2 ml-4">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-9 ml-2 mr-2">
            <div class="box">
                <h3 class="text-center"><?= Html::encode($this->title) ?></h3><br>
                <div>
                    <?php foreach ($archive_list as $smeta): ?>
                        <div class="uk-card uk-card-default uk-card-body uk-margin-bottom">
                            <article class="uk-comment">
                                <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand">
                                        <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                            <li>Название: <?=$smeta->title?></li>
                                            <li>Дата создания: <?=date('Y.m.d h:i:s', $smeta->date_created)?></li>
                                            <li>Дата последнего редактирования: <?=date('Y.m.d h:i:s', $smeta->date_modified)?></li>
                                        </ul>
                                    </div>
                                </header>
                                <div class="uk-comment-body">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                                    <p class="uk-align-center">
                                        <a style="" class="uk-button uk-button-danger my-button" href="/user/settings/view/<?=$smeta->id?>">Перейти</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                    <?php endforeach?>
                </div>
            </div>
        </div>
    </div>
</div>
