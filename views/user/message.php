<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module
 */

$this->title = $title;
?>

<div style="background:url('../../../img/blur-1853262.jpg') center no-repeat; background-size: 100% 100%; height: 100%;">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
            <div class="grey_box rounded-lg" style="margin-top: 15%">
                <div class="jumbotron">
                    <h3><?=$title?></h3>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
