"use strict";

const commonAutocompleteMixins = {
    data: function() {
        return {
            list: [],
        };
    },
    methods:{
        onChange: function(query, item_id) {
            if (!query && !item_id) { // данные стерли, надо об этом сказать
                this.$emit('option_chosen', {
                    'field_title': this.filed_title,
                    'item': this.item
                });
            } else {
                //если стоит ограничение на минимальную длину поискового запроса
                let search_min_query = parseInt(this.search_min_query);
                if (query && search_min_query && query.length <= search_min_query) {
                    return;
                }

                let get_data = [];
                if (query) {
                    get_data.push('query=' + query);
                }
                if (item_id) {
                    get_data.push('item_id=' + item_id);

                    if (this.list.length > 0) {
                        let filtered_list = this.list.filter(item => item.id === item_id);
                        this.item = filtered_list[0];
                    }
                    this.$emit('option_chosen', {
                        'field_title': this.filed_title,
                        'item': this.item
                    });
                }
                let result = get_data.join('&');
                let link = this.link + result;
                let self = this;
                axios.get(link)
                    .then(function (response) {
                        self.list = response.data.content;
                    }).catch(function () {
                    console.warn('Ошибка при загрузке данных по ссылке' + link);
                });
            }
        },
    }
};
/**
 * Поиск с автодополнениями.
 */
Vue.component('common-autocomplete', {
    template: `<v-autocompleete v-model="item.title" :options = "list" @input="onChange(item.title)" :input_class='input_class' @hide="$emit('hide');"\>
                   <div slot="selected" class="location-icon">
                        <i v-bind:class="icon"></i>
                   </div>
                   <div slot="option" slot-scope="option">
                        <span v-if="option.data.first_unregister_company" style="color: #727b84">&nbsp;<br>Незарегистрированные компании:</span>
                        <div v-on:click="onChange('', option.data.id)">
                            <span v-if="option.data.id !== null">{{option.data.title}} <span v-if="option.data.inn" style="color: #727b84"> {{option.data.inn}}</span></span>
                            <div v-else>
                                <span style="color:#7a7979">{{option.data.error}}</span><br>
                                <hr style="color:#cfcdcd">
                                <a :href="option.data.link">{{option.data.text}}</a>
                            </div>
                        </div>
                   </div>
               </v-autocompleete>`,
    mixins: [commonAutocompleteMixins],
    data: function() {
        return {
            item: {
                title: null
            },
        };
    },
    props: {
        link: {
            type: String
        },
        search_min_query: {
            type: String
        },
        icon: {
            type: String
        },
        filed_title: {
            type: String
        },
        input_class: {
            type: String,
            default: 'form-control form-control-sm',
        },
        company_title: {
            type: String,
            default: ''
        },
        company_id: {
            type: Number,
            default: 0
        },
        is_company_registered: {
            type: Boolean,
            default: true
        }
    },
    methods: {
        onHide: function () {

        }
    },

    mounted: function () {
        this.company_id = parseInt(this.company_id);
        if (this.company_title === '' || this.company_id <= 0) {
            return;
        }
        this.item.title = this.company_title;
        this.item.id =  this.company_id;
        this.item.is_company_registered =  this.is_company_registered;
        this.$emit('option_chosen', {
            'field_title': this.filed_title,
            'item': this.item
        });
    }
});
