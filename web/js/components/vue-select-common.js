"use strict";
/**
 * Селект с подгрузкой вариантов выбора по ajax
 */
Vue.component('common-select', {
    template: `<select  v-model="item" v-bind:class="input_class" @change="chooseOption()">
                    <option v-for="option in list" v-bind:value="option">
                        {{option.title}}
                    </option>
               </select>`,
    data: function() {
        return {
            list: [],
            item: {}
        };
    },
    props: {
        link: {
            type: String
        },
        filed_title: {
            type: String
        },
        input_class: {
            type: String,
            default: 'form-control form-control-sm'
        },
        selected_item_title: {
            type: String
        },
        selected_item_id: {
            type: Number
        }
    },
    methods:{
        onLoad: function() {
            axios.get(this.link)
                .then(response => {
                    this.list= Object.values(response.data.content);
                    let  selected_item_key = 0;
                    for (let i in this.list) {
                        if (this.selected_item_title) {
                            if (this.list[i].title == this.selected_item_title) {
                                selected_item_key = i;
                                break;
                            }
                        } else if (this.selected_item_id) {
                            if (this.list[i].id == this.selected_item_id) {
                                selected_item_key = i;
                                break;
                            }
                        }
                    }
                    this.item = this.list[selected_item_key];
                    this.$emit('option_chosen', {
                        'field_title': this.filed_title,
                        'item': this.item
                    });

                });
        },
        chooseOption: function() {
            this.$emit('option_chosen', {
                'field_title': this.filed_title,
                'item': this.item
            });
        }
    },
    mounted: function() {
        this.onLoad();
    }
});
