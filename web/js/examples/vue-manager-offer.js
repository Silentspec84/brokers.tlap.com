"use strict";
document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: '#offer',
        mixins: [listMixins, validatorRowsMixins, formulasMixins],
        data: {
            _csrf: null,
            edite_mode: false,
            is_loading: true,
            offer_status: null,
            status_new: null,
            status_draft: null,
            status_ready: null,
            status_on_confirm: null,
            shared: {
                client_company: null,
                client_email: null,
                client_name: null,
                client_surname: null,
                comment: null,
                date_till: null,
                id: null,
                manager_comment: null
            },
            table: [],
            heads: {},
            /**
             *  Формулы(FormulaList) записанные в обратной польской нотации.
             *  Например, ['10 4 5 + *', '10 4 5 + * 2 +']. где цифры это номер колонки (FieldConfig->order_in_manager_search)
             **/
            formulas: null,
            error_list: [],
            currencies: [],
            invalid_inputs: [],
            not_exist_value: '',
            redirect_link_after_drop: '/manager-offer/index',
            drop_link: '/manager-offer-api/delete',
            send_offer_to_client_link: '/manager-offer-api/send-offer-to-client',
        },
        mounted: function() {
            const configElement = document.getElementById('sharedData');
            this.offer_status = JSON.parse(this.$refs.config.dataset.offer_status);
            this.status_new = JSON.parse(this.$refs.config.dataset.status_new);
            this.status_draft = JSON.parse(this.$refs.config.dataset.status_draft);
            this.status_ready = JSON.parse(this.$refs.config.dataset.status_ready);
            this.status_on_confirm = JSON.parse(this.$refs.config.dataset.status_on_confirm);
            if (this.offer_status === this.status_new || this.offer_status === this.status_draft) {
                this.edite_mode = true;
            }
            this.shared = JSON.parse(configElement.innerHTML);
            this._csrf = this.$refs.config.dataset.csrf;
            this.not_exist_value = this.$refs.config.dataset.not_exist_value;
            this.load();
        },
        methods: {
            processData: function(link) {
                let form_data = {
                    '_csrf': this.$refs.config.dataset.csrf,
                    'items': this.shared,
                    'table': this.table,
                };

                let rows = this.table;
                for (let line = 0; line < rows.length; line++) {
                    for (let row in rows[line]) {
                        if (rows[line].hasOwnProperty(row) && this.isNumericInt(null, null, row)) {
                            this.validate_rows(line, row, rows[line][row])
                        }
                    }
                }

                if (!this.validate() || this.areRowsHaveErrors()) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }

                axios.post(link, JSON.stringify(form_data), {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(response.data.is_success) {
                        swal("Выполнено успешно", {
                            buttons: false,
                            timer: 1500,
                        });
                        this.edite_mode = false;
                        this.shared.id = Number(response.data.content.id);
                        window.location.href = '/manager-offer/view/' + this.shared.id;
                    }
                    else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            if (key === 'all_fields') {
                                this.createSwal(response.data.errors[key], "error", 3000);
                                return;
                            }
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                }).catch(e => {
                    console.log(e);
                });
            },
            sendOfferToClient: function(id) {
                this.id = Number(id);
                this.createSwal("Идет отправка e-mail клиенту", "info", 300000);
                axios.post(this.send_offer_to_client_link, {_csrf:this._csrf, id: this.id})
                    .then(response => {
                        function redirect(id) {
                            window.location.href = '/manager-offer/view/' + id;
                        }
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                            this.closeSwal(response);
                            this.createSwal('Ошибка отправки!', "error", 2000);
                        } else {
                            this.closeSwal(response);
                            this.createSwal("E-mail отправлен клиенту", "success", 2000);
                            setTimeout(redirect, 2000, id);
                        }
                    });
            },
            saveDataAndDownloadPdf: function() {
                this.processData('/manager-offer-api/save');
                window.location.href = '/manager-offer/download-pdf/' + this.shared.id;
            },
            saveManagerComment: function() {
                let form_data = {
                    '_csrf': this.$refs.config.dataset.csrf,
                    'items': this.shared,
                    'table': this.table,
                };
                let url = '/manager-offer-api/manager-comment?id='+this.shared.id;
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                axios.post(url, JSON.stringify(form_data), {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if (response.data.is_success) {
                        swal("Сохранено", {
                            buttons: false,
                            timer: 1500,
                        });
                        console.log('save successful');
                    } else {
                        this.error_list = {};
                        for (let key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                    }
                }).catch(e => {
                    console.warn(e);
                });

            },

            validate: function() {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['manager_comment', 'date_till', 'client_surname', 'client_name', 'client_company', 'client_email', 'comment'],
                        message: 'Данное поле обязательно к заполнению!',
                        needValidate: 'isNeedValidate',
                        params: {}
                    },
                    min: {
                        fields: ['client_surname', 'client_name', 'client_company', 'client_email', 'comment'],
                        min: 2,
                        needValidate: 'isNeedValidate',
                        params: {}
                    },
                    max: {
                        fields: ['manager_comment', 'client_surname', 'client_name', 'client_company', 'client_email', 'comment'],
                        max: {manager_comment: 1000, client_surname: 255, client_name: 255, client_company: 255, client_email: 255, comment: 30000},
                        needValidate: 'isNeedValidate',
                        params: {}
                    },
                    email: {
                        fields: ['client_email'],
                        message: 'Неправильный формат почты (xxx@xxx.xx)',
                        needValidate: 'isNeedValidate',
                        params: {}
                    },
                };

                return this.validateForm(config, this.shared) && !this.areRowsHaveErrors();
            },

            isNeedValidate: function (field_name, field_value, params) {
                let not_required_fields = ['manager_comment', 'comment'];

                if (!field_value) {
                    if (not_required_fields.indexOf(field_name) === -1) {
                        return true;
                    } else {
                        return false;
                    }
                }
                return true;
            },

            load: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.get('/manager-offer-api/table-data', {params: {id: this.shared['id']}}).then(response => {
                    if (response.data.is_success) {
                        let response_heads = response.data.content.heads;
                        for(let key in response.data.content.heads) {
                            this.heads[key] = response.data.content.heads[key]['title'];
                        }
                        this.table = Object.values(response.data.content.rows);
                        this.table.map(item => item.shown = true);
                        this.formulas = response.data.content.formulas;
                        this.currencies = response.data.content.currencies;
                        this.is_loading = false;
                        this.table = this.getCalculateRows(response_heads, this.table, this.formulas);
                    }
                }).catch(e => {
                    console.warn('cant load data');
                });
            },
        }
    });
});
