"use strict";

/**
 * Показывает статус КП.
 */
Vue.component('offer-status', {
    template: `<span :class="getClass()">{{title}}</span>`,
    props: {
        title: {
            type: String,
            default: ''
        },
        status: '',
        statusList: '',
        statusModes: '',
    },
    methods: {
        getClass: function() {
            let mode = this.status ? 'bg-' + this.class  : '';
            return 'd-inline-block ' + mode + ' width-xl text-white text-center p-1';
        }
    },
    mounted: function() {
        let list = JSON.parse(this.statusList);
        let modes = JSON.parse(this.statusModes);
        this.title = this.status ? list[this.status] : '';
        this.class = this.status ? modes[this.status] : '';
    }
});
