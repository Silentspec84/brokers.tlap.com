document.addEventListener("DOMContentLoaded", function() {
    const company_profile = new Vue({
        mixins: [listMixins, validatorMixins],
        el: "#company_profile",
        data: {
            data_list: {},
            form: {
                _csrf: this._csrf,
            },
            error_list: {},
            get_link: '/config-api/get-company-profile',
            add_link: '/config-api/add-company-profile'
        },
        mounted() {
            this.error_list = {};
            this.onLoad();
        },
        methods: {
            onLoad() {
                this.form._csrf = this.$refs.config.dataset.csrf;
                this.load();
            },
            load: function () {
                var self = this;
                self.createSwal("Ожидайте загрузки данных", "info", 60000);
                axios.get(self.get_link)
                    .then(function (response) {
                        self.data_list = response.data.content;
                        swal.close();
                    });
            },
            validate: function() {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['full_name', 'short_name', 'inn'],
                        message: 'Данное поле обязательно к заполнению!',
                        needValidate: 'isNeedValidate',
                    },
                    min: {
                        fields: ['full_name', 'short_name', 'name', 'real_address', 'post_address', 'jury_address', 'inn', 'kpp', 'okpo', 'ogrn', 'bik', 'account', 'checking_account'],
                        min: {full_name: 4, short_name: 4, name: 4, real_address: 6, post_address: 6, jury_address: 6, inn: 2, kpp: 2, okpo: 2, ogrn: 2, bik: 2, account: 2, checking_account: 2},
                        needValidate: 'isNeedValidate',
                    },
                    max: {
                        fields: ['full_name', 'short_name', 'name', 'real_address', 'post_address', 'jury_address', 'inn', 'kpp', 'okpo', 'ogrn', 'bik', 'account', 'checking_account'],
                        max: {full_name: 60, short_name: 60, name: 255, real_address: 255, post_address: 255, jury_address: 255, inn: 20, kpp: 20, okpo: 20, ogrn: 20, bik: 20, account: 80, checking_account: 80},
                        needValidate: 'isNeedValidate',
                    },
                    isNumericInt: {
                        fields: ['inn', 'kpp', 'okpo', 'ogrn', 'bik', 'account', 'checking_account'],
                        message: 'Данное поле должно быть целым числом',
                        needValidate: 'isNeedValidate',
                    },
                    companyName: {
                        fields: ['full_name', 'short_name', 'name'],
                        message: 'Данное поле содержит недопустимые символы. Список разрешенных символов: буквы(кириллица, латиница), цифры, кавычки, пробел, тире, запятая, точка, №',
                        needValidate: 'isNeedValidate',
                    },
                };

                return this.validateForm(config, this.data_list);
            },
            isNeedValidate: function (field_name, field_value) {
                let required_fields = ['full_name', 'short_name', 'inn'];

                if (required_fields.indexOf(field_name) !== -1) {
                    return true; // нужно валидировать обязательные поля (required_fields)
                } else {
                    return this.required(field_name, {}, field_value);  // нужно валидировать остальные поля, если они заполнены
                }
                return false;
            },
            addItem: function() {
                this.createSwal("Идет сохранение", "info", 60000);
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                this.data_list._csrf = this.form._csrf;
                return axios.post(this.add_link, this.data_list)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                this.createSwal(response.data.errors[0], "error", 3000);
                                return;
                            }
                            for (var key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[0][key];
                            }
                            this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                        } else {
                            this.createSwal("Сохранено", "success", 1500);
                            this.error_list = {};
                            swal.close();
                        }
                    });
            },
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "col-md-12" : "col-md-12  border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            }
        }

    });
});