"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#table",
        mixins: [listMixins],
        data: {
            tables_view_link: '/table-variables-config-api/get-tables',
            data_list: [],
            _csrf: null
        },
        methods: {
            load: function () {
                return axios.post(this.tables_view_link, {_csrf:this._csrf})
                    .then(response => {
                        if (response.data.is_success) {
                            this.data_list = response.data.content;
                        } else {
                            this.error_list = [];
                            for (var key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        }
                    });
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.load();
        }

    });
});
