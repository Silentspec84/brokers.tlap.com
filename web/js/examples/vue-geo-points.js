"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        mixins: [listMixins, seacrchComponentMixins],
        el: "#geo_points",
        data: {
            _csrf: null,
            countries: [],
            towns: [],
            town: {},
            country: {},
            transport_points: [],
            editable_aliases: {},
            add_transport_points: {},
            town_alias_title: '',
            transport_point_titles: {
                port_title_en: '',
                port_title_ru: '',
                railway_title: '',
                auto_title: ''
            },
            town_canonical_en: {
                title: '',
                lang: 'EN',
                country_id: null
            },
            query: '',
            town_canonical_ru: {
                title: '',
                lang: 'RU',
                country_id: null
            },
            location_types_titles: [],  //Location::getLocationTypesTitles()
            form: {
                data_list: this.data_list,
                _csrf: null
            },
            error_list: {},
            is_loading: false,
            add_link_town: '/geo-points-api/add-town',
            get_link_countries: '/geo-points-api/get-countries',
            get_link_towns: '/geo-points-api/get-towns',
            get_link_transport_points: '/geo-points-api/get-transport-points',
            drop_link: '/geo-points-api/drop-geo-point',
            update_link: '/geo-points-api/update-town-aliases',
            add_link_town_alias: '/geo-points-api/add-town-alias',
            add_link_transport_point: '/geo-points-api/add-transport-point',
            link_get_search_result: '/geo-points-api/get-geo-points-from-search',
            search_placeholder: 'Город / порт / станция / авто доставка',
        },
        mounted() {
            var dataset = this.$refs.config.dataset;
            this.form._csrf = dataset.csrf;
            this._csrf = dataset.csrf;
            this.location_types_titles = JSON.parse(dataset.location_types_titles);
            this.getCountries();
        },
        methods: {
            async onSearch() {
                if (!this.query) {
                    this.error_list = {};
                    return this.getCountries();
                }
                if (await this.search()) {
                    this.countries = this.search_result.countries;
                    this.towns = this.search_result.towns;
                    this.transport_points = this.search_result.transport_points;
                }
            },
            getCountries: function () {
                axios.get(this.get_link_countries)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                        } else {
                            this.error_list = {};
                            this.countries = response.data.content;
                        }
                    });
            },
            getTowns: function (country_id) {
                axios.post(this.get_link_towns, {_csrf:this._csrf, country_id: country_id})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                        } else {
                            this.error_list = {};
                            this.$set(this.towns, country_id, response.data.content);
                        }
                    });
            },
            getTransportPoints: async function (town_id) {
                const response = await axios.post(this.get_link_transport_points, {
                    _csrf: this._csrf,
                    town_id: town_id
                });

                if (!response.data.is_success) {
                    this.error_list = response.data.errors;
                } else {
                    this.error_list = {};
                    this.$set(this.transport_points, town_id, response.data.content);
                }
            },
            addTown: function (country_id) {
                this.town_canonical_en.country_id = country_id;
                this.town_canonical_ru.country_id = country_id;
                this.town_canonical_en.title = this.$refs['town_canonical_en_'+country_id][0].value;
                this.town_canonical_ru.title = this.$refs['town_canonical_ru_'+country_id][0].value;
                this.createSwal("Идет сохранение географической точки", "info", 300000);
                axios.post(this.add_link_town, {_csrf:this._csrf, town_canonical_en: this.town_canonical_en, town_canonical_ru: this.town_canonical_ru})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                            this.closeSwal(response);
                            this.createSwal("Ошибка сохранения географической точки", "error", 2000);
                        } else {
                            this.error_list = {};
                            this.town_canonical_en.title = '';
                            this.town_canonical_ru.title = '';
                            this.closeSwal(response);
                            this.createSwal("Геоографическая точка сохранена", "success", 3000);
                            if(this.query){
                                this.onSearch();
                            } else {
                                this.getTowns(country_id);
                            }
                        }
                    });
            },
            addTownAlias: function (town, town_id, country_id) {
                this.createSwal("Идет обновление географической точки", "info", 300000);
                axios.post(this.add_link_town_alias, {_csrf:this._csrf, town_id: town_id, title: this.$refs['town_alias_title_' + town_id][0].value})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                            this.closeSwal(response);
                            this.createSwal("Ошибка сохранения географической точки", "error", 2000);
                        } else {
                            this.error_list = {};
                            this.town_alias_title = '';
                            this.closeSwal(response);
                            this.createSwal("Геоографическая точка обновлена", "success", 3000);
                            this.showAddAlias(town_id, town);
                            if (this.query) {
                                this.onSearch();
                            } else {
                                this.getTowns(country_id);
                            }
                        }
                    });
            },
            updateAliasesTown: function (town_id, town, country_id) {
                axios.post(this.update_link, {_csrf:this._csrf, town_aliases: town.other_aliases})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                        } else {
                            this.editable_aliases[town_id].edit = false;
                            if (this.query) {
                                this.onSearch();
                            } else {
                                this.getTowns(country_id);
                            }
                        }
                    });
            },
            addTransportPoint: function (town_id, tp_type) {
                this.createSwal("Идет сохранение географической точки", "info", 300000);

                if (tp_type === this.$config.TYPE_PORT) {
                    this.transport_point_titles.port_title_en = this.$refs['port_title_en_' + town_id][0].value;
                    this.transport_point_titles.port_title_ru = this.$refs['port_title_ru_' + town_id][0].value;
                }
                if (tp_type === this.$config.TYPE_RAIL_WAY_STATION) {
                    this.transport_point_titles.railway_title = this.$refs['railway_title_' + town_id][0].value;
                }
                if (tp_type === this.$config.TYPE_AUTO) {
                    this.transport_point_titles.auto_title = this.$refs['auto_title_' + town_id][0].value;
                }

                axios.post(this.add_link_transport_point, {
                    _csrf:this._csrf,
                    transport_point_titles: this.transport_point_titles,
                    tp_type: tp_type,
                    town_id: town_id
                }).then(response => {
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                        this.closeSwal(response);
                        this.createSwal("Ошибка сохранения географической точки", "error", 2000);
                    } else {
                        this.error_list = {};
                        this.add_transport_points = {};
                        this.transport_point_titles = {
                            port_title_en: '',
                            port_title_ru: '',
                            railway_title: '',
                            auto_title: ''
                        };
                        this.closeSwal(response);
                        this.createSwal("Геоографическая точка сохранена", "success", 3000);
                        if (this.query) {
                            this.onSearch();
                        } else {
                            this.getTransportPoints(town_id);
                        }
                    }
                });
            },
            showTowns: function (country) {
                if (!country.towns_loaded) {
                    this.getTowns(country.id);
                    country.towns_loaded = true;
                }
                country.show_towns = !country.show_towns;
            },
            showTransportPoints: async function (town, town_id) {
                if (!town.transport_points_loaded) {
                    await this.getTransportPoints(town_id);
                    town.transport_points_loaded = true;
                }
                town.show_transport_points = !town.show_transport_points;
            },
            showAddAlias: function (town_id, town) {
                if (this.isEditableAliases(town_id)) {
                    this.cancelEditAliasesTown(town_id, town);
                }
                town.show_add_alias = !town.show_add_alias;
            },
            showAddTransportPoint: async function (town, town_id, type_transport_point) {
                if (!town.show_transport_points) { // Если жмем добавить, а город еще не расскрыт.
                    await this.showTransportPoints(town, town_id);
                }
                if (!this.add_transport_points[town_id] || !this.add_transport_points[town_id][type_transport_point]) {
                    this.$set(this.add_transport_points, town_id, {[type_transport_point]: this.transport_points[town_id][this.location_types_titles[type_transport_point]]});
                }
            },
            editAliasesTown: function (town_id, town) {
                if (town.show_add_alias) {
                    this.showAddAlias(town_id, town);
                }
                if (!this.editable_aliases[town_id] || !this.editable_aliases[town_id]['edit']) {
                    town.old_aliases = [];
                    for (let i = 0; i < town.other_aliases.length; i++) {
                        town.old_aliases.push(Object.assign({}, town.other_aliases[i]));
                    }
                    this.$set(this.editable_aliases, town_id, {edit: true});
                }
            },
            isEditableAliases: function(town_id) {
                return (this.editable_aliases[town_id] && this.editable_aliases[town_id].edit);
            },
            isShownTransportPointInExistRow: function(town_id, type_transport_point, index_tp) {
                return this.isAddTransportPoint(town_id, type_transport_point)
                    && (this.add_transport_points[town_id][type_transport_point] === index_tp)
                    && (this.add_transport_points[town_id][type_transport_point] < this.transport_points[town_id]['max_length']);
            },
            isShownTransportPointInNewRow: function(town_id, type_transport_point) {
                return this.isAddTransportPoint(town_id, type_transport_point)
                    && (this.add_transport_points[town_id][type_transport_point] === this.transport_points[town_id]['max_length']);
            },
            isAddTransportPoint:function (town_id, type_transport_point) {
                return (this.add_transport_points[town_id] && (this.add_transport_points[town_id][type_transport_point] !== null));
            },
            cancelEditAliasesTown: function (town_id, town) {
                if(this.editable_aliases[town_id]['edit']) {
                    this.$set(this.editable_aliases, town_id, {edit: false});
                    town.other_aliases = [];
                    for (let i = 0; i < town.old_aliases.length; i++) {
                        town.other_aliases.push(town.old_aliases[i]);
                    }
                }
                this.error_list = {};
            },
            isExistTransportPoint: function (town_id, type_tp, index_tp) {
                return this.transport_points[town_id][type_tp] && this.transport_points[town_id][type_tp][index_tp];
            },
            isExistPortRu: function (town_id, type_tp, index_tp) {
                return this.isExistTransportPoint(town_id, type_tp, index_tp) && this.transport_points[town_id][type_tp][index_tp]['ru'];
            },
            isExistPortEn: function (town_id, type_tp, index_tp) {
                return this.isExistTransportPoint(town_id, type_tp, index_tp) && this.transport_points[town_id][type_tp][index_tp]['en'];
            },
            getErrorText: function(field) {
                if (!this.error_list[field]) {
                    return '';
                }
                for (let model_field in this.error_list[field]) {
                    return this.error_list[field][model_field][0] ? this.error_list[field][model_field][0] : this.error_list[field][model_field];
                }
                return '';
            },
        }
    });
});