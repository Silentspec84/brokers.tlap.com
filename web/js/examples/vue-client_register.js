document.addEventListener("DOMContentLoaded", function() {
    Vue.directive('mask', VueMask.VueMaskDirective);
    const client_register = new Vue({
        el: "#client_register",
        mixins: [listMixins, validatorMixins],
        data: {
            form: {
                first_name: null,
                last_name: null,
                email: null,
                phone: null,
                inn: null,
                client_company_name: null,
                client_company_business_type: 130,
                _csrf: null
            },
            client_company_business_types: [],
            register_link: '/site-api/register',
            error_list: {},
            _csrf: null
        },

        methods: {
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "col-md-12" : "col-md-12  border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            },
            validate: function() {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['first_name', 'last_name', 'phone', 'email', 'client_company_business_type',
                            'inn', 'client_company_name'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    isInn: {
                        fields: ['inn'],
                        message: 'Данное поле должно быть целым числом',
                    },
                    min: {
                        fields: ['client_company_name', 'first_name', 'last_name', 'email'],
                        min: 2,
                    },
                    max: {
                        fields: ['client_company_name', 'first_name', 'last_name', 'email'],
                        max: {client_company_name: 50, first_name: 50, last_name: 50, email: 88},
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                    },
                    phone: {
                        fields: ['phone'],
                        message: 'Неправильный формат телефона (+#(###)###-##-##)',
                    },
                    userName: {
                        fields: ['first_name', 'last_name', 'client_company_name'],
                        message: 'Данное поле должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 2-х символов и больше 50.',
                    },
                };

                return this.validateForm(config, this.form);
            },
            save: function (form) {
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                swal('Идет регистрация пользователя', {
                    buttons: false,
                    icon: 'info',
                    timer: 300000,
                });
                this.form._csrf = this._csrf;
                return axios.post(this.register_link, this.form)
                    .then(response => {
                        this.error_list = {};
                        if (!response.data.is_success) {
                            swal.close();
                            for (let key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[0][key];
                            }
                            swal('Ошибка регистрации!', {
                                buttons: false,
                                icon: 'error',
                                timer: 2000,
                            });
                        } else {
                            swal.close();
                            swal('Регистрация прошла успешно!', {
                                buttons: false,
                                icon: 'success',
                                timer: 3000,
                            });
                            window.location.href = "/site/register-success";
                        }
                    });
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.client_company_business_types = JSON.parse(this.$refs.config.dataset.client_company_business_types);
        }

    });
});
