document.addEventListener("DOMContentLoaded", function() {

    Vue.directive('mask', VueMask.VueMaskDirective);

    Vue.component('employee-list', {
        mixins: [listMixins, validatorMixins],
        template: '#employee-list-template',
        data: function () {
            return {
                email_is_sending: false,
                employee_list: [],
                _csrf: null,
                query: '',
                show_modal: false,
                form: {
                    title: null,
                    is_new: true // если true - добавление нового  менеджера, иначе - редактирование существующего
                },
                new_employee_form: {
                    first_name: '',
                    last_name: '',
                    middle_name: '',
                    email: '',
                    phone: '',
                },
                status_list: [],
                status_filter: null,
                role_list: [],
                status_modes: [],
                error_list: [],
                warning_list: {},
                get_link: '/config-api/get-employee-list',
                add_link: '/config-api/add-employee',
                edit_link: '/config-api/edit-employee',
                drop_link: '/config-api/drop-employee',
                change_status_link: '/config-api/change-employee-status',
                resend_registration_email_link: '/config-api/resend-registration-email'
            };
        },
        methods: {
            showModal: function(employee) {
                if (employee) {
                    if (employee.status === this.$config.STATUS_INACTIVE) {
                        swal({
                            text: 'Данный менеджер не активен! Для редактирования сначала переведите учетную запись данного менеджера в статус "Активный"',
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true,
                        });
                        return;
                    }
                    this.initForm(employee);
                }
                this.form.title = 'Редактировать данные';
                this.form.is_new = false;
                this.show_modal = true;
            },
            initForm: function(employee) {
                this.new_employee_form.id = employee.id;
                this.new_employee_form.first_name = employee.first_name;
                this.new_employee_form.last_name = employee.last_name;
                this.new_employee_form.middle_name = employee.middle_name;
                this.new_employee_form.email = employee.email;
                this.new_employee_form.phone = employee.phone;
            },
            closeModal: function() {
                this.new_employee_form = {
                    first_name: '',
                    last_name: '',
                    middle_name: '',
                    email: '',
                    phone: '',
                };

                setTimeout(() => {
                    this.form.title = 'Добавить';
                }, 200);
                this.form.is_new = true;
                this.show_modal = false;
                this.error_list = [];
            },
            validate: function() {
                this.error_list = {};

                let config = {
                    required: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email', 'phone'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    min: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email'],
                        min: 2,
                    },
                    max: {
                        fields: ['last_name', 'first_name', 'middle_name', 'email'],
                        max: {last_name: 20, first_name: 20, middle_name: 20, email: 30},
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                    },
                    phone: {
                        fields: ['phone'],
                        message: 'Неправильный формат телефона (+#(###)###-##-##)',
                    },
                    userName: {
                        fields: ['first_name', 'last_name', 'middle_name'],
                        message: 'Данное поле должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 2-х символов и больше 88.',
                    },
                };

                return this.validateForm(config, this.new_employee_form);
            },
            load: function() {
                this.createSwal("Идет загрузка данных...", "info", 60000);
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.get_link, {
                    _csrf: this.$refs.configs.dataset.csrf
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if (!response.data.is_success) {
                        this.error_list = {};
                        for (var key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                        swal.close();
                    } else {
                        this.employee_list = response.data.content;
                        for(let i = 0; i < this.employee_list.length; i++) {
                            this.employee_list[i]['shown'] = true;
                        }
                        this.warning_list = response.data.warnings;
                        swal.close();
                    }
                })
                    .catch(e => {
                        this.error_list = {};
                        for (var key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                    });
            },
            getFilteredList: function() {
                let result = [];
                for (let i = 0; i < this.employee_list.length; ++i) {
                    let employee = this.employee_list[i];
                    if (this.status_filter == null || this.status_filter == employee.status) {
                        result.push(this.employee_list[i]);
                    }
                }
                return result;
            },
            getEmployeeStatuses: function() {
                let result = [{value: null, title: "Все значения"}];
                for (let key in this.status_list) {
                    result.push({value: key, title: this.status_list[key]});
                }
                return result;
            },
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "col-md-12" : "col-md-12  border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            },
            showDropDialog: function(employee) {
                var self = this;
                if (employee.role !== this.$config.ROLE_ADMIN) {
                    swal({
                        className: "drop-clients",
                        title: "Удалить позицию?",
                        text: "Удалённая информация не может быть восстановлена",
                        buttons: {
                            confirm: {
                                text: "Удалить",
                                value: true,
                                visible: true,
                                className: "btn btn-danger",
                                closeModal: true
                            },
                            cancel: {
                                text: "Отмена",
                                value: null,
                                visible: true,
                                className: "btn btn-secondary",
                                closeModal: true,
                            }
                        }
                    }).then((willDelete) => {
                        if (willDelete) {
                            self.dropItem(employee.id);
                        }
                    });
                }
                if (employee.role === this.$config.ROLE_ADMIN) {
                    swal({
                        text: 'Главный менеджер не может быть удален!',
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true,
                    });
                }

            },
            getTableHeads: function() {
                return {
                    last_name: 'Фамилия',
                    first_name: 'Имя',
                    middle_name: 'Отчество',
                    email: 'e-mail',
                    phone: 'Телефон',
                    role: 'Роль',
                    status: 'Статус',
                    action: 'Действие',
                    actions: ' '
                }
            },
            getNotSortableColumns: function () {
                return {action: 'Действие', actions: ' '};
            },
            getCustomValuesToSortedColumns: function () {
                return {status: this.status_list, role: this.role_list}
            },
            addEmployee: function() {
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить! Заполните выделенные красным поля.", "error", 3000);
                    return;
                }
                var self = this;
                self.createSwal("Идет сохранение", "info", 60000);
                delete self.new_employee_form.status;
                delete self.new_employee_form.role;
                delete self.new_employee_form.shown;
                self.new_employee_form._csrf = self._csrf;
                var link = !self.form.is_new ? self.edit_link : self.add_link;
                return axios.post(link, self.new_employee_form)
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                self.createSwal(response.data.errors[0], "error", 3000);
                                return;
                            }
                            for (var key in response.data.errors[0]) {
                                self.error_list[key] = response.data.errors[0][key];
                            }
                            self.createSwal("Невозможно сохранить заявку! Заполните выделенные красным поля.", "error", 3000);
                            self.show_modal = true;
                        } else {
                            self.createSwal("Сохранено", "success", 1000);
                            self.error_list = {};
                            self.show_modal = false;
                            self.new_employee_form = {};
                            self.form.title = 'Добавить сотрудника';
                            self.form.is_new = true;
                            self.load();
                        }
                    });
            },
            changeEmployeeStatus: function(id, status) {
                let self = this;
                let title = status === this.$config.STATUS_INACTIVE ? "Активировать менеджера?" : "Деактивировать менеджера?";
                let text = status === this.$config.STATUS_INACTIVE ? "После этого менеджер получит доступ в кабинет" : "После этого менеджер будет лишен доступа в кабинет";
                let button_name = status === this.$config.STATUS_INACTIVE ? "Активировать" : "Деактивировать";
                let button_class = status === this.$config.STATUS_INACTIVE ? "btn btn-primary" : "btn btn-danger";
                if (employee.role !== this.$config.ROLE_ADMIN) {
                    swal({
                        className: "drop-clients",
                        title: title,
                        text: text,
                        buttons: {
                            confirm: {
                                text: button_name,
                                value: true,
                                visible: true,
                                className: button_class,
                                closeModal: true
                            },
                            cancel: {
                                text: "Отмена",
                                value: null,
                                visible: true,
                                className: "btn btn-secondary",
                                closeModal: true,
                            }
                        }
                    }).then((willDelete) => {
                        if (willDelete) {
                            self.createSwal("Идет сохранение", "info", 60000);
                            return axios.post(self.change_status_link, {_csrf: self._csrf, id: id})
                                .then(function (response) {
                                    if (!response.data.is_success) {
                                        self.error_list = {};
                                        if (typeof response.data.errors[0] === "string") {
                                            self.createSwal(response.data.errors[0], "error", 3000);
                                        }
                                    } else {
                                        self.createSwal("Сохранено", "success", 3000);
                                        swal.close();
                                        self.load();
                                    }
                                });
                        }
                    });
                }
            },
            resendRegistrationEmail: function(id) {
                if (this.email_is_sending) {
                    return;
                }
                this.email_is_sending = true;
                var self = this;
                self.createSwal("Идет отправка письма", "info", 60000);
                return axios.post(self.resend_registration_email_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                self.createSwal(response.data.errors[0], "error", 3000);
                            }
                            self.email_is_sending = false;
                        } else {
                            self.createSwal("Готово!", "success", 3000);
                            swal.close();
                            self.email_is_sending = false;
                        }
                    });
            },
            getStatusClass: function(status) {
                let css_style = status ? this.status_modes[status] : '';
                let mode = status ? 'badge-' + css_style  : '';
                return 'badge ' + mode + ' text-white small py-1 px-2';
            },
            getStatusTitle: function(status) {
                return status ? this.status_list[status] : '';
            },
            getRoleTitle: function(role) {
                return role ? this.role_list[role] : '';
            },
            getAction: function (employee) {
                if (employee.role === this.$config.ROLE_ADMIN) {
                    return '<span>Не доступно</span>';
                }
                if (employee.status === this.$config.STATUS_ACTIVE) {
                    return '<span class="btn btn-link btn-sm p-0">Деактивировать</span>';
                }
                if (employee.status === this.$config.STATUS_UNCONFIRMED) {
                    return '<span class="btn btn-link btn-sm p-0" data-toggle="tooltip" data-placement="left" ' +
                        'title="Повторно выслать письмо со ссылкой на подтверждение email адреса сотрудника">Выслать повторно</span>';
                }
                if (employee.status === this.$config.STATUS_INACTIVE) {
                    return '<span class="btn btn-link btn-sm p-0">Активировать</span>';
                }
            },
            makeAction: function (employee) {
                if (employee.role === this.$config.ROLE_ADMIN) {
                    return;
                }
                if (employee.status === this.$config.STATUS_ACTIVE || employee.status === this.$config.STATUS_INACTIVE) {
                    this.changeEmployeeStatus(employee.id, employee.status);
                }
                if (employee.status === this.$config.STATUS_UNCONFIRMED) {
                    this.resendRegistrationEmail(employee.id);
                }
            },
            createSwal: function (title, icon_style, timer) {
                swal(title, {
                    buttons: false,
                    icon: icon_style,
                    timer: timer,
                });
            },
            closeSwal: function() {
                swal.close();
            },
        },
        mounted: function() {
            this.load();
            this._csrf = this.$refs.configs.dataset.csrf;
            this.form.title = 'Добавить сотрудника';
            this.form.is_new = true;
            this.status_list = JSON.parse(this.$refs.configs.dataset.status_list);
            this.role_list = JSON.parse(this.$refs.configs.dataset.role_list);
            this.status_modes = JSON.parse(this.$refs.configs.dataset.status_modes);
        }
    });

    new Vue({
        el: '#employee'
    });
});