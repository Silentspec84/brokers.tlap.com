"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('offer-list', {
        mixins: [listMixins, dateMixins],
        template: '#offer-list-template',
        data: function () {
            return {
                offers: [],
                filters_checkboxes: null,
                query: '',
                show_modal: false,
                archive_candidates: [],
                warning_list: {},
                error_list: {},
            };
        },
        methods: {
            onSearch(query) {
                this.query = query;
            },
            onChangeFilters(data) {
                this.filters_checkboxes.forEach((el, i) => {
                    if (el.title === data) {
                        this.filters_checkboxes[i].is_active = true;
                    } else {
                        this.filters_checkboxes[i].is_active = false;
                    }
                });
                this.load();
            },
            archiveModal: function(id) {
                this.archive_candidates = [];
                if(id === undefined) {
                    for(let i = 0; i < this.offers.length; i++) {
                        if(this.offers[i]['checked']) {
                            this.archive_candidates.push(this.offers[i].id);
                        }
                    }
                }
                else {
                    this.archive_candidates.push(id);
                }
                this.show_modal = true;
            },
            archive: function() {
                axios.post('/client-offer-api/archive', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    ids: this.archive_candidates
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    console.log(response.data);
                    if(response.data.is_success) {
                        this.load();
                    }
                }).catch(e => {
                    console.warn('archiving error');
                });
                this.show_modal = false;
                this.load();
            },
            getTableHeads: function() {
                return {
                    id: '№ КП',
                    bets: 'Кол-во ставок',
                    date_sent_to_client: 'Получено',
                    date_till: 'Срок действия',
                    manager_name: 'Менеджер'
                }
            },
            load() {
                axios.post('/client-offer-api/list', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    filters_checkboxes: this.filters_checkboxes
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if (response.data.is_success) {
                        this.error_list = {};
                        this.offers = response.data.content;
                        for(let i = 0; i < this.offers.length; i++) {
                            this.offers[i]['shown'] = true;
                        }
                        this.warning_list = response.data.warnings;
                    } else {
                        this.error_list = response.data.errors;
                    }
                })
                    .catch(e => {
                        console.warn('cant load dates');
                    });
            },
        },
        mounted: function () {
            this.load();
            this._csrf = this.$refs.configs.dataset.csrf;
            this.filters_checkboxes = JSON.parse(this.$refs.configs.dataset.filters_checkboxes);
        }
    });


    //index page
    new Vue({
        el: '#offer'
    });
});
