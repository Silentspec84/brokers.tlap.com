"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#table-data",
        mixins: [listMixins],
        data: {
            _csrf: null,
            title_fields: [],
            rows: [],
            has_duplicate: false,
            date_loaded: null,
            error_list: '',
            get_link: null,
            message: 0 //0 - in process, 1 - success, 2 - error
        },
        methods: {
            isDuplicate: function (row) {
                if(row['is_duplicate']) {
                    return 'alert alert-warning';
                }
            }
        },
        mounted: function () {
            var dataset = this.$refs.config.dataset;
            this.get_link = dataset.get_link;
            axios.get(this.get_link).then(response => {
                if (response.data.is_success) {
                    this.message = response.data.is_success ? 1 : 2;
                    this.title_fields = response.data.content.title_fields;
                    this.rows = response.data.content.rows;
                    this.has_duplicate = response.data.content.has_duplicate;
                    this.date_loaded = response.data.content.date_loaded;
                } else {
                    this.message = 2;
                    this.error_list = response.data.errors[0];
                }
            }).catch(e => {
                this.message = 2;
            });
        }
    });
});
