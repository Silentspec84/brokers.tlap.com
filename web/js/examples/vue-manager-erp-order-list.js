"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('erp-order-list', {
        mixins: [listMixins, erpMixins],
        template: '#erp-order-list-template',
        methods: {
            archiveModal: function(id) {
                this.archive_candidates = [];
                if(id === undefined) {
                    for(let i = 0; i < this.orders.length; i++) {
                        if(this.orders[i]['checked']) {
                            this.archive_candidates.push(this.orders[i].id);
                        }
                    }
                }
                else {
                    this.archive_candidates.push(id);
                }
                this.show_modal = true;
            },
            archive: function() {
                axios.post('/manager-erp-api/set-archive', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    ids: this.archive_candidates
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(response.data.is_success) {
                        this.loadData();
                    }
                }).catch(e => {
                    console.warn('archiving error');
                });
                this.show_modal = false;
            },
            sortbyWithPaginator: function(order, key) {
                this.loadData( this.get_link, {sort_direction: order*-1, field_order:key});
            },
        }
    });


    //index page
    new Vue({
        el: '#erp-order'
    });
});
