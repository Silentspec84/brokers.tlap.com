"use strict";

/**
 * Показывает статус заявки.
 */
Vue.component('bid-status', {
    template: `<span :class="getClass()">{{title}}</span>`,
    props: {
        title: {
            type: String,
            default: ''
        },
        status: '',
        statusList: '',
        statusModes: '',
    },
    methods: {
        getClass: function() {
            let mode = this.status ? 'bg-' + this.class  : '';
            return 'd-inline-block ' + mode + ' text-white small py-1 px-2';
        }
    },
    mounted: function() {
        let list = JSON.parse(this.statusList);
        let modes = JSON.parse(this.statusModes);
        this.title = this.status ? list[this.status] : '';
        this.class = this.status ? modes[this.status] : '';
    }
});
