"use strict";

/**
 * Показывает статус КП, с возможностью изменить этот статус.
 */
Vue.component('offer-status-editable', {
    template: `
    <div class="dropdown d-inline-block">
        <span :class="curClass" data-toggle="dropdown">{{curTitle}}</span>
        <div class="dropdown-menu">
            <a v-for="item in filteredList" v-on:click.prevent="changeStatus(item.id)" class="dropdown-item" href="#">
                {{item.title}}
            </a>
        </div>
    </div>`,
    props: {
        status: '',
        offerId: null,
        csrf: '',
        statusList: '',
        statusModes: '',
    },
    data: function() {
        return {
            statuses: []
        };
    },
    computed: {
        filteredList: function() {
            return this.statuses.filter(item => item.id != this.status);
        },
        curStatus: function() {
            return this.statuses.filter(item => item.id == this.status).pop();
        },
        curClass: function() {
            let mode = this.curStatus ? 'bg-' + this.curStatus.mode : '';
            return 'd-inline-block ' + mode + ' width-xl text-white text-center p-1';
        },
        curTitle: function() {
            return this.curStatus ? this.curStatus.title : '';
        },
    },
    methods: {
        changeStatus: function(status_id) {
            axios.post('/manager-offer-api/change-status', {
                _csrf: this.csrf,
                id: this.offerId,
                status: status_id,
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(response => {
                if(response.data.is_success) {
                    this.$emit('update');
                }
            }).catch(e => {
                console.warn(e);
            });
        },
    },
    mounted: function() {
        let list = JSON.parse(this.statusList);
        let modes = JSON.parse(this.statusModes);
        for(let item in list) {
            this.statuses.push({id: item, title: list[item], mode: modes[item]});
        }
    }
});
