"use strict";

document.addEventListener("DOMContentLoaded", function() {

    const uploader_form = {
        components: {vuejsDatepicker},
        template: '#uploader-form-template',
        data: function() {
            return {
                max_file_size: 60000000,
                acceptable_types: [
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-excel',
                    'application/vnd.oasis.opendocument.spreadsheet'
                ],
                filename: '',
                errors: [],
                active: 'form',
                dates:  [
                    { key: 'date_upload_since', value: '', label: 'Действителен с: ' },
                    { key: 'date_upload_by', value: new Date(), label: 'по: ' },
                ],
                table_id_selected: 0,
                data_list: [],
                tables_valid: true,
                calendar_is_openned: false,
                type_update_id_selected: 0,
                types_update: [],
            };
        },
        mounted: function () {
            if (this.$store.state.upload_error.length > 0) {
                this.errors.push(this.$store.state.upload_error);
            }
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            let get_date_link = '/data-upload-api/get-dates';
            axios.get(get_date_link)
                .then(response => {
                    if(response.data.is_success) {
                        this.dates[0].value = new Date(response.data.content.date_upload_since);
                        this.dates[1].value = new Date(response.data.content.date_upload_by);
                    }
                }).catch(e => {
                console.warn('cant load dates');
            });

            let get_link = '/data-upload-api/get-tables-list';
            axios.get(get_link)
                .then(response => {
                    if(!response.data.is_success) {
                        this.errors = this.errors.concat(response.data.errors);
                        this.active = 'form';
                        this.tables_valid = false;
                        return;
                    }
                    this.data_list = response.data.content.data_list;
                    this.types_update = response.data.content.types_update;
                    this.table_id_selected = this.data_list[0].id;
                    this.type_update_id_selected = this.types_update[0].id;
                }).catch(e => {
                    this.tables_valid = false;
                });
        },
        computed: {
            file_types: function() {
                return this.acceptable_types.join(',');
            },
            errors: function() {
                return this.errors.filter(item => item.length > 0);
            }
        },
        methods: {
            loadTable: function(e) {
                this.errors = [];
                var file = document.getElementById('file_content').files[0];
                if(!file) {
                    this.errors.push('Вы не выбрали файл');
                    return;
                }
                if(this.acceptable_types.indexOf(file.type) === -1) {
                    this.errors.push('Неподдерживаемый тип файла');
                    return;
                }
                if(file.size > this.max_file_size) {
                    this.errors.push('Недопустимый размер файла');
                    return;
                }

                var form_data = new FormData();
                form_data.append('document', file);
                form_data.append('table_id', this.table_id_selected);
                form_data.append('save_mode', this.type_update_id_selected);

                for(var date in this.dates) {
                    form_data.append(this.dates[date].key, this.getTimestamp(this.dates[date].value));
                }

                form_data.append('_csrf', this.$store.getters.csrf);

                this.filename = file.name;
                this.active = 'process';
                axios.post('/data-upload-api/create', form_data, {
                    headers: {
                        'Content-Type': 'multipart/form-data; boundary=' + Date.now(),
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(!response.data.is_success) {
                        this.errors = this.errors.concat(response.data.errors);
                        this.active = 'form';
                        return;
                    }
                    this.active = 'ok';
                    this.$router.push('/data-upload/match/');
                }).catch(e => {
                    this.errors.push('Произошла ошибка, попробуйте повторить попытку');
                    this.active = 'form';
                });
            },
            getDate: function(date) {
                return moment(date).format('DD.MM.YYYY');
            },
            getTimestamp: function(date) {
                return Math.floor(date.getTime() / 1000);
            },
            getClass: function(field) {
                return 'form-control form-control-sm';
            },
            inputDisabled: function(date_id) {
                this.calendar_is_openned = true;
                $(date_id).prop('disabled',true);
            },
            inputWrite: function(date_id) {
                this.calendar_is_openned = false;
                $(date_id).prop('disabled',false);
            },
            saveCorrectDate: function(date_id, index) {
                if (!this.calendar_is_openned) {
                    var data_string = document.getElementById(date_id).value;
                    var date_input = new Date(data_string.replace(/(\d+)\.(\d+)\.(\d+)/, '$3-$2-$1'));
                    this.dates[index].value = date_input;
                }

                if ( isNaN(this.dates[index].value.getTime()) ) {
                    this.dates[index].value = new Date();
                }
            },
            isErrors: function () {
                console.log(this.errors);
                return this.errors.length > 0;
            }
        }
    };

    const data_matching = {
        template: '#matching-form-template',
        props: {
            _csrf: null,
            table_data: [],
            column_keys: [],
            select_options: [],
            select: [],
            errors: [],
        },
        mounted: function () {
            axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            try {
                var dataset = this.$refs.config.dataset;
                this._csrf = dataset.csrf;
                this.save_link = dataset.save_link;
                this.load_link = dataset.load_link;
                this.cache_link = dataset.cache_link;
                this.loadData();
                this.errors = [];
            }
            catch(e) {
                console.warn(e);
            }
        },

        methods: {
            loadData: function() {
                console.log('loading...');
                axios.get(this.load_link).then(response => {
                    if(!response.data.is_success && response.data.errors.length) {
                        this.$store.commit('setUploadError', response.data.errors.pop());
                        this.$router.push('/data-upload/');
                    }
                    this.column_keys = response.data.content.column_keys;
                    this.table_data = response.data.content.excel_data;
                    this.select_options = response.data.content.select_options;
                    this.select = response.data.content.select;
                    this.table_id = response.data.content.table_id;
                    this.selectValidate();
                }).catch(e => {
                    console.warn(e);
                });
            },

            saveAll: function () {

                if (this.errors.length > 0) {
                    return;
                }

                var table = this.table_data;
                var matched_fields = this.select;
                var fields = [];
                var field_key;
                var field_val;

                for (var row_number in this.table_data) {
                    if (table.hasOwnProperty(row_number)) {
                        for (var col_number in table[row_number]) {
                            field_key = matched_fields[col_number];
                            if (field_key !== '0') {
                                field_val = table[row_number][col_number];
                                fields.push({field_id: field_key, field_val: field_val});
                            }
                        }
                    }
                }

                return axios.post(this.save_link, {'_csrf' : this._csrf, 'fields': fields})
                    .then(response => {
                        if (response.data.is_success === true) {
                            window.location.href = '/data-upload/check-document/';
                        }
                    })
                    .catch(error => {
                        console.log(error.response); // todo вывести красиво ошибку пользователю CH-709
                    });
            },

            selectValidate: function () {
                this.errors = [];
                let matched_fields = [];
                let error = {};
                let cnt_choosen_fields = 0;
                for (let i = 0; i < this.column_keys.length; i++) {

                    if (typeof this.select[i] === 'undefined') {
                        error.key = i;
                        error.val = 'Не выбрано значение поля';
                        this.errors.push(error); // не выбрано значение
                    } else {
                        if (this.select[i] == 0) { // выбрано "не учитывать" - 0
                            continue;
                        }
                        cnt_choosen_fields++;
                        let val = +this.select[i];
                        if (matched_fields.indexOf(val) !== -1) {
                            error.key = i;
                            error.val = 'Нельзя сопоставить одно поле нескольким столбцам';
                            this.errors.push(error); // дубликат
                        }
                        matched_fields.push(val);
                    }
                }

                if (cnt_choosen_fields === 0) {
                    error.key = 0;
                    error.val = 'Нужно сопоставить хотя бы 1 поле';
                    this.errors.push(error);
                }
            },

            getCssClassForSelect: function (id) {

                let filtred_errors = this.errors.filter(function(error) {
                    return error.key == id;
                });

                if (filtred_errors.length > 0) {
                    return 'is-invalid';
                }

                return '';
            },

            getError: function (id) {
                $('[data-toggle="tooltip"]').tooltip();

                let filtred_errors = this.errors.filter(function(error) {
                    return error.key == id;
                });

                if (filtred_errors.length > 0) {
                    return filtred_errors[0].val;
                }

                return '';
            },

            getOptionValue: function (options) {
                return options[1];
            },
            setSelectToCache: function (key) {
                console.log('cache...');
                this.selectValidate();

                let error = {};
                let form_data = {
                    '_csrf': this._csrf,
                    'key': key,
                    'table_id': this.table_id,
                    'val': this.select[key]
                };
                axios.post(this.cache_link, form_data).then(response => {
                    if (response.data.errors.length > 0) {
                        error.key = key;
                        error.val = response.data.errors[0];
                        this.errors.push(error);
                    }
                }).catch(e => {
                    console.log(e);
                });

            }
        }
    };

    const document_checker = {
        template: '#document-checker-template',
        props: {
            filename: String,
            location_field_list: {

        },

        columns: {
            type: Array,
            default: []
        },
        data: {
            type: Array,
            default: []
        },
        checked_all: {
            type: Boolean,
            default: true
        },
        filter_mode: {
            type: String,
            default: ''
        }
    },
    computed: {
        strings_for_save_cnt: function() {
            return this.data.filter(field => field.checked).length;
        },
        doubles_cnt: function() {
            return this.doubles.length;
        },
        errors_cnt: function() {
            return !this.data.length ? 0 : this.data.reduce((total, field) => {
                if(field.items.some(item => item.error)) {
                    total++;
                }
                return total;
            }, 0);
        },
        find_text: function() {
            let cnt = this.strings_for_save_cnt;
            return this.endOfNum(cnt, ['сохранена', 'сохранено', 'сохранено']);
        },
        str_text: function() {
            let cnt = this.strings_for_save_cnt;
            return this.endOfNum(cnt, ['строка', 'строки', 'строк']);
        },
        error_text: function() {
            return this.endOfNum(this.errors_cnt, ['ошибка', 'ошибки', 'ошибок']);
        },
        doubles: function() {
            return;
        }
    },
    methods: {
        findCell: function(id) {
            for (let i in this.data) {
                for (let j in this.data[i].items) {
                    if(this.data[i].items[j].id === id) {
                        return this.data[i].items[j];
                    }
                }
            }
        },
        locationChange: function(data) {
            let location = {
                id: null,
                title: null,
                type: null,
            };
            let alias_location = {
                id: null,
                title: null
            };
            const cell = this.findCell(data.field_title);
            if (!data.item) {
                this.updateCell(cell);
                return;
            }
            location.id = data.item.id;
            location.type = data.item.type.type;
            alias_location.title = location.title = data.item.title;
            alias_location.id = data.item.alias_id;

                cell.value.raw_value = data.item.title;
                cell.value.location = location;
                cell.value.location_alias = alias_location;
                this.updateCell(cell);
            },
            onLocationsSearch: function(query, item_id, link) {
                let get_data = [];
                if (query) {
                    get_data.push('query=' + query );
                }
                if (item_id) {
                    get_data.push('item_id=' + item_id );
                }
                let result = get_data.join('&');
                link = link + result;

                axios.get(link)
                    .then(function (response) {
                        disableLoader(title);
                        return response.data.content;

                    }).catch(function () {
                });
            },
            getErrorLocation: function (cell) {
                if (!cell.error || typeof cell.value !== "object") {
                    return '';
                }

                return 'Такой гео-точки не найдено! <a href="/geo-points/">Создать гео-точку</a>';
            },

            saveData: function() {
                let form_data = {
                    '_csrf': this.$store.getters.csrf,
                    'items': this.data
                };

                let swal_cont = document.createElement("div");
                swal_cont.innerHTML = "<i class=\"fa fa-circle-o-notch fa-spin\"></i>";
                swal({
                    text: "Сохраняем...",
                    buttons: false,
                    content: swal_cont
                });

                axios.post('/data-upload-api/check', JSON.stringify(form_data), {
                    headers: {
                            'Content-Type': 'application/json',
                            'X-Requested-With': 'XMLHttpRequest'
                        }
                    }).then(response => {

                        if(response.data.is_success && response.data.content.is_valid) {
                            this.$router.push('/data-upload/finish/');
                        }
                        else {
                            this.loadData();
                        }

                        swal.close();
                    }).catch(e => {

                    console.log(e);
                });
            },
            goBack: function() {
                this.$router.push('/data-upload/');
            },
            toggleAll: function() {
                let self = this;
                this.checked_all = !this.checked_all;
                if (!this.checked_all) {
                    this.data.map(field => field['checked'] = false);
                } else {
                    this.data.map(field => field['checked'] = self.rowHasNoErrors(field.items));
                }
            },
            filterDoubles: function() {
                this.filter_mode = this.filter_mode == 'doubles' ? '' : 'doubles';
            },
            filterErrors: function() {
                this.filter_mode = this.filter_mode == 'errors' ? '' : 'errors';
            },
            filtered: function(data) {
                if(this.filter_mode == 'errors') {
                    return data.filter(field => field.items.some(item => item.error));
                }
                if(this.filter_mode == 'doubles')
                    return this.doubles;
                return data;
            },

            rowHasNoErrors(row) {
                let has_no_errors = true;
                for(let i = 0; i < row.length; ++i) {
                    if (row[i].error) {
                        return false;
                    }
                }

                return has_no_errors;
            },

            updateCell: function(cell) {
                var form_data = new FormData();
                form_data.append('id', cell.id);
                if (typeof cell.value === "object" && cell.value !== null) {
                    const location = JSON.stringify(cell.value);
                    form_data.append('value', location);
                } else {
                    form_data.append('value', cell.value);
                }

                form_data.append('_csrf', this.$store.getters.csrf);
                cell.error = false;
                axios.post('/data-upload-api/update-cell', form_data, {headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }}).then(response => {
                if (response.data.errors.length > 0) {
                    cell.error = true;
                } else {
                    cell.value = response.data.content.value;
                }
                }).catch(e => {
                    console.log(e);
                });
                cell.edited = true;
                cell.edit = false;
                this.$emit('update');
            },
            loadData: function() {
                console.log('loading...');
                axios.post('/data-upload-api/document', {'_csrf': this.$store.getters.csrf}, {headers: {
                    'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                }}).then(response => {
                    this.filename = response.data.content.document_name;
                    this.data = [];
                    this.columns = response.data.content.columns;
                    this.location_field_list = response.data.content.location_field_list;
                    response.data.content.rows.forEach(item => {
                        let row = item.map(elem => new Object({
                            value: elem.value, id: elem.id, error: elem.error, edit: false, edited: false, prev_value: '',
                            location: elem.location

                        }));

                        this.data.push({checked: this.rowHasNoErrors(row), items: row});
                    });
                    this.$nextTick(function () {
                        $('[data-toggle="tooltip"]').tooltip({
                            placement: 'right',
                            delay: { show: 300, hide: 1000 }
                        });
                    });
                }).catch(e => {
                    console.log(e);
                });
            },
            endOfNum: function (number, titles) {
                let cases = [2, 0, 1, 1, 1, 2];
                return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
            }
        },
        mounted: function() {
            this.loadData();
        }
    };

    const upload_finish = {
        template: '#uploader-finish',
        props: {
            message: {
                //0 - in process, 1 - success, 2 - error
                type: Number,
                default: 0
            },
            title_fields: {
                type: Array,
                default: []
            },
            rows_with_index: {
                type: Array,
                default: []
            },
            has_duplicate: {
                type: Boolean,
                default: false
            }
        },
        methods: {
            isDuplicate: function (row) {
                if(row['is_duplicate']) {
                    return 'alert alert-warning'
                }
            }
        },
        mounted: function() {
            axios.post('/data-upload-api/finish', {_csrf: this.$store.getters.csrf}, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(response => {
                this.message = response.data.is_success ? 1 : 2;
                if(response.data.is_success) {
                    this.title_fields = response.data.content.title_fields;
                    this.rows_with_index = response.data.content.rows_with_index;
                    this.has_duplicate = response.data.content.has_duplicate;
                }
            }).catch(e => {
                this.message = 2;
            });
        }
    };

    const routes = [
        {path: '/data-upload/', component: uploader_form},
        {path: '/data-upload/match', component: data_matching},
        {path: '/data-upload/check-document', component: document_checker},
        {path: '/data-upload/finish', component: upload_finish},
    ];
    const router = new VueRouter({
        mode: 'history',
        routes: routes,
        linkExactActiveClass: 'btn-secondary'
    });

    const store = new Vuex.Store({
        state: {
            upload_error: ''
        },
        getters: {
            csrf: state => document.getElementById('loader_config').dataset.csrf,
        },
        mutations: {
            setUploadError: function(state, message) {
                state.upload_error = message;
            }
        }
    });
    Vue.use(Vuex);

    new Vue({
        el: '#uploader',
        store,
        router: router
    });
});
