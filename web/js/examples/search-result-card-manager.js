"use strict";

Vue.component('search-result-card-manager', {
    template: `
        <div class="card search-result-card" :class="{opened: details_shown}" v-show="params.shown">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-2 text-md-right">
                        <div class="h3 mb-2">
                            <strong>{{priceTotal}}</strong>
                            <div class="price-detail">{{priceDetail}}</div>
                        </div>
                        <button v-if="canAddtoCart()"
                                type="button"
                                class="btn btn-primary btn-sm mb-2 btn-to-cart"
                                :class="{'btn-success': params.disabled}"
                                @click="addOrRemoveToCart">
                            <span v-if="!params.disabled">В корзину</span>
                            <span v-else>
                                <span class="btn-label"><i class="mdi mdi-check-all"></i></span> Добавлено
                            </span>
                        </button>
                        <button v-else 
                                type="button"
                                data-toggle="tooltip"
                                data-original-title="Чтобы добавить данные в расчет заполните поле «Наименование клиента»‎"
                                class="btn btn-secondary btn-sm mb-2 btn-to-cart"
                                @mouseover="toggleTooltip()">
                             В корзину
                        </button>
                        <div class="text-md-right mt-2">Срок действия:<br>{{fields.DTAC}}</div>
                    </div>
                    
                    <div class="col-lg-7 pl-4">
                        <div class="h3"><strong>порт {{fields.PSIN}} &mdash; {{fields.DELI}}</strong></div>
                        <div class="text-big mb-2">
                            Груз: <span class="text-dark">{{fields.TCON}}" &times; 1, <24т.</span>
                            В пути: <span class="text-dark">25 дней</span>
                        </div>
                        <div class="mt-1 card-order-route">
                            <i class="mdi mdi-ferry border rounded-circle p-1 mr-1"></i>
                            <span class="text-big text-dark">порт {{fields.PSIN}} &mdash; порт {{fields.POUT}}</span> 5 дней, {{fields.LINE}}
                            <table class="table card-details-table table-borderless ml-4" v-if="details_shown">
                                <tr>
                                    <td style="width: 150px;">Фрахт: </td>
                                    <td>$ {{fields.FRAH}}</td>
                                </tr>
                                <tr>
                                    <td>Drop: </td>
                                    <td>$ {{fields.DROP}}</td>
                                </tr>
                                <tr>
                                    <td>Агентские: </td>
                                    <td>$ {{fields.AGEN}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Подытог: </strong></td>
                                    <td><strong>$ {{fields.PIFR}}</strong></td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-1 card-order-route">
                             <i class="mdi mdi-train border rounded-circle p-1 mr-1"></i>
                            <span class="text-big text-dark">порт {{fields.POUT}} &mdash; {{fields.DELI}} {{fields.SOUT}}</span> 5 дней
                            <table class="table card-details-table table-borderless ml-4" v-if="details_shown">
                                <tr>
                                    <td style="width: 150px;">ЖД: </td>
                                    <td>{{fields.RWTF}} ₽</td>
                                </tr>
                                <tr>
                                    <td>Охрана: </td>
                                    <td>{{this.params[this.security_field_id]}} ₽</td>
                                </tr>
                                <tr>
                                    <td>ВТТ: </td>
                                    <td>{{fields.VTTT}} ₽</td>
                                </tr>
                                <tr>
                                    <td>Спец. тариф: </td>
                                    <td>{{this.params[this.sptr_field_id]}} ₽</td>
                                </tr>
                                <tr>
                                    <td>Оп. груз: </td>
                                    <td>{{this.params[this.dang_field_id]}} ₽</td>
                                </tr>
                                 <tr>
                                    <td>Агентские: </td>
                                    <td>{{fields.AGIP}} ₽</td>
                                </tr>
                                <tr>
                                    <td><strong>Подытог: </strong></td>
                                    <td><strong>{{this.params[this.pirw_field_id]}}</strong></td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-1 card-order-route">
                            <i class="mdi mdi-truck border rounded-circle p-1 mr-1"></i>
                            <span class="text-big text-dark">{{fields.DELI}} {{fields.SOUT}} &mdash; {{fields.DELI}}</span> 5 дней
                            <table class="table card-details-table table-borderless ml-4" v-if="details_shown">
                                <tr>
                                    <td style="width: 150px;">Раскредитация: </td>
                                    <td>{{fields.RASC}} ₽</td>
                                </tr>
                                <tr>
                                    <td>Авто: </td>
                                    <td>{{fields.AUTO}} ₽</td>
                                </tr>             
                                <tr>
                                    <td><strong>Подытог: </strong></td>
                                    <td><strong>{{fields.PIAU}}</strong></td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-2 card-details-toggler" @click="details_shown = !details_shown">
                            <template v-if="!details_shown">Показать детали</template>
                            <template v-else>Скрыть детали</template>
                            <i class="dripicons-chevron-down" :class="{'dripicons-chevron-up': details_shown}"></i>
                        </div>
                    </div>
                    
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-12 mb-4 card-toggler">
                                <div class="badge badge-secondary mr-2" style="display: none;">Самый быстрый</div>
                                <i class="dripicons-chevron-down" @click="details_shown = !details_shown"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <template v-for="item in params.additional_params.hidden_row">
                                    <div class="badge badge-hollow mr-1 mb-1" v-if="item.title == 'Дом. кон-т' && item.field_value == 'Да'">H B/L</div>
                                    <div class="badge badge-hollow mr-1 mb-1" v-if="item.title != 'Дом. кон-т'">{{item.field_value}}</div>
                                </template>
                                <div class="badge badge-hollow mr-1 mb-1">{{custom_type}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    props: {
        params: {
            type: Object,
            default() {
                return {};
            }
        },
        additional_params: {
            type: Object,
            default() {
                return {};
            }
        },
        title_fields: {
            type: Object,
            default() {
                return {};
            }
        },
        is_registered_client_company: {
            type: Boolean,
            default: false
        },
        cart_items_count: {
            type: Number,
            default: 0
        },
        csrf: {
            type: String,
            default: ''
        }
    },
    data() {
        return {
            details_shown: false,
            selected_rows: false,
            custom_type: '',
            cart_action_in_progress: false,
            fields: {},
            security_field_id: 0,
            reso_field_id: 0,
            pirw_field_id: 0,
            dang_field_id: 0,
            sptr_field_id: 0,
        }
    },
    methods: {
        canAddtoCart() {
            return this.additional_params.client_company_id && this.is_registered_client_company;
        },
        addOrRemoveToCart() {
            let rows_result_table = [];
            this.params.checked = true;

            if (this.isRowInCart) {
                this.params.additional_params.cart_tables_data_ids = [];
            }
            rows_result_table.push(this.params);
            if (this.cart_action_in_progress) {
                return;
            }
            this.cart_action_in_progress = true;
            if (this.isRowInCart) { // строка есть в корзине - удаляем
                let is_selected_all_rows = false;

                if (this.cart_items_count === 1) { //удаляется последняя строка
                    is_selected_all_rows = true;
                }
                axios.post('/cart-api/delete-cart-rows', {
                    _csrf:this.csrf,
                    rows: rows_result_table,
                    client_company_id: parseInt(this.additional_params.client_company_id),
                    is_all_rows: is_selected_all_rows,
                    is_delete_from_search: true
                }).then(response => {
                    if (response.data.is_success) {
                        this.$set(this.params, 'disabled', false);
                        this.cart_action_in_progress = false;
                    }
                }).catch(e => {
                    console.log(e);
                });
            } else { // строки нет у корзине - добавляем
                axios.post('/manager-search-api/save-cart', {
                    _csrf:this.csrf,
                    rows: rows_result_table,
                    additional_params: this.additional_params
                }).then(response => {
                    if (response.data.is_success) {
                        this.$set(this.params, 'disabled', true);
                        this.cart_action_in_progress = false;
                    } else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                }).catch(e => {
                    this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                    this.is_response_error = true;
                });
            }
        },
        toggleTooltip() {
            $('[data-toggle="tooltip"]').tooltip();
        },
    },
    created() {
        if (this.additional_params.custom_type === 1) {
            this.custom_type = 'Порт';
        }
        if (this.additional_params.custom_type === 2) {
            this.custom_type = 'Станция';
        }

        //Устанавливаем соответствие между кодами полей и их значениями
        let titles = Object.values(this.title_fields).map((el, i) => {
            // Но из-за этого теряется реактивность, запоминаем id свойств, реактивность которых нужно сохранить,
            // обращаемся к ним через this.params[id]
            if (el.code === 'SECU') {
                this.security_field_id = i + 1;
            }
            if (el.code === 'RWOR') {
                this.reso_field_id = i + 1;
            }
            if (el.code === 'PIRW') {
                this.pirw_field_id = i + 1;
            }
            if (el.code === 'DANG') {
                this.dang_field_id = i + 1;
            }
            if (el.code === 'SPTR') {
                this.sptr_field_id = i + 1;
            }
            return el.code;
        });
        let values = Object.values(this.params).map(el => {
            return el;
        });

        titles.forEach((el, i) => {
            this.fields[titles[i]] = values[i];
        });
    },
    computed: {
        isRowInCart() {
            return this.params.disabled;
        },
        priceDetail() {
            return `₽ ${parseInt(this.fields.AUTO) + parseInt(this.fields.PIRW) + parseInt(this.params[this.security_field_id])} + $ ${parseInt(this.fields.PIFR)}`
        },
        priceTotal() {
            return this.params[this.reso_field_id];
        }
    },
});