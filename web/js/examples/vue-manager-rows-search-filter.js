"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        mixins: [listMixins, rowsSearchFilterMixins],
        el: "#manager_view_search_filter",
        data: {
            link_get_search_rows_by_hash_filter: '/manager-search-filter-api/get-search-rows-by-hash-filter',
            link_get_exception_rows: '/manager-search-filter-api/get-exception-rows',
        },
    });
});