"use strict";
document.addEventListener("DOMContentLoaded", function() {
    Vue.component('client-search-list', {
        mixins: [listMixins, filtersCheckboxesMixins, searchMixins, calendarSettingsMixin],
        template: '#client-search-list-template',
        data: function() {
            return {
                is_get_offers: false,
                // дополнительные параметры, которые не являются поисковыми фильтрами, но влияют на отображение данных в таблице
                additional_params: {
                    weight: '', // если < 24т, отображаем столбец RWAF, иначе - RWBE - не отображается
                    custom_type: "", // если порт - показываем столбец VTTT, иначе - нули вместо него
                    is_security: 0, // если отмечена - показываем столбец SECU, иначе - нули вместо него
                },
                offers_error_list: {},
                offers_warning_list: {},
                offers_title_fields: {},
                rows_offers_table: {},
                offers_formulas: {}, // формулы для подсчета строк входящих КП
                filters_checkboxes: null, // фильтры для КП (активные/архивные)
                get_client_offers_link: "/client-search-api/get-client-offers",
                get_result_table_link: "/client-search-api/get-result-table",
                set_create_bid_link: "/client-bid-api/create-bid",
                set_view_bid_link: "/client-bid/view/",
                save_filters_state_link: "/client-search-api/menu-filters-state-save",
                html_elements: { // Переопределение от autoHeightForGridMixins
                    'main': 'tagName',
                    'client_search': 'id',
                    'search_result': 'id',
                    'grid_table': 'id'
                },
            };
        },

        mounted: function () {
            this.filters_checkboxes = JSON.parse(this.$refs.config.dataset.filters_checkboxes);

            //если поиск, то нам не нужно отдельно загружать КП
            let search_url = this.parseUrl(location.href);
            let search_query = search_url.searchObject;
            if (!search_query.search) {
                this.load();
            }
        },

        methods: {
            /** Сетим необщие(для менеджерской и клиентской поисковой форм) свойства при получение поисковой выдачи */
            setNotCommonProperties(response) {},
            /** В менеджерском поиске, если ты выбрал компанию из dadata, нужно прервать метод searchByButton*/
            async isCompanyRegisterNeeded() {return false;},
            /** Возвращает ссылку для поисковой выдачи по переданному поисковому запросу составленному из get-параметров */
            getSearchLinkBySearchQuery(search_query) {
                return '/client-search/?' + search_query + 'search=1';
            },
            /** Проверяем что фильтр попадает под значения полей скрытой-строки (которая появляется при нажатии выпадашки на строке)*/
            isMatchFilterValueWithHiddenRow(row, filter_key, filter_value) {return false;},

            /** БЛОК Входящих КП полученных от менеджера */
            /** Загрузка входящих КП */
            load() {
                let vue = this;
                this.is_get_offers = true;
                axios.post(this.get_client_offers_link, {_csrf:this._csrf, filters_checkboxes: this.filters_checkboxes}).then(function (response) {
                    vue.offers_warning_list = {};
                    vue.offers_error_list = {};
                    if (response.data.is_success) {
                        vue.offers_warning_list = response.data.warnings;
                        if(vue.isEmptyObject(vue.offers_warning_list)) {
                            vue.offers_title_fields = response.data.content.heads;
                            vue.rows_offers_table =  Object.values(response.data.content.rows);
                            vue.offers_formulas = response.data.content.formulas;
                            vue.currencies = response.data.content.currencies;
                            vue.rows_offers_table = vue.getCalculateRows(vue.offers_title_fields, vue.rows_offers_table, vue.offers_formulas);
                            if(vue.error_list['all_fields']) {
                                vue.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                                vue.is_response_error = true;
                                vue.is_data_loaded = true;
                                return;
                            }
                        }
                    } else {
                        vue.offers_error_list = response.data.errors;
                    }
                    vue.is_data_loaded = true;
                }).catch(e => {
                    vue.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                    vue.is_response_error = true;
                });
            },
            getOffersError(field, id = false) {
                var result = '';

                if (id === false && (field in this.offers_error_list)) {
                    result = this.offers_error_list[field];
                } else if (id in this.offers_error_list && (field in this.offers_error_list[id])) {
                    result = this.offers_error_list[id][field];
                }
                $('[data-toggle="tooltip"]').tooltip();

                return result;
            },
            getOffersWarning(field, id = false) {
                var result = '';
                if (id === false && (field in this.offers_warning_list)) {
                    return this.offers_warning_list[field];
                }
                if (id in this.offers_warning_list && (field in this.offers_warning_list[id])) {
                    return this.offers_warning_list[id][field];
                }

                return result;
            },
            getOffersTableHeads() {
                let heads = {};
                for(let key in this.offers_title_fields) {
                    heads[key] = this.offers_title_fields[key]['title'];
                }
                return heads;
            },
            getOffersTableBody() {
                let rows = {};
                let i = 1;
                for(let key in this.rows_offers_table) {
                    rows[i] = this.rows_offers_table[key];
                    i++;
                }
                return rows;
            },
            sortByPrice(direction) {
                this.rows_result_table.sort((a, b) =>{
                    const x = parseInt(a[13]);
                    const y = parseInt(b[13]);

                    let comparison = 0;
                    if (x > y) {
                        comparison = 1;
                    } else if (x < y) {
                        comparison = -1;
                    }

                    if (direction === 'desc') {
                        return comparison * -1;
                    }

                    return comparison;
                });
            },
            createNewBid(row) {
                this.error_list = {};
                axios.post(this.set_create_bid_link, {_csrf:this._csrf, row: row}).then(response => {
                    if (response.data.is_success) {
                        window.location.replace(this.set_view_bid_link + response.data.content.id);
                    } else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                }).catch(e => {
                    this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                    this.is_response_error = true;
                });
            },
            /** КОНЕЦ БЛОКА Входящих КП полученных от менеджера */
        }
    });

    //index page
    new Vue({
        el: '#client_search'
    });
});
