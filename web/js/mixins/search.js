import {urlMixins} from './url';
import {arrayObjectMixins} from './array-object';
import {formulasMixins} from './formulas';
import {axios} from '../store/axios';

/** Миксин для работы c поиском, используется в менеджерском и клиентском поиске */
export const searchMixins = {
    mixins: [urlMixins, arrayObjectMixins, formulasMixins],
    data() {
        return {
            formError: false,
            formCollapsed: false,
            formSticky: false,
            field_ids_to_field_orders: [], // массив соотношений field_id к order_key, используется например для соотношения фильтров к полям строк
        }
    },
    methods: {
        handleScroll() {
            if (window.pageYOffset >= 250) {
                this.formCollapsed = true;
                this.formSticky = true;
            }
            if (window.pageYOffset <= 150) {
                this.formCollapsed = false;
                this.formSticky = false;
            }
        },
        uncollapse() {
            window.removeEventListener('scroll', this.handleScroll);
            this.formCollapsed = false;
            document.querySelector('.filter-inner').style.top = '12rem';
        },
        fillFormByUrl(search_query) {
            for (let field in this.form_data.additional_params) {
                if (this.form_data.additional_params.hasOwnProperty(field)) {
                    let val = '';
                    if (field !== 'client_company_title') {
                        val = parseInt(search_query[field]);
                    } else {
                        val = decodeURIComponent(search_query[field]);
                    }
                    if (field === 'weight') {
                        val = search_query[field].toString();
                    }
                    this.form_data.additional_params[field] = val;
                }
            }
            let loading_location_title = decodeURIComponent(search_query.loading_location_title);
            let discharge_location_title = decodeURIComponent(search_query.discharge_location_title);
            let loading_location_complex_id = null;
            let discharge_location_complex_id = null;

            // (все терминалы), (все порты) и тд - обрабатываем отдельно
            if (loading_location_title.includes('(')) {
                loading_location_title = loading_location_title.split(' ')[0];
                loading_location_complex_id =  decodeURIComponent(search_query.loading_location);
            }
            if (discharge_location_title.includes('(')) {
                discharge_location_title = discharge_location_title.split(' ')[0];
                discharge_location_complex_id =  decodeURIComponent(search_query.discharge_location);
            }

            const loading_location = axios.get('/public-api/get-locations', {
                params: {
                    query: decodeURIComponent(loading_location_title),
                }
            });
            const discharge_location = axios.get('/public-api/get-locations', {
                params: {
                    query: decodeURIComponent(discharge_location_title),
                }
            });
            const valid_loading_locations = axios.get('/manager-search-filter-api/get-valid-location-ids?location_type=1');
            const valid_discharge_locations = axios.get('/manager-search-filter-api/get-valid-location-ids?location_type=2');
            Promise.all([loading_location, discharge_location, valid_loading_locations, valid_discharge_locations]).then(result => {
                result.forEach(({data}, index) => {
                    if (index === 0) {
                        let loading_valid_location_ids = result[2].data.content.valid_location_ids;
                        data = data.content.filter(el => {
                            if (!el.is_all_locations) {
                                return el.id.join('') === search_query.loading_location;
                            } else {
                                el.id = el.id.filter(port => {
                                    return loading_valid_location_ids.includes(port);
                                });
                                if (loading_location_complex_id === el.id.join(',')) {
                                    return true;
                                }
                            }
                        });

                        this.set_loading_location(data[0]);
                    }
                    if (index === 1) {
                        let discharge_valid_location_ids = result[2].data.content.valid_location_ids;
                        data = data.content.filter(el => {
                            if (!el.is_all_locations) {
                                return el.id.join('') === search_query.discharge_location;
                            } else {
                                el.id = el.id.filter(port => {
                                    return discharge_valid_location_ids.includes(port);
                                });
                                if (discharge_location_complex_id === el.id.join(',')) {
                                    return true;
                                }
                            }
                        });
                        console.log(data)
                        this.set_discharge_location(data[0]);
                    }
                });
                this.get_result_table();
            });
            for (let field in this.form_data.form) {
                if (this.form_data.form.hasOwnProperty(field)) {
                    if (field === 'cargo') {
                        this.form_data.form.cargo = decodeURIComponent(search_query.cargo);
                    }
                }
            }
        },
        searchByButton() {
            let url = this.getSearchParamsLink();
            history.pushState({'page_id': 'search_result'}, 'Поиск', url);
            this.get_result_table();
        },
        get_result_table() {
            this.set_search_state(true);
            this.clear_form_errors();
            axios.post('/manager-search-api/get-result-table', {...this.form_data})
                .then(({data}) => {
                    if (data.errors.all_fields) {
                        this.set_form_error(data.errors.all_fields);
                    }
                    this.set_search_result(data);
                    this.set_table_heads();
                    this.set_search_state(false);
                    this.set_currencies(this.search_result.content.currencies);

                    for (let order in this.search_result.content.title_fields) {
                        this.field_codes_to_field_orders[this.search_result.content.title_fields[order].code] = order;
                    }

                    /** Заполнение modifiers_field_values*/
                    for (let row in this.search_result.content.rows_result_table) {
                        for (let i in this.modifiers) {
                            let code = this.modifiers[i]['code'];
                            let field_order = this.field_codes_to_field_orders[code];
                            if (typeof this.modifiers_field_values[row] === 'undefined') {
                                this.modifiers_field_values[row] = {};
                            }
                            this.modifiers_field_values[row][code] = this.search_result.content.rows_result_table[row][field_order];
                        }
                    }

                    if (this.form_data.additional_params.is_home_consignment === 0) {//Временная заглушка для пункта "домашний коносамент"
                        this.search_result.content.rows_result_table.forEach(el => {
                            el.additional_params.hidden_row[4].field_value = 'Нет';
                        });
                    }

                    this.onModifierChange();
                    window.addEventListener('scroll', this.handleScroll);
                });
        },
        /**
         * Получает ссылку на поиск с параметрами, на которую будет сделан редирект, если прошла валидация
         * @returns {string}
         */
        getSearchParamsLink() {
            let search_query = '';
            search_query += this.getSearchQueryFromForm();
            search_query += this.getSearchQueryFromAdditionalParams();

            return this.getSearchLinkBySearchQuery(search_query);
        },
        /**
         * Возвращает кусок строки параметров поискового запроса из this.form
         * @returns {string}
         */
        getSearchQueryFromForm() {
            let search_query = '';
            for (let input in this.form_data.form) { // получаем данные из формы
                if (!this.form_data.form.hasOwnProperty(input)) {
                    continue;
                }
                let input_val = this.form_data.form[input];
                if (input === 'loading_location_object') {
                    if (input_val !== null) {
                        search_query += 'loading_location_type=' + input_val.type.type + '&loading_location_title=' + input_val.title + '&';
                    }
                } else if(input === 'discharge_location_object') {
                    if (input_val !== null) {
                        search_query += 'discharge_location_type=' + input_val.type.type + '&discharge_location_title=' + input_val.title + '&' ;
                    }
                } else {
                    search_query += input + '=' + encodeURIComponent(input_val) + '&';
                }
            }
            return search_query;
        },
        /**
         * Возвращает кусок строки параметров поискового запроса из  additional_params, где хранятся некоторые инпуты поисковой формы, не вошедшие в this.form.
         * Какого хрена так случилось? - Ответом ему было безмолвие...
         * @returns {string}
         */
        getSearchQueryFromAdditionalParams() {
            let search_query = '';
            for (let input in this.form_data.additional_params) {
                if (!this.form_data.additional_params.hasOwnProperty(input)) {
                    continue;
                }
                let input_val = this.form_data.additional_params[input];
                if (input_val === true) {
                    input_val = 1;
                } else if(input_val === false) {
                    input_val = 0;
                }
                search_query += input + '=' + encodeURIComponent(input_val) + '&';
            }
            return search_query;
        },

        /** Возвращает ссылку для поисковой выдачи по переданному поисковому запросу составленному из get-параметров */
        getSearchLinkBySearchQuery(search_query) {
            return '?client_company_id='
                + this.form_data.additional_params.client_company_id
                +'&client_company_title='
                + encodeURIComponent(this.form_data.additional_params.client_company_title)
                + '&' +  search_query.substring(0, search_query.length - 1);
        },
        showError(text) {
            this.formError = true;
            document.querySelectorAll('#search_form input, #search_form button').forEach(el=> {
                el.setAttribute('disabled', 'disabled');
            });

            document.getElementById('error-box').innerHTML = text;
        },
        /**
         * Кратко: Если не передан модификатор то выполняем для всех, если передан то для конкретного
         * Полно: Если передает modifier_changed, то меняется значение поля-модификатора в search_result.content.rows_result_table
         * и пересчитывает формулы где участвует это поле-модификатор
         * Если modifier_changed не передан, то пересчитывает все формулы и проверяются все поля-модификаторы,
         * например для загрузки поиска по ссылке
         */
        onModifierChange(modifier_changed, $event) {
            if ($event) {
                this.$store.commit('set_modifier_status', $event);
            }
            this.setValuesModifiersColumns(modifier_changed);
            this.search_result.content.rows_result_table = this.getCalculateRows(this.search_result.content.title_fields, this.search_result.content.rows_result_table, this.search_result.content.formulas);
        },
        /**
         * В зависимости от чекбокса из поисковой формы,
         * зануляет или заполняет modifier-колонку в строках значениями из this.modifiers_field_values
         */
        setValuesModifiersColumns(modifier_changed) {
            if (typeof modifier_changed !== 'undefined') {
                return this.setValuesModifierColumn(modifier_changed);
            }
            for (let i in this.modifiers) {
                this.setValuesModifierColumn(this.modifiers[i])
            }
        },
        /**
         * @see setValuesModifiersColumns
         * Изменение одной колонки-modifier
         * @param modifier
         */
        setValuesModifierColumn(modifier) {
            let field_order = this.field_codes_to_field_orders[modifier.code];
            for (let num_row = 0; num_row < this.search_result.content.rows_result_table.length; num_row++) {
                if (this.isReplaceWithRealValueNeeded(num_row, modifier)) {
                    this.search_result.content.rows_result_table[num_row][field_order] = this.modifiers_field_values[num_row][modifier.code];
                    continue;
                }
                this.search_result.content.rows_result_table[num_row][field_order] = 0;
            }
        },
        /**
         * Если строка disabled(добавлена в корзину), то нужно смотреть значение модиуифактора в строке,
         * иначе смотреть значение модификатора из поисковой формы
         * @return boolean
         */
        isReplaceWithRealValueNeeded(num_row, modifier) {
            if (this.isDisabledRow(num_row)) {
                return (typeof this.search_result.content.rows_result_table[num_row]['additional_params'][modifier.service_title] !== 'undefined')
                    ? this.search_result.content.rows_result_table[num_row]['additional_params'][modifier.service_title]
                    : false;
            }

            return this.form_data.additional_params[modifier.service_title];
        },
        /** Является ли строка disabled(добавленной в корзину) */
        isDisabledRow(num_row) {
            return this.search_result.content.rows_result_table[num_row]['disabled'] === true;
        },
        changeDateFormat(value, old_format, new_format) {
            let unixTimestamp = moment(value, [old_format]).unix();
            return moment.unix(unixTimestamp).format(new_format);
        }
    },
    created() {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    }
};