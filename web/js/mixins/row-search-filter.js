/**
 * Миксин для отображение строк попадающих под фильтр или исключенных строк
 */
export const rowsSearchFilterMixins = {
    mixins: [paginatorMixin, seacrchComponentMixins],
    data: function () {
        return {
            _csrf: null,
            headers_rows_search: [],
            rows_search: [],
            error_list: {},
            is_loading_data: null,
            page_links: {},
            current_page: 1,
            link_get_search_rows_by_hash_filter: '',
            link_get_exception_rows: '',
            link_get_search_rows: '', // принимает значение link_get_search_rows_by_hash_filter или link_get_exception_rows если строки исключенные
        };
    },
    mounted() {
        let search_params = '';
        let parse_url = this.parseUrl(window.location, true);
        if (parse_url.search) {
            let get_params = parse_url.searchObject;
            search_params = '?' + this.httpBuildQuery(get_params);
        }

        let dataset = this.$refs.config.dataset;
        this._csrf = dataset.csrf;
        this.hash_filter = dataset.hash_filter;
        this.is_exception_rows = dataset.is_exception_rows;
        if (this.is_exception_rows) {
            this.link_get_search_rows = this.link_get_exception_rows + search_params;
            this.loadData(this.link_get_search_rows);
        } else {
            this.link_get_search_rows = this.link_get_search_rows_by_hash_filter + search_params;
            this.loadData(this.link_get_search_rows, {params: {hash_filter: this.hash_filter}});
        }
    },
    methods: {
        async onSearch() {
            if (!this.query) {
                this.error_list = {};
                return this.loadData(this.link_get_search_rows, {params: {hash_filter: this.hash_filter}});
            }
            this.is_loading_data = true;
            if (await this.search()) {
                this.headers_rows_search = this.search_result.headers_rows_search;
                this.rows_search = this.search_result.rows_search;
                this.page_links = this.search_result.page_links;
                this.current_page = this.search_result.current_page;
                this.pages = this.getPages(this.page_links);
            }
            this.is_loading_data = false;
        },
        getSearchResultResponse() {
            return axios.get(this.link_get_search_rows, {params: {hash_filter: this.hash_filter, query: this.query}});
        },
        getTableHeads: function() {
            let heads = {};
            for(let key in this.headers_rows_search) {
                // Игнорируем поля не имеющие field_id (например Подать заявку)
                if (!this.headers_rows_search[key]['id']) {
                    continue;
                }
                heads[key] = this.headers_rows_search[key]['title'];
            }
            return heads;
        },
        hasPage(page) {
            return (typeof this.pages[page] !== 'undefined');
        },
        loadData(link, params) {
            if (link) {
                this.link_get_search_rows = link;
            }
            this.is_loading_data = true;
            axios.get(this.link_get_search_rows, params).then(response => {
                if (response.data.is_success) {
                    this.headers_rows_search = response.data.content.headers_rows_search;
                    this.rows_search = response.data.content.rows_search;
                    this.page_links = response.data.content.page_links;
                    this.current_page = response.data.content.current_page;
                    this.pages = this.getPages(this.page_links);
                } else {
                    this.error_list = response.data.errors;
                }
                this.is_loading_data = false;
            }).catch(e => {
                this.$set(this.error_list, 'all_fields', 'Ошибка получения данных, обратитесь к администратору!');
                this.is_loading_data = false;
            });
        },
        sortbyWithPaginator: function(order, key) {
            this.loadData( this.link_get_search_rows, {params: {sort_direction: order, field_order:key, hash_filter: this.hash_filter, query: this.query}} );
        },
        getError: function (field, id = false) {
            let result = '';
            if (typeof this.error_list === 'string') {
                return this.error_list;
            }
            if (id === false && (field in this.error_list)) {
                result = this.error_list[field];
            } else if (id in this.error_list && (field in this.error_list[id])) {
                result = this.error_list[id][field];
            }
            $('[data-toggle="tooltip"]').tooltip();

            return result;
        },
    }
};