/**
 * Миксин для работы с компонентом vue-filters-checkboxes-component.js
 *
 * @type {{methods: {getFiltersCheckboxes(): *, onChangeFiltersCheckboxes(*): void}}}
 */
export const filtersCheckboxesMixins = {
    methods: {
        getFiltersCheckboxes() {
            return this.filters_checkboxes;
        },
        onChangeFiltersCheckboxes(data) {
            this.filters_checkboxes = data;
            this.load();
        }
    }
};