/** Миксин для работы c поиском, используется в менеджерском и клиентском поиске */
searchMixins = {
    mixins: [urlMixins, arrayObjectMixins, collapseBlocksMixins, autoHeightForGridMixins, formulasMixins],
    data: function () {
        return {
            formCollapsed: false,
            formSticky: false,
            title_fields: {}, // заголовки полей поисковой выдачи
            rows_result_table: [], // строки поисковой выдачи
            selected_rows: [], // выбранные строки поисковой выдачи, которые не были положены в корзину
            form_error: false,
            /**
             * Боковые фильтры вида:
             * [
             *      field_id1 => [
             *          title => Порт отгрузки,
             *          values => [
             *              Веньчжоу => [
             *                  checked => true,
             *                  value => Веньчжоу
             *              ]
             *          ]
             *      ],
             *          ...
             * ]
             */
            filters: {},
            /** Табличный вид? (если false, то карточный) */
            table_layout: this.$cookies.get('table_layout') === 'true',
            bold_field_ids: [], // массив с номерами столбцов, которые нужно выделить жирным текстом
            /**
             * Массив в которм хранится состояние фильтров, для отображения строки.
             * Массив вида:
             * [
             *      hash_row1 => [
             *          filter_key1 => true,
             *              ...
             *          filter_keyN => false
             *      ],
             *          ...
             *      hash_rowN => [
             *          filter_key1 => false,
             *              ...
             *          filter_keyN => true
             *      ]
             * ]
             * Строка будет показана, если у строки все фильтры true.
             * Это означает что строка не противоречит ни одному боковому фильтру.
             * И скрыта если хоть один фильтр false.
             *
             * */
            results_count: null,
            row_hashes_with_status_filters: {},
            /**
             *  Формулы(FormulaList) записанные в обратной польской нотации.
             *  Например, ['10 4 5 + *', '10 4 5 + * 2 +']. где цифры это номер колонки (FieldConfig->order_in_manager_search | FieldConfig->order_in_client_search)
             **/
            formulas: null,
            currencies: null, // массив валют, участвующих в поисковой выдаче
            field_ids_to_field_orders: [], // массив соотношений field_id к order_key, используется например для соотношения фильтров к полям строк
            field_codes_to_field_orders: {}, // массив соотношений соотношение field_code к order_key
            not_exist_value: null, // символ для обозначения не существующего значения
            has_data_been_loaded_before: false, // форма была загружены ране? Если true - не будем искать атоматом при совпадении get параметров и параметров формы.
            inputs_loaded_counter: 0, // количество предзагруженных полей поисковой формы
            invalid_inputs: [], // некорректные поля
            invalid_inputs_for_highlight: [], // Нужно чтобы не подчеркивал поля, до нажатия на кнопку искать!
            is_form_loaded: false, // загружена ли форма?
            is_data_loaded: false, // загружены ли данные?
            is_response_error: false, // есть ли ошибки загрузки поисковой выдачи
            is_search: false, // вхождение в одноименный метод
            _csrf: null,
            /**
             * Модификаторы в поисковой форме, массив вида
             *  [
             *      'service_title' => 'is_security',
             *      'code' => FieldConfig::SECURITY,
             *      'title_for_view' => 'Охрана'
             *  ],[
             *      'service_title' => 'is_danger_cargo',
             *      'code' => FieldConfig::DANGER,
             *      'title_for_view' => 'Опасный груз'
             *  ],[
             *      'service_title' => 'is_special_tariff',
             *      'code' => FieldConfig::SPECIAL_TARIFF,
             *      'title_for_view' => 'Спец. тариф'
             *  ]
             */
            modifiers: null,
            modifiers_field_values: [], // здесь хранятся значение полей-модификаторов для всех строк поисковой выборки(rows_result_table)
            // форма поисковых фильтров
            form: {
                loading_location: '',
                lang: '',
                discharge_location: '',
                cargo: '',
                loading_date: ''
            },
            error_list: {}, // массив ошибок
        };
    },
    mounted: function () {
        this._csrf = this.$refs.config.dataset.csrf;
        this.not_exist_value = JSON.parse(this.$refs.config.dataset.not_exist_value);
        this.form.loading_date = this.$refs.config.dataset.loading_date;
        this.additional_params.weight = this.$refs.config.dataset.weight;
        this.additional_params.is_security = parseInt(this.$refs.config.dataset.is_security);
        this.modifiers = JSON.parse(this.$refs.config.dataset.modifiers);

        new Pikaday({
            field: document.getElementById('loading_date'),
            ...this.calendarSettings
        });
        /** Выставляем значение состояния фильтра из cookies*/
        let filter_state = this.getFilterStateFromCookies();
        if (filter_state !== null) {
            this.filter_state = filter_state;
        }
        /** Выставляем значение состояния поисковой формы из cookies*/
        let search_form_state = this.getSearchFormStateFromCookies();
        if (search_form_state !== null) {
            this.search_form_state = search_form_state;
        }
    },

    methods: {
        handleScroll () {
            if (window.pageYOffset >= 250) {
                this.formCollapsed = true;
                this.formSticky = true;
            }
            if (window.pageYOffset <= 150) {
                this.formCollapsed = false;
                this.formSticky = false;
            }
        },
        uncollapse() {
            window.removeEventListener('scroll', this.handleScroll);
            this.formCollapsed = false;
            document.querySelector('.filter-inner').style.top = '14.5rem';
        },
       /** Метод переключает вид между карточным и табличным*/
        changeLayout() {
            this.table_layout = !this.table_layout;
            this.$cookies.set('table_layout', this.table_layout);
            if (this.table_layout) {
                setTimeout(() => {
                    this.setHeightGrid(); // После получения данных, устанваливаем высоту grid-таблицы
                }, 100);
            }
        },
        /** Действия при изменение значения в поисковой форме */
        optionChange(data) {
            this.inputs_loaded_counter++;
            if (this.inputs_loaded_counter >= 2) { // 2 элемента, которые всегда пред-загружены не зависимо от get-параметров. Это Тип загрузки и Место ТО
                setTimeout(() => {
                    this.is_form_loaded = true;
                }, 1000); //  подождем еще 1 секунду, чтобы другие элементы успели пред-загрузиться (если такие есть) и покажем форму.
            }
            if (data.field_title === 'loading_location') {
                if (data.item) {
                    this.form.loading_location = data.item.id;
                    this.form.loading_location_object = data.item;
                    this.form.lang = data.item.lang;
                } else {
                    this.form.loading_location = '';
                    this.form.loading_location_object = null;
                    this.form.lang = '';
                }
            }
            if (data.field_title === 'discharge_location') {
                if (data.item) {
                    this.form.discharge_location = data.item.id;
                    this.form.discharge_location_object = data.item;
                    this.form.lang = data.item.lang;
                } else {
                    this.form.discharge_location = '';
                    this.form.discharge_location_object = null;
                    this.form.lang = '';
                }
            }
            if (data.field_title === 'cargo') {
                this.form.cargo = data.item.title;
            }
            if (data.field_title === 'custom_type') {
                this.additional_params.custom_type = data.item.id;
            }

            /** Это блок "Наименование клиентской компании", есть только в менеджерском поиске */
            if (data.field_title === 'client_company') {
                this.is_registered_client_company = false;
                if (data.item.is_company_registered) {
                    this.is_registered_client_company = true;
                }
                if (data.item.id && data.item.title) {
                    this.additional_params.client_company_id = data.item.id;
                    this.additional_params.client_company_title = data.item.title;
                    this.selected_client_company_data = data.item;
                } else {
                    this.additional_params.client_company_id = '';
                    this.additional_params.client_company_title = '';
                    this.is_registered_client_company = false;
                }
            }

            if (!this.validate()) {
                return;
            }

            if (this.isGetParamsMatchedWithForm() && !this.has_data_been_loaded_before) {
                this.search(); // ищем когда форма изменилась при этом должны быть get-параметры и должна быть пройдена валидация
            }
        },
        /** Валидирует поисковую форму, заполняет массив this.invalid_inputs_for_highlight */
        validate() {
            this.invalid_inputs = [];
            this.invalid_inputs_for_highlight = [];
            for (let input in this.form) {
                if (!this.form.hasOwnProperty(input)) {
                    continue;
                }
                if (this.form[input] === '') {
                    this.invalid_inputs_for_highlight.push(input);
                }
            }
            if (!this.form.loading_location) {
                this.invalid_inputs_for_highlight.push('loading_location');
            }
            if (!this.form.discharge_location) {
                this.invalid_inputs_for_highlight.push('discharge_location');
            }
            if (isNaN(this.additional_params.weight) || this.additional_params.weight.trim() === '') {
                this.invalid_inputs_for_highlight.push('weight');
            }

            return this.invalid_inputs_for_highlight.length === 0;
        },
        /**
         * Get-параметры в url соотвествуют тому, что введено в поисковой форме?
         * @returns {boolean}
         */
        isGetParamsMatchedWithForm() {
            let search_url = this.parseUrl(location.href);
            let search_query = search_url.searchObject;

            for (let input in this.form) {
                if (!this.form.hasOwnProperty(input) || input === 'loading_location_object' || input === 'discharge_location_object') { // этих объектов нет в get-параметрах
                    continue;
                }
                if (input === 'loading_date') { // формат отличается от того, что в урле
                    let unixTimestamp = moment(search_query[input], ['DD.MM.YYYY', 'YYYY-MM-DD']).unix();
                    search_query[input] = moment.unix(unixTimestamp).format('DD.MM.YYYY'); // todo давай эти 2 строки вынесем в метод: changeDateFormat(value, old_format, new_format)
                }
                if (this.form[input] != decodeURIComponent(search_query[input])) {
                    return false;
                }
            }

            for (let input in this.additional_params) {
                if (!this.additional_params.hasOwnProperty(input)) {
                    continue;
                }
                if (this.additional_params[input] != decodeURIComponent(search_query[input]) ) {
                    return false;
                }
            }

            return true;
        },
        /** запрос и получение поисковой выдачи */
        async search() {
            this.error_list = {};
            if (!this.validate()) {
                this.highLightInvalidInputs();
                return;
            }
            this.is_data_loaded = false;
            this.is_search = true;

            try {
                let unixTimestamp = moment(this.form.loading_date, ['DD.MM.YYYY', 'YYYY-MM-DD']).unix();
                this.form.loading_date = moment.unix(unixTimestamp).format('YYYY-MM-DD'); // todo давай эти 2 строки вынесем в метод: changeDateFormat(value, old_format, new_format)

                const response = await axios.post(this.get_result_table_link, {_csrf:this._csrf, form: this.form, additional_params:this.additional_params});
                if (response.data.is_success) {
                    this.is_response_error = false;
                    this.title_fields = response.data.content.title_fields;
                    this.rows_result_table = Object.values(response.data.content.rows_result_table);
                    this.currencies = response.data.content.currencies;
                    this.bold_field_ids = response.data.content.bold_field_ids;
                    this.results_count = response.data.content.count_rows_result_table;
                    this.cart_items_count = this.rows_result_table.filter(item => item.disabled).length;

                    this.setNotCommonProperties(response);

                    this.setFieldCodesToFieldOrders();
                    this.setModifiersFieldValuesAndRowsShown();
                    this.filters = response.data.content.filters;

                    if (this.isEmptyObject(this.filters)) {
                        this.setFilterStateAsHidden();
                    }
                    this.field_ids_to_field_orders = response.data.content.field_ids_to_field_orders;
                    this.formulas = response.data.content.formulas;
                    this.onModifierChange();

                    //После получения данных нужно еще раз стриггерить фильтр, чтобы скрыть ненужные строки
                    for (let filterProp in this.filters) {
                        this.filterChange(filterProp);
                    }


                    if (this.additional_params.is_home_consignment === 0) {//Временная заглушка для пункта "домашний коносамент"
                        this.rows_result_table.forEach(el => {
                            el.additional_params.hidden_row[4].field_value = 'Нет';
                        })
                    }

                    if(this.error_list['all_fields']) {
                        this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                        this.is_response_error = true;
                        this.is_data_loaded = true;
                        return;
                    }
                } else {
                    this.is_response_error = true;
                    this.error_list = response.data.errors;
                }
                this.is_data_loaded = true;
            } catch(e) {
                this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                this.is_response_error = true;
            }

            this.has_data_been_loaded_before = true;
        },
        /** Подсвечивание некорректных полей поисковой выдачи, путем перекладывания из временной переменной invalid_inputs_for_highlight */
        highLightInvalidInputs() {
            this.invalid_inputs = this.invalid_inputs_for_highlight.slice();
        },

        setFieldCodesToFieldOrders() {
            for (let order in this.title_fields) {
                this.$set(this.field_codes_to_field_orders, this.title_fields[order].code, order);
            }
        },
        /** Заполнение this.modifiers_field_values и установка строкам поисковой выдачи флага shown*/
        setModifiersFieldValuesAndRowsShown() {
            for (let row in this.rows_result_table) {
                this.$set( this.rows_result_table[row], 'shown', true);
                for (let i in this.modifiers) {
                    let code = this.modifiers[i]['code'];
                    let field_order = this.field_codes_to_field_orders[code];
                    if (typeof this.modifiers_field_values[row] === 'undefined') {
                        this.modifiers_field_values[row] = {};
                    }
                    this.$set(this.modifiers_field_values[row], code, this.rows_result_table[row][field_order]);
                }
            }
        },
        showError(text) {
            this.form_error = true;
            document.querySelectorAll('#search_form input, #search_form button').forEach(el=> {
                el.setAttribute('disabled', 'disabled');
            });

            document.getElementById('error-box').innerHTML = text;
        },
        /**
         * Кратко: Если не передан модификатор то выполняем для всех, если передан то для конкретного
         * Полно: Если передает modifier_changed, то меняется значение поля-модификатора в rows_result_table
         * и пересчитывает формулы где участвует это поле-модификатор
         * Если modifier_changed не передан, то пересчитывает все формулы и проверяются все поля-модификаторы,
         * например для загрузки поиска по ссылке
         */
        onModifierChange(modifier_changed) {
            this.setValuesModifiersColumns(modifier_changed);
            this.rows_result_table = this.getCalculateRows(this.title_fields, this.rows_result_table, this.formulas);
        },
        /**
         * В зависимости от чекбокса из поисковой формы,
         * зануляет или заполняет modifier-колонку в строках значениями из this.modifiers_field_values
         */
        setValuesModifiersColumns(modifier_changed) {
            if (typeof modifier_changed !== 'undefined') {
                return this.setValuesModifierColumn(modifier_changed);
            }
            for (let i in this.modifiers) {
                this.setValuesModifierColumn(this.modifiers[i])
            }
        },
        /**
         * @see setValuesModifiersColumns
         * Изменение одной колонки-modifier
         * @param modifier
         */
        setValuesModifierColumn(modifier) {
            let field_order = this.field_codes_to_field_orders[modifier.code];
            for (let num_row = 0; num_row < this.rows_result_table.length; num_row++) {
                if (this.isReplaceWithRealValueNeeded(num_row, modifier)) {
                    this.rows_result_table[num_row][field_order] = this.modifiers_field_values[num_row][modifier.code];
                    continue;
                }
                this.rows_result_table[num_row][field_order] = 0;
            }
        },
        /**
         * Если строка disabled(добавлена в корзину), то нужно смотреть значение модиуифактора в строке,
         * иначе смотреть значение модификатора из поисковой формы
         * @return boolean
         */
        isReplaceWithRealValueNeeded(num_row, modifier) {
            if (this.isDisabledRow(num_row)) {
                return (typeof this.rows_result_table[num_row]['additional_params'][modifier.service_title] !== 'undefined')
                    ? this.rows_result_table[num_row]['additional_params'][modifier.service_title]
                    : false;
            }

            return this.additional_params[modifier.service_title];
        },
        /** Является ли строка disabled(добавленной в корзину) */
        isDisabledRow(num_row) {
            return this.rows_result_table[num_row]['disabled'] === true;
        },
        /** Поиск по нажатию кнопки "Искать" */
        async searchByButton() {
            if (!this.validate()) {
                this.highLightInvalidInputs();
                return;
            }
            if (await this.isCompanyRegisterNeeded()) {
                return;
            }

            window.location.href = this.getSearchParamsLink(); // делаем редирект на поиск с get-параметрами
        },
        /**
         * Получает ссылку на поиск с параметрами, на которую будет сделан редирект, если прошла валидация
         * @returns {string}
         */
        getSearchParamsLink() {
            let search_query = '';
            search_query += this.getSearchQueryFromForm();
            search_query += this.getSearchQueryFromAdditionalParams();

            return this.getSearchLinkBySearchQuery(search_query);
        },
        /**
         * Возвращает кусок строки параметров поискового запроса из this.form
         * @returns {string}
         */
        getSearchQueryFromForm() {
            let search_query = '';
            for (let input in this.form) { // получаем данные из формы
                if (!this.form.hasOwnProperty(input)) {
                    continue;
                }
                let input_val = this.form[input];
                if (input === 'loading_location_object') {
                    if (input_val !== null) {
                        search_query += 'loading_location_type=' + input_val.type.type + '&loading_location_title=' + input_val.title + '&';
                    }
                } else if(input === 'discharge_location_object') {
                    if (input_val !== null) {
                        search_query += 'discharge_location_type=' + input_val.type.type + '&discharge_location_title=' + input_val.title + '&';
                    }
                } else if(input === 'loading_date') {
                    let unixTimestamp = moment(this.form.loading_date, ['DD.MM.YYYY', 'YYYY-MM-DD']).unix();
                    let loading_date = moment.unix(unixTimestamp).format('YYYY-MM-DD'); // todo давай эти 2 строки вынесем в метод: changeDateFormat(value, old_format, new_format)
                    search_query += 'loading_date=' + loading_date + '&';
                } else {
                    search_query += input + '=' + encodeURIComponent(input_val) + '&';
                }
            }
            return search_query;
        },
        /**
         * Возвращает кусок строки параметров поискового запроса из  additional_params, где хранятся некоторые инпуты поисковой формы, не вошедшие в this.form.
         * Какого хрена так случилось? - Ответом ему было безмолвие...
         * @returns {string}
         */
        getSearchQueryFromAdditionalParams() {
            let search_query = '';
            for (let input in this.additional_params) {
                if (!this.additional_params.hasOwnProperty(input)) {
                    continue;
                }
                let input_val = this.additional_params[input];
                if (input_val === true) {
                    input_val = 1;
                } else if(input_val === false) {
                    input_val = 0;
                }
                search_query += input + '=' + encodeURIComponent(input_val) + '&';
            }
            return search_query;
        },
        /** Получение css-классаов для полей поисковой формы */
        getClass(field_title) {
            if (this.invalid_inputs.indexOf(field_title) !== -1) {
                return 'form-control form-control-sm is-invalid';
            } else {
                return 'form-control form-control-sm';
            }
        },
        /** Приведение заголовков поисковой выдачи, к виду для использования в grid (Номер столбца => имя столбца) */
        getTableHeads() {
            let heads = {};
            for(let key in this.title_fields) {
                heads[key] = this.title_fields[key]['title'];
            }
            return heads;
        },
        /** Описание поведения при изменение значения бокового фильтра */
        filterChange(key) {
            if (this.filters[key].values) {
                for (let value in this.filters[key].values) {
                    if (this.filters[key].values[value].checked === false) {
                        this.setRowHashesStatusFilter(key, value, false);
                    } else {
                        this.setRowHashesStatusFilter(key, value, true);
                    }
                }
            }

            let hash_row = '';
            let is_show_row = true;
            for (let row in this.rows_result_table) {
                hash_row = this.rows_result_table[row].additional_params.hash_row;
                is_show_row = true;
                for (let filter_key in  this.row_hashes_with_status_filters[hash_row]) {
                    if (this.row_hashes_with_status_filters[hash_row].hasOwnProperty([filter_key]) && this.row_hashes_with_status_filters[hash_row][filter_key] === false) {
                        is_show_row = false;
                        break;
                    }
                }
                this.rows_result_table[row]['shown'] = is_show_row;
            }

            this.saveFilterState();
        },
        /** Устанавливаем значение для переданного фильтра в массив в которм хранится состояние фильтров, для отображения строки. */
        setRowHashesStatusFilter(filter_key, filter_value, is_checked) {
            let hash_row = '';
            for (let row in this.rows_result_table) {
                hash_row = this.rows_result_table[row].additional_params.hash_row;
                /** Инициализация */
                if (!this.row_hashes_with_status_filters[hash_row]) {
                    this.row_hashes_with_status_filters[hash_row] = {[filter_key]: true};
                }
                /** Проверяем из отображаемых полей строки*/
                if (this.field_ids_to_field_orders[filter_key] && this.rows_result_table[row][this.field_ids_to_field_orders[filter_key]] == filter_value) {
                    this.row_hashes_with_status_filters[hash_row][filter_key] = is_checked;
                    continue;
                }
                /** Проверяем из полей скрытой-строки, которая появляется при нажатие выпадашки на строке*/
                if (this.isMatchFilterValueWithHiddenRow(row, filter_key, filter_value)) {
                    this.row_hashes_with_status_filters[hash_row][filter_key] = is_checked;
                }
            }
        },
        setAndSaveFilterState(filter_field_id) {
            this.filters[filter_field_id]['show'] = !this.filters[filter_field_id]['show'];
            this.saveFilterState();
        },
        getFilterState(filter_field_id) {
            return this.filters[filter_field_id]['show'] ? 'show' : '';
        },
        /** Сохраняет изменения в настройках: какие чекбоксы меню показывать */
        saveFilterState() {
            axios.post(this.save_filters_state_link, {_csrf:this._csrf, filters_state: this.filters}).then(response => {
                if (!response.data.is_success) {
                    this.error_list['all_fields'] = response.data.errors.all_fields;
                    this.is_response_error = true;
                }
            }).catch(e => {
                this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Ошибка! Данные фильтров не сохранены</div>";
                this.is_response_error = true;
            });
        },
        /** Ответ от сервера получен и в нем строки результирующей таблицы, а не ошибка */
        isDataLoadingSuccess() {
            return !this.is_response_error && this.is_data_loaded;
        },
        /** Если форма не свернута или если результирующей выборка не получена*/
        isShowSearchForm() {
            return this.search_form_state.is_search_form_collapsed == 0 || !this.isDataLoadingSuccess();
        },
        /** Если фильтр не свернут или если результирующей выборка не получена*/
        isShowFilter() {
            return this.filter_state.is_filter_block_collapsed == 0 && this.isDataLoadingSuccess();
        },
    },
    computed: {
        cart_items_count() {
            const cart = this.rows_result_table.filter(el => el.disabled);
            return cart.length;
        },
        /** Разрешаем отправлять форму только если выбраны пункты отправления и назначения */
        canSearch() {
            return this.form.loading_location && this.form.discharge_location;
        },
    },
    created() {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed() {
        window.removeEventListener('scroll', this.handleScroll);
    }
};