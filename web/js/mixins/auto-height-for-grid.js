/**
 * Миксин высчитывающий и устанавливающий высоту grid-таблице, относительно клиенсткого дисплея
 */
export const autoHeightForGridMixins = {
    data: function () {
        return {
            /**
             * По сути это набор html элементов, описывающую вложенность до элемента,
             * чью высоту мы хотим вычислить.
             *
             * объект вида 'название селектора' : 'вид селектора'
             * Например,
             * 'main': 'tagName',
             * 'search_result': 'id',
             * 'result_table_container': 'id',
             * 'grid_table': 'id'
             */
            html_elements: {},
        };
    },
    methods: {
        /** Устанвавливает высоту grid таблицы, относительно размеров клиентского дисплея */
        setHeightGrid: function() {
            let dirty_height_grid = this.getDirtyHeightGrid(); // Получаем высоту grid без учета margins gridа и paddings родительского контейнера
            let grid_table = document.getElementById('grid_table');
            let result_table_container = document.getElementById('result_table_container');
            let clean_height_grid = this.getCleanHeightGrid(grid_table, result_table_container, dirty_height_grid) - 5; //Получаем чистую высоту grid таблицы
            if (clean_height_grid < 300) {// Минимально допустимая высота (5 строк)
                clean_height_grid = 300;
            }
            grid_table.style.height = clean_height_grid  + 'px'; //устанавливаем grid вычисленную выше высоту
        },
        /** Получаем высоту grid без учета margins gridа и paddings родительского контейнера */
        getDirtyHeightGrid() {
            let dirty_height_grid = document.body.clientHeight; //высота дисплея клиентского браузера
            for (let title in this.html_elements) {
                let current_element;
                let type_element = this.html_elements[title];
                if (type_element === 'tagName') {
                    current_element = document.getElementsByTagName(title)[0];
                } else {
                    current_element = document.getElementById(title);
                }
                /** Получаем массив из HTMLCollection детей, родителя текущего элемента */
                let array_children_current_element_parents = [...current_element.parentNode.children];
                let sum_height_without_current_element = array_children_current_element_parents.filter(
                    (val) => val[type_element] !== current_element[type_element]
                ).reduce(
                    (result,val) => result + val.clientHeight + this.getMarginHeightByElement(val), 0
                );

                dirty_height_grid = dirty_height_grid - sum_height_without_current_element;
            }

            return dirty_height_grid;
        },
        getMarginHeightByElement(element) {
            let computedStyle = getComputedStyle(element);
            let margin_top_height = Number(computedStyle.marginTop.replace (new RegExp ('px', 'g'), ''));
            let margin_bottom_height = Number(computedStyle.marginBottom.replace (new RegExp ('px', 'g'), ''));

            return margin_top_height + margin_bottom_height;
        },
        getPaddingHeightByElement(element) {
            let computedStyle = getComputedStyle(element);
            let padding_top_height = Number(computedStyle.paddingTop.replace (new RegExp ('px', 'g'), ''));
            let padding_bottom_height = Number(computedStyle.paddingBottom.replace (new RegExp ('px', 'g'), ''));

            return padding_top_height + padding_bottom_height;
        },
        getCleanHeightGrid(element, parent_element, dirty_height_grid) {
            let ratio_fitting = 1; // Браузер может показать вертикальный скролл, поэтому берем небольшой запас, чтобы не были блоки вычислены в притык в притык
            let clean_height_grid = dirty_height_grid - this.getMarginHeightByElement(element) - this.getPaddingHeightByElement(parent_element) - ratio_fitting;
            let filter_height = document.querySelector('#filters .bg-white').clientHeight;

            // Если результирующая высота таблицы менее высоты фильтра, то приравниваем её к высоте фильтра
            return clean_height_grid > filter_height ? clean_height_grid : filter_height - 87;
        }
    }
};