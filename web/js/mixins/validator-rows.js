/**
 * Миксин валидатора редактируемых ячеек в grid-editable.
 * В js файл, куда подключается данный миксин, нужно засетить свойство this.rows_validation_rules.
 * Свойство this.rows_validation_rules нужно сетить методе, где загружаются строки (обычно load() или onLoad()).
 * Дынные для this.rows_validation_rules должны передаваться с бэкэнда.
 *
 * this.validate_rows() - запускает валидацию
 * this.areRowsHaveErrors() - проверяет, были ли выявлены ошибки в ходе валидации
 *
 * @see коммит CH-2268 для vue-cart.js и vue-manager-offer.js
 */
validatorRowsMixins = {
    mixins: [validatorMixins],
    data: function () {
        return {
            error_editable_cell: [],
            rows_validation_rules: [],
        };
    },

    methods: {
        /** Здесь и далее line - номер строки, row - номер столбца, value - значение. Рефаторинг CH-2398 */
        validate_rows: function(line, row, value) {
            let validation_rules = this.rows_validation_rules[row];
            if (!validation_rules) {
                return true;
            }
            if (!validation_rules.required && !value) {
                this.clearError(line, row);
                return true;
            }

            let validation_methods = validation_rules.methods;
            for (let validation_method in validation_methods) {
                let rule = (validation_methods[validation_method].rule !== undefined) ? validation_methods[validation_method].rule : null;
                if (!this[validation_method](null, rule, value)) {
                    let message = validation_methods[validation_method].message;
                    this.setError(line, row, message);
                    return false;
                } else {
                    this.clearError(line, row);
                }
            }

            return true;
        },

        onUpdateCell: function (line, row, value) {
            this.validate_rows(line, row, value);
            this.$nextTick(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        },

        onUpdateCol: function (rows) {
            for (let i = 0; i < rows.length; i++) {
                this.validate_rows(rows[i].index, rows[i].key, rows[i].value);
            }
            this.$nextTick(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        },

        clearError: function (line, row) {
            let error = [];
            error[row] = '';
            this.$set(this.error_editable_cell, line, error);
        },

        setError: function (line, row, message) {
            let error = [];
            error[row] = message;
            this.$set(this.error_editable_cell, line, error);
        },

        areRowsHaveErrors: function () {
            for (let i = 0; i < this.error_editable_cell.length; i++) {
                if (this.error_editable_cell[i]) {
                    for (let j = 0; j < this.error_editable_cell[i].length; j++) {
                        if (this.error_editable_cell[i][j]) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
};