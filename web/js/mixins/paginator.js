import {urlMixins} from './url';

export const paginatorMixin = {
    mixins: [urlMixins],
    data() {
        return {
            raw_page_link: '/erp-api/search?page=',
        };
    },
    methods: {
        /**
         * Возвращает массив страниц для отрисовки пагинатора.
         * Например:
         * [1,2,3,4]
         * или
         * [1,...,3,4,5,...,189]
         * или
         * [1,...,186,187,188,189]
         * @param page_links - объект ссылок на страницы, который приходит от стандартного пагинатора Yii
         * @returns {Array}
         */
        getPages(page_links) {
            let pages = [];
            let page_numbers = [];
            let self_page;
            let page_url = '';
            for (let page_type in page_links) {
                page_url = this.parseUrl(page_links[page_type]);
                if (page_type === 'self') {
                    self_page = parseInt(page_url.searchObject.page);
                }
                page_numbers.push(parseInt(page_url.searchObject.page));
            }

            let slashed_page = page_url.search.replace(/(page=)(\d+)/, '$1'); // страница без номера. Получится так: page=
            this.raw_page_link = page_url.pathname + slashed_page; // это get_link, но со страницей без номера
            let last_page = Math.max.apply(null, page_numbers);

            if (last_page > 8) {
                if (self_page <= 4) { // нужно "..." только справа
                    pages = this.getDotsRight(last_page);
                } else {
                    if (last_page - 4 >= self_page) { // нужно "..." в обоих сторон
                        pages = this.getDotsBoth(self_page, last_page);
                    } else { // нужно "..." только слева
                        pages = this.getDotsLeft(last_page);
                    }
                }
            } else { // не нужно "..." вообще
                pages = this.getNoDots(last_page);
            }
            return pages;
        },
        loadDataOnPageClick(page) {
            if (page === this.current_page) {
                return;
            }
            window.history.pushState(null, null, this.reBuildGetLink(window.location.href,{page: page}));

            this.$emit('paginationClicked', this.getPageLink(page, this.raw_page_link));
        },
        getDotsRight(last_page) {
            let pages = [];
            pages.push(1);
            pages.push(2);
            pages.push(3);
            pages.push(4);
            pages.push(5);
            pages.push(6);
            pages.push('...');
            pages.push(last_page);
            return pages;
        },
        getDotsBoth(self_page, last_page) {
            let pages = [];
            pages.push(1);
            pages.push('...');
            pages.push(self_page - 2);
            pages.push(self_page - 1);
            pages.push(self_page);
            pages.push(self_page + 1);
            pages.push(self_page + 2);
            pages.push('...');
            pages.push(last_page);
            return pages;
        },
        getDotsLeft(last_page) {
            let pages = [];
            pages.push(1);
            pages.push('...');
            pages.push(last_page - 5);
            pages.push(last_page - 4);
            pages.push(last_page - 3);
            pages.push(last_page - 2);
            pages.push(last_page - 1);
            pages.push(last_page);
            return pages;
        },
        getNoDots(last_page) {
            let pages = [];
            for (let i = 1; i <= last_page; i++) {
                pages.push(i);
            }
            return pages;
        },
        getPageLink(page, raw_link) {
            page = parseInt(page);
            if (!page || !raw_link) {
                return '';
            }
            let page_url = this.parseUrl(raw_link);
            let unslashed_page = page_url.search.replace('page=', 'page='+page); // добавляем номер страницы в get_link
            let result = page_url.pathname + unslashed_page;

            return result;
        },
        getPageLinkClass(page, current_page) {
            let pl_class = 'page-item';
            pl_class = (page !== '...') ? pl_class : pl_class + ' disabled ';
            pl_class = (page == current_page) ? pl_class + ' active ' : pl_class;

            return pl_class;
        }
    },
    mounted: function () {
        //ловим событие изменения истории браузера при нажатии кнопок вперед-назад
        //для обновления страницы
        window.addEventListener('popstate', function(e) {
            window.location.reload();
        });
    }
};