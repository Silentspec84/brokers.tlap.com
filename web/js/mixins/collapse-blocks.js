/**
 * Миксин скрывающий/раскрывающий блоки.
 * Например, боковой фильтр в поиске и поисковая форма
 */
export const collapseBlocksMixins = {
    data: function () {
        return {
            filter_state: { // состояние бокового фильтра
                result_table_class: 'col-lg-10', // класс определяет количество колонок результирующей таблицы
                is_filter_block_collapsed: 0, // блок фильтров скрыт?
                fa_filters_arrow_class: 'fe-arrow-left' // иконка-стрелка, для  схлопывания/расхлопывания фильтров влево/вправо?
            },
            search_form_state: {
                is_search_form_collapsed: 0, // блок поисковой формы скрыт?
                fa_search_form_arrow_class: 'fe-arrow-up' // иконка-стрелка, для схлопывания/расхлопывания поисковой формы вверх/вниз?
            }
        };
    },
    methods: {
        /**
         * Сворачивает/разворачивает блок с боковыми фильтрами по нажатию на стрелку
         */
        collapseFilterBlock() {
            if (this.filter_state.result_table_class === "col-lg-10") {
                this.filter_state.result_table_class = "col-lg-12";
                this.filter_state.is_filter_block_collapsed = 1;
                this.filter_state.fa_filters_arrow_class = "fe-arrow-right";
            } else {
                this.filter_state.result_table_class = "col-lg-10";
                this.filter_state.is_filter_block_collapsed = 0;
                this.filter_state.fa_filters_arrow_class = "fe-arrow-left";
            }

            this.setFilterStateInCookies();
        },
        getFilterStateFromCookies() {
            return this.$cookies.get('filter_state');
        },
        setFilterStateInCookies() {
            this.$cookies.set('filter_state', this.filter_state);
        },
        setFilterStateAsHidden() {
            this.filter_state.result_table_class = "col-lg-12";
            this.filter_state.is_filter_block_collapsed = 1;
        },
        /**
         * Сворачивает/разворачивает блок с поисковой формой по нажатию на стрелку
         */
        collapseSearchFormBlock() {
            this.search_form_state.is_search_form_collapsed = Number(!this.search_form_state.is_search_form_collapsed);
            if (this.search_form_state.is_search_form_collapsed === 1) {
                this.search_form_state.fa_search_form_arrow_class = 'fe-arrow-down';
            } else {
                this.search_form_state.fa_search_form_arrow_class = 'fe-arrow-up';
            }

            this.setSearchFormStateInCookies();
        },
        /** this.html_elements подключаются в миксине autoHeightForGridMixins */
        isAutoHeightCalculate() {
            return this.html_elements;
        },
        getSearchFormStateFromCookies() {
            return this.$cookies.get('search_form');
        },
        setSearchFormStateInCookies() {
            this.$cookies.set('search_form', this.search_form_state);
        },
    }
};