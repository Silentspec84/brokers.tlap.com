/**
 * Mixin полезных функций для работы со строками в js.
 * Постоянно дополняется...
 */
export const stringMixins = {
    methods: {
        decodeHTML: function (html) {
            let txt = document.createElement('textarea');
            txt.innerHTML = html;
            return txt.value;
        },
        /** Проверка что строка является числом */
        isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },
        /** Возвращает float, если передана строка */
        getFloatByString(value) {
            if (typeof value === 'string') {
                return parseFloat(value);
            }

            return value;
        },
    }
};