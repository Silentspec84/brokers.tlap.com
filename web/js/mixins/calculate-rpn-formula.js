/**
 * Mixin вычисляет выражения записанные в обратной польской нотации
 * Например 10 4 5 + +
 */

export const calculateRpnFormulaMixins = {
    methods: {
        calculateRpnFormula: function (formula) {
            let formula_operands_and_operators = formula.split(" ");
            let result = new Array();
            for (let i in formula_operands_and_operators) {
                if (formula_operands_and_operators[i] !== "+"
                    && formula_operands_and_operators[i] !== "*"
                    && formula_operands_and_operators[i] !== "-"
                    && formula_operands_and_operators[i] !== "/"
                    && formula_operands_and_operators[i] !== "^")
                {
                    result.push(parseInt(formula_operands_and_operators[i]));
                } else {
                    let operator = formula_operands_and_operators[i];
                    let right_operand = result.pop();
                    let left_operand = result.pop();
                    switch (operator) {
                        case "+":
                            result.push(parseFloat(left_operand) + parseFloat(right_operand));
                            break;
                        case "*":
                            result.push(parseFloat(left_operand) * parseFloat(right_operand));
                            break;
                        case "-":
                            result.push(parseFloat(left_operand) - parseFloat(right_operand));
                            break;
                        case "/":
                            result.push(parseFloat(left_operand) / parseFloat(right_operand));
                            break;
                        case "^":
                            result.push(Math.pow(parseFloat(left_operand), parseFloat(right_operand)));
                            break;
                    }
                }
            }

            return result.join('');
        }
    }
};