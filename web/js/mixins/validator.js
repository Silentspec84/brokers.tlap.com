/**
 Применение:
 1. Составляем конфиг из правил
 2. Запускаем у себя метод this.validateForm(config, this.form), куда передаем конфиг и данные формы.
 3. Получаем в ответ результат - true в случае успеха и false в случае неудачи.
 4. Ошибки сетятся в error_list, используем listMixins и метод getError() для вывода ошибок в форме. Помним, что getError поддерживает массивы (getError: function (field, id = false))

 Пример метода, валидирующего данные из формы

 validate: function(status) {

// Очищаем список ошибок
this.error_list = {};

// Формируем конфиг
 let config = {
 required: {
 fields: ['client_full_name', 'client_email', 'client_phone', 'date_shipping_from', 'date_way_till', 'date_cargo_ready', 'order_number', 'delivery_basis', 'delivery_address', 'cargo_code', 'cargo_description', 'notify', 'consignee'],
 message: 'Данное поле обязательно к заполнению!',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
 customValidation: {fields: ['shipper'], method: 'validateShipper', params: {status: status}},
 isString: {fields: ['client_full_name', 'client_email', 'client_phone'], message: 'Данное поле не является строкой!'},
 isBool: {fields: ['client_full_name', 'client_email', 'client_phone'], message: 'Данное поле должно быть булевым значением'},
 isInteger: {fields: ['client_full_name', 'client_email', 'client_phone'], message: 'Данное поле должно быть целым числом'},
 isFloat: {fields: ['client_full_name', 'client_email', 'client_phone'], message: 'Данное поле должно быть дробным числом'},match: {fields: [], pattern: ''},
 min: {
 fields: ['client_full_name', 'order_number', 'delivery_basis', 'bid_comment', 'delivery_address', 'cargo_code', 'cargo_description', 'notify', 'consignee'],
 min: 3,
 message: 'Минимальная длина поля - 3 символа',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
 max: {
 fields: ['order_number', 'delivery_basis', 'bid_comment', 'delivery_address', 'cargo_code', 'cargo_description', 'notify', 'consignee'],
 max: 255,
 message: 'Максимальная длина поля - 255 символов',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
 isDate: {
 fields: ['date_way_from', 'date_way_till', 'date_cargo_ready'],
 format: 'YYYY-MM-DD',
 message: 'Неверный формат даты',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },dateAfter: {
 fields: ['date_way_from', 'date_way_till', 'date_cargo_ready'],
 format: 'YYYY-MM-DD',
 date: moment().subtract(1, 'd'),
 message: 'Дата должна быть не раньше текущей',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },dateBefore: {fields: ['date_way_from'], format: 'YYYY-MM-DD', date: moment().format("YYYY-MM-DD"), message: 'Дата должна быть раньше текущей'},
 dateEqual: {fields: ['date_way_from'], format: 'YYYY-MM-DD', date: moment().format("YYYY-MM-DD"), message: ''Дата должна быть равна текущей'},match: {fields: ['client_email'],pattern: '/^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i'},
 email: {
 fields: ['client_email'],
 message: 'Неправильный формат почты (xxx@xxx.xx)',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
 phone: {
 fields: ['client_phone'],
 message: 'Неправильный формат телефона (+#(###)###-##-##)',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
 userName: {
 fields: ['client_full_name'],
 message: 'Поле Контактное лицо должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 3-х символов и больше 50.',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
  companyName: {
 fields: ['company_name'],
 message: 'Поле Организация должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 3-х символов и больше 50.',
 needValidate: 'isNeedValidate',
 params: {status: status}
 },
};

// метод возвращает результат валидации (true|false)
 return this.validateForm(config, this.form);
},
 */

validatorMixins = {
    methods: {

        /**
         * Правило проверяет, заполнено ли поле
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        required: function(field, rule, value) {
            return !!value || value === false;
        },

        /**
         * Кастомная валидация по методу, который определяется в файле, из которого вызвана валидация.
         * @param field
         * @param rule
         * @param value
         * @returns {*}
         */
        customValidation: function(field, rule, value) {
            return rule['condition'];
        },

        /**
         * Правило проверяет, является ли значение из поля строкой
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isString: function(field, rule, value) {
            return typeof (value) === 'string';
        },

        /**
         * Правило проверяет, является ли значение из поля булевым значением
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isBool: function(field, rule, value) {
            return typeof (value) === 'boolean';
        },

        /**
         * Правило проверяет, является ли значение из поля integer
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isInteger: function(field, rule, value) {
            return Number(value) === value && value % 1 === 0;
        },

        /**
         * Правило проверяет, является ли значение из поля double
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isFloat: function(field, rule, value) {
            return Number(value) === value && value % 1 !== 0;
        },

        /**
         * Аналог isNumeric в php, пропустит только строки с целыми числами
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isNumericInt: function (field, rule, value) {
            return !isNaN(parseFloat(value)) && isFinite(value) &&  value.indexOf('.') === -1;
        },

        /**
         * Проверка ИНН на числа
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isInn: function (field, rule, value) {
            return value.match(/^\d{10,12}/);
        },

        /**
         * Аналог isNumeric в php, пропустит строки с целыми и дробными числами
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        isNumericFloat: function (field, rule, value) {
            return !isNaN(parseFloat(value)) && isFinite(value);
        },

        /**
         * Если значение из поля строка, проверяется длина и сравнивается с переданным в правиле значением min
         * Если значение из поля число, оно сравнивается с переданным в правиле значением min
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        min: function(field, rule, value) {
            let min_value = 0;
            let result = false;

            if (typeof rule['min'] === 'object') {
                min_value = rule['min'][field];
            } else {
                min_value = rule['min'];
            }
            if (this.isString(field, rule, value)) {
                result = value.length >= min_value;
            }
            if (this.isInteger(field, rule, value) || this.isFloat(field, rule, value)) {
                result = String(value).length >= min_value;
            }

            if (!result) {
                this.error_list[field] = 'Минимальная длина поля - ' + min_value + ' символа(ов)';
            }
            return result;
        },

        /**
         * Если значение из поля строка, проверяется длина и сравнивается с переданным в правиле значением max
         * Если значение из поля число, оно сравнивается с переданным в правиле значением max
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         * @deprecated - используйте более простые maxInt и maxString
         */
        max: function(field, rule, value) {
            let max_value = 0;
            let result = false;

            if (typeof rule['max'] === 'object') {
                max_value = rule['max'][field];
            } else {
                max_value = rule['max'];
            }
            if (this.isString(field, rule, value)) {
                result = value.length <= max_value;
            }
            if (this.isInteger(field, rule, value) || this.isFloat(field, rule, value)) {
                result = value.toString().length <= max_value;
            }

            if (!result) {
                this.error_list[field] = 'Максимальная длина поля - ' + max_value + ' символа(ов)';
            }
            return result;
        },

        /**
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        maxInt: function(field, rule, value) {
            return value <= rule['max'];
        },

        /**
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        maxString: function(field, rule, value) {
            return value.length <= rule['max'];
        },

        /**
         * @param field
         * @param rule
         * @param value
         * @returns {boolean}
         */
        minInt: function(field, rule, value) {
            return value >= rule['min'];
        },

        /**
         * Проверяет, является ли переданное в поле значение датой
         * @param field
         * @param rule
         * @param value
         * @returns {*}
         */
        isDate: function(field, rule, value) {
            return moment(value, rule['format'], true).isValid();
        },

        /**
         * Проверяет, является ли переданное в поле значение датой после даты, переданной в параметрах правила
         * @param field
         * @param rule
         * @param value
         * @returns {*}
         */
        dateAfter: function(field, rule, value) {
            return moment(value, rule['format'], true).isAfter(moment(rule['date']).format(rule['format']));
        },

        /**
         * Проверяет, является ли переданное в поле значение датой перед датой, переданной в параметрах правила
         * @param field
         * @param rule
         * @param value
         * @returns {*}
         */
        dateBefore: function(field, rule, value) {
            return moment(value, rule['format'], true).isBefore(moment(rule['date']).format(rule['format']));
        },

        /**
         * Проверяет, является ли переданное в поле значение датой, равной переданной в параметрах правила
         * @param field
         * @param rule
         * @param value
         * @returns {*}
         */
        dateEqual: function(field, rule, value) {
            return moment(value, rule['format'], true).isSame(moment(rule['date']).format(rule['format']));
        },

        /**
         * Проверяет значение из поля на соответствие переданному регулярному выражению pattern
         * @param field
         * @param rule
         * @param value
         * @returns {*|boolean}
         */
        match: function(field, rule, value) {
            return this.isString(field, rule, value) && value.match(rule['pattern']);
        },

        /**
         * Валидация email
         * @param field
         * @param rule
         * @param value
         * @returns {*|boolean}
         */
        email: function(field, rule, value) {
            let re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
            return this.isString(field, rule, value) && value.match(re);
        },

        /**
         * Валидация телефона
         * @param field
         * @param rule
         * @param value
         * @returns {*|boolean}
         */
        phone: function(field, rule, value) {
            let re = /^[+]{1}[0-9]{1}[(]{1}[0-9]{3}[)]{1}[0-9]{3}[-]{1}[0-9]{2}[-]{1}[0-9]{2}$/;
            return this.isString(field, rule, value) && value.match(re);
        },

        /**
         * Валидация имени пользователя (Имя Фамилия, с тире и точками (Mr. Михаил Салтыков-Щедрин))
         * @param field
         * @param rule
         * @param value
         * @returns {*|boolean}
         */
        userName: function(field, rule, value) {
            let re = /^[a-zA-Z0-9\s]{2,100}$/u;
            return this.isString(field, rule, value) && value.match(re);
        },

        /**
         * Валидация названия фирмы с тире, кавычками и цифрами, в несколько слов
         * @param field
         * @param rule
         * @param value
         * @returns {*|boolean}
         */
        companyName: function(field, rule, value) {
            let re = /^[a-zA-Zа-яёА-ЯЁ0-9\s\-,"'№\.]+$/u;
            let only_digits = /^\d+$/;
            return this.isString(field, rule, value) && value.match(re) && !value.match(only_digits);
        },

        validateForm: function (config, form) {

            this.error_list = {};
            let validation_result = true;

            for (let rule in config) {
                let fields = config[rule].fields;
                for (let key in fields) {
                    let field = fields[key];
                    if (this.error_list[field]) {
                        continue;
                    }
                    let rule_options = config[rule];
                    let field_value = form[field];
                    if (rule === 'customValidation') { // кастомная валидация (будет вызван кастомный метод, который нужно определить в js-файле)
                        let customMethod = rule_options['method'];
                        if (!this[customMethod](rule_options['params'])) { // вызов метода кастомной валидации
                            validation_result = false;
                        }
                    } else { // стандартная валидация (будет вызван метод-правило из миксина, например min, dateEqual  и т.п.)
                        let need_validate_method = rule_options['needValidate'];
                        if (need_validate_method) { // существует опция needValidate - разрешающий валидацию по условию
                            if (!this[need_validate_method](field, field_value, rule_options['params'])) { // запускаем и проверяем, что вернул метод, переданный в опции needValidate
                                continue;
                            }
                        }
                        if (!this[rule](field, rule_options, field_value)) { // вызов метода-правила
                            validation_result = false;
                            if (rule_options['message']) {
                                this.error_list[field] = rule_options['message'];
                            }
                        }
                    }
                }
            }
            return validation_result; // true или false
        }
    }
};