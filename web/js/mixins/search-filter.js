/**
 * Миксин для настройки поисковой выдачи
 */
export const searchFilterMixins = {
    data: function () {
        return {
            filter_keys: {}, // Заголовки первичные и вторичные ключи
            filter_values: {}, // Созданные строки-фильтры
            options_filter_keys: {},   // Поля для select, чтобы выбрать первичный или вторичный ключ
            options_filter_values: {},   // Все возможные значения фильтра для заданных ключей
            new_key_field_id: '', // field_id нового ключа
            new_filter: {}, // Выбранные значения селекта нового фильтра ключ filed_id
            show_button_add_keys: false, // Показывать кнопки добавления ключей или нет
            is_adding_primary_key: false, // Нажата ли кнопка добавления первичного ключа
            is_adding_secondary_key: false, // Нажата ли кнопка добавления вторичного ключа
            duplicate_hash_filter: [], // Не уникальные хэшы при попытке удалить первичный ключ
            show_hint: false, // Показывать alert all_warning
            hint: null, // Хранит сообщение последнего warning_list['hint']
            is_loading_data: false, // Флаг для загрузки данных
            error_list: {},
            warning_list: {},
            /** Определить ссылки в компоненте, к которому будет примешан миксин */
            link_get_filter_keys_and_values: '',
            link_get_options_filter_keys: '',
            link_get_options_filter_values: '',
            link_add_filter_key: '',
            link_add_filter_row: '',
            link_safe_delete_filter_key: '',
            link_delete_filter_key: '',
            link_delete_filter_row: '',
            link_view_search_filter_rows: '',
        };
    },
    mounted() {
        let dataset = this.$refs.config.dataset;
        this._csrf = dataset.csrf;
        this.showChangedHint();
        this.loadData();
    },
    methods: {
        /** БЛОК загрузки данных на страницу,
         * при выполнение дейсвтий(добавление/удаление ключей и данных фильтров и т д) на странице
         */
        /** Выставляет подсказку и подгружает существующие фильтр-ключи и фильтр-строки и их возможные значения*/
        loadData:async function() {
            this.error_list = {};
            this.duplicate_hash_filter = {};
            this.is_loading_data = true;
            await this.getOptionsFilterKeys();
            if (!this.error_list['all_fields']) {
                await this.getOptionsFilterValues();
            }
            if (!this.error_list['all_fields']) {
                await this.getFilterKeysAndValues();
            }
            this.showChangedHint();
            this.hideSelectAddFilterKey();
            this.is_loading_data = false;
        },
        /** Список значений для селекта ключей*/
        getOptionsFilterKeys:async function() {
            const response = await axios.get(this.link_get_options_filter_keys)
            if (!response.data.is_success) {
                this.error_list = response.data.errors;
                this.show_button_add_keys = false;
            } else {
                this.options_filter_keys = response.data.content;
                if (!this.isEmptyOptionsFilterKeys()) {
                    this.show_button_add_keys = true;
                } else {
                    this.hideSelectAddFilterKey();
                    this.show_button_add_keys = false;
                }
            }
            this.warning_list = response.data.warnings;
        },
        /** Проверка this.options_filter_keys так как может прийти массивом или объектом */
        isEmptyOptionsFilterKeys() {
            return (Array.isArray(this.options_filter_keys) && this.options_filter_keys.length === 0) || !this.options_filter_keys;
        },
        /** Список значений для селекта значений*/
        getOptionsFilterValues:async function() {
            const response = await axios.get(this.link_get_options_filter_values)
            if (!response.data.is_success) {
                this.error_list = response.data.errors;
            } else {
                this.options_filter_values = response.data.content;
                for (let field_id in this.options_filter_values) {
                    this.$set(this.new_filter, field_id, null);
                    this.new_filter[field_id] = {'option_values': []};
                }
            }
            this.warning_list = response.data.warnings;
        },
        /** Список существующий фильтр-ключей и фильтр-строк */
        getFilterKeysAndValues:async function() {
            const response = await  axios.get(this.link_get_filter_keys_and_values);
            if (!response.data.is_success) {
                this.error_list = response.data.errors;
            }
            this.warning_list = response.data.warnings;
            this.filter_keys = response.data.content.filter_keys;
            this.filter_values = response.data.content.filter_values;
        },
        /** Показывать новую подсказку */
        showChangedHint: function() {
            if (this.isChangedHint()) {
                this.hint = this.warning_list['hint'];
                this.show_hint = true;
            }
        },
        /** Говорит о том что пришла новая подсказка из this.warning_list['hint']*/
        isChangedHint: function() {
            return this.warning_list['hint'] && this.hint !== this.warning_list['hint'];
        },
        /** Скрывать селекты добавления новых фильтр-ключей*/
        hideSelectAddFilterKey: function () {
            this.is_adding_primary_key = false;
            this.is_adding_secondary_key = false;
        },

        /** БЛОК Добавления/обновления/удаление фильтр-ключей */
        addFilterKey: function(key_type) {
            this.createSwal("Идет добавление нового ключа", "info", 300000);
            axios.post(this.link_add_filter_key, {_csrf:this._csrf, key_type: key_type, field_id: this.new_key_field_id})
                .then(response => {
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                        this.closeSwal(response);
                        let error_message = typeof(this.error_list['new_key']) === 'string' ? this.error_list['new_key'] : 'Ошибка добавление ключа!';
                        this.createSwal(error_message, "error", 2000);
                    } else {
                        this.new_key_field_id = '';
                        this.loadData();
                        this.closeSwal(response);
                        this.createSwal("Ключ успешно сохранен", "success", 3000);
                    }
                    this.$nextTick(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                });
        },
        safeDeleteKey: function(key) {
            axios.post(this.link_safe_delete_filter_key, {_csrf:this._csrf, field_id: key.field_id})
                .then(response => {
                    this.warning_list = response.data.warnings;
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                        this.duplicate_hash_filter = response.data.content.duplicate_hash_filter;
                    } else {
                        this.showDropDialogForKey(key);
                    }
                });
        },
        showDropDialogForKey(key) {
            swal({
                className: "drop-clients",
                title: "Удалить ключ?",
                text: "Удалённый ключ может повлиять на все настроенные фильтры",
                buttons: {
                    confirm: {
                        text: "Удалить",
                        value: true,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true
                    },
                    cancel: {
                        text: "Отмена",
                        value: null,
                        visible: true,
                        className: "btn btn-secondary",
                        closeModal: true,
                    }
                }
            }).then((willDelete) => {
                if (willDelete) {
                    this.deleteKey(key);
                }
            });

        },
        deleteKey: function(key) {
            axios.post(this.link_delete_filter_key, {_csrf:this._csrf, key_type: key.key_type, id: key.id})
                .then(response => {
                    this.warning_list = response.data.warnings;
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                    } else {
                        this.new_filter = {};
                        this.loadData();
                    }
                });
        },

        /** БЛОК Добавления/обновления/удаление фильтр-строк(фильтр-значений) */
        addFilterRow: function() {
            this.createSwal("Идет добавление нового фильтра", "info", 300000);
            axios.post(this.link_add_filter_row, {_csrf:this._csrf, filter: this.new_filter})
                .then(response => {
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                        if (!this.error_list['all_warning']) {
                            this.closeSwal(response);
                            let error_message = typeof(this.error_list['filter_new']) === 'string' ? this.error_list['filter_new'] : 'Ошибка добавление фильтра!';
                            this.createSwal(error_message, "error", 2000);
                            return true;
                        }
                    }
                    this.loadData();
                    this.closeSwal(response);
                    this.createSwal("Фильтр успешно сохранен", "success", 3000);
                });
        },
        updateFilterRow: function(index_row) {
            this.createSwal("Идет обновление фильтра", "info", 300000);
            axios.post(this.link_add_filter_row, {_csrf:this._csrf, filter: this.filter_values[index_row].data, old_hash_filter: this.filter_values[index_row].hash_filter, num_filter_row: index_row})
                .then(response => {
                    this.warning_list = response.data.warnings;
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                        this.closeSwal(response);
                        let error_message = typeof(this.error_list['filter_' + index_row]) === 'string' ? this.error_list['filter_' + index_row] : 'Ошибка обновления фильтра!';
                        this.createSwal(error_message, "error", 2000);
                    } else {
                        this.filter_values[index_row]['edit'] = false;
                        this.loadData();
                        this.closeSwal(response);
                        this.createSwal("Фильтр успешно обновлен", "success", 3000);
                    }
                });
        },
        showDropDialogForFilter(hash_filter) {
            swal({
                className: "drop-clients",
                title: "Удалить позицию?",
                text: "Удалённая информация не может быть восстановлена",
                buttons: {
                    confirm: {
                        text: "Удалить",
                        value: true,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true
                    },
                    cancel: {
                        text: "Отмена",
                        value: null,
                        visible: true,
                        className: "btn btn-secondary",
                        closeModal: true,
                    }
                }
            }).then((willDelete) => {
                if (willDelete) {
                    this.deleteFilter(hash_filter);
                }
            });

        },
        deleteFilter: function(hash_filter) {
            axios.post(this.link_delete_filter_row, {_csrf:this._csrf, hash_filter: hash_filter})
                .then(response => {
                    this.warning_list = response.data.warnings;
                    if (!response.data.is_success) {
                        this.error_list = response.data.errors;
                    } else {
                        this.loadData();
                    }
                });
        },
        /** БЛОК отображения поисковой выдачи по фильтру */
        viewSearchFilterRows: function (index_row) {
            if (!this.filter_values[index_row] || !this.filter_values[index_row]['hash_filter']) {
                return this.createSwal('Ошибка отображения подходящих строк под фильтр, обратитесь к администратору!' , "error", 2000);
            }
            window.open(this.link_view_search_filter_rows + '/?hash_filter=' + this.filter_values[index_row]['hash_filter'], '_blank');
        },

        /** БЛОК режим редактирования фильтр-строки */
        isEditableFilter: function(index_row) {
            return (this.filter_values[index_row] && this.filter_values[index_row].edit);
        },
        editFilter: function (index_row) {
            this.filter_values[index_row]['edit'] = true;
            this.filter_values[index_row].old_data = [];
            for(let i in this.filter_values[index_row]) {
                if (i === 'data') {
                    for(let j in this.filter_values[index_row][i]) {
                        this.filter_values[index_row].old_data[j] = (Object.assign({}, this.filter_values[index_row][i][j]));
                    }
                }
            }
        },
        cancelEditFilter: function (index_row) {
            this.filter_values[index_row]['edit'] = false;
            this.error_list = {};
            this.filter_values[index_row].data = this.filter_values[index_row].old_data;
        },

        /** БЛОК обертка над SweatAlert*/
        createSwal: function (title, icon_style, timer) {
            swal(title, {
                buttons: false,
                icon: icon_style,
                timer: timer,
            });
        },
        closeSwal: function() {
            swal.close();
        },

        /** БЛОК методов для Multiselect */
        /** Проверка что в Multiselect, есть выбранные options */
        hasSelectedOptions(value)
        {
            return value && value.length > 0;
        },
        /** Выводит в мультиселект сообщение, когда количество выбранных элементов привышает limit */
        limitText (count) {
            return `ещё +${count}`;
        },
        /** Удаляет(очищает) все выбранные options в Multiselect */
        clearOptions(index_row, field_id) {
            if (this.filter_values[index_row].data[field_id].option_values) {
                this.$set(this.filter_values[index_row].data[field_id], 'option_values', null);
            }
        },

        /** БЛОК-остальное для визуальщины, отображения/скрытия подсказок/кнопок/блоков */
        cleanDuplicateFilters(error_key) {
            this.error_list[error_key] = null;
            this.duplicate_hash_filter = {};
        },
        getFilterRowClass: function(index_row, is_warning) {
            if (is_warning) {
                return 'alert alert-warning text-nowrap not-border filter-row';
            }

            return index_row % 2 !== 0 ? 'bg-light text-nowrap not-border filter-row' : 'text-nowrap not-border filter-row';
        },
        showHint: function () {
            this.show_hint = !this.show_hint;
        },
        showSelectAddPrimaryKey: function () {
            this.is_adding_secondary_key = false;
            this.is_adding_primary_key = true;
        },
        showSelectAddSecondaryKey: function () {
            this.is_adding_primary_key = false;
            this.is_adding_secondary_key = true;
        },
        getErrorText: function(field) {
            $('[data-toggle="tooltip"]').tooltip();

            if (!this.error_list[field]) {
                return '';
            }
            if (typeof(this.error_list[field]) === 'string') {
                return this.error_list[field];
            }
            for (let model_field in this.error_list[field]) {
                return this.error_list[field][model_field][0] ? this.error_list[field][model_field][0] : this.error_list[field][model_field];
            }
            return '';
        },
    }
};