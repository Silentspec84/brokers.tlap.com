document.addEventListener("DOMContentLoaded", function() {
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    /**
     * MixIns which works with data lists in vue applications
     *
     * element which refers to vue entity must have
     *
     * Vue entity which include listMixins must have data section with:
     * this.error_list[]
     * this.data_list[]
     * this.get_link - load data link
     * this.edit_link - edit data link
     * this.drop_link - delete item link
     * this.switch_link - switch item link
     * this._csrf - token from reqired attribute [refs="config" data-csrf="<TOKEN>"]
     *
     * API response must have
     * - content[]|string
     * - errors []|string
     * - is_success (bool)
     *
     */
    listMixins = {
        methods: {
            getError: function (field, id = false) {
                var result = '';

                if (id === false && (field in this.error_list)) {
                    result = this.error_list[field];
                } else if (id in this.error_list && (field in this.error_list[id])) {
                    result = this.error_list[id][field];
                }
                $('[data-toggle="tooltip"]').tooltip();

                return result;
            },
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "" : "border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            },
            getWarning: function (field, id = false) {
                var result = '';
                if (id === false && (field in this.warning_list)) {
                    return this.warning_list[field];
                }
                if (id in this.warning_list && (field in this.warning_list[id])) {
                    return this.warning_list[id][field];
                }

                return result;
            },
            showDropDialog(id) {
                var self = this;

                swal({
                    className: "drop-clients",
                    title: "Удалить позицию?",
                    text: "Удалённая информация не может быть восстановлена",
                    buttons: {
                        confirm: {
                            text: "Удалить",
                            value: true,
                            visible: true,
                            className: "btn btn-danger",
                            closeModal: true
                        },
                        cancel: {
                            text: "Отмена",
                            value: null,
                            visible: true,
                            className: "btn btn-secondary",
                            closeModal: true,
                        }
                    }
                }).then((willDelete) => {
                    if (willDelete) {
                        self.dropItem(id);
                    }
                });

            },
            createSwal: function (title, icon_style, timer) {
                swal(title, {
                    buttons: false,
                    icon: icon_style,
                    timer: timer,
                });
            },
            closeSwal: function(response) {
                swal.close();
            },
            /**
             * Рендерит ячейку либо с разбивкой по блокам если в формате JSON
             * либо её содержимое
             *
             * @param value
             * @returns {*}
             */
            renderCell: function(value) {
                if (typeof value === 'string') {
                    try {
                        value = JSON.parse(value);
                    } catch(e) {
                        return value;
                    }
                }

                if (typeof value !== 'object') {
                    return value;
                }

                const result = this.getValuesWithCodes(value);
                // приводим результат к строке. Если больше одного элемента в массиве
                // ставим между ними знак +
                if (result.length >= 1) {
                    return result.join(" + ");
                }

                return '';
            },
        }
    };

    // Dropdown
    $(function () {
        var selectors = {
            block: '.dropdown',
            trigger: '[data-toggle="dropdown"]',
            menu: '.dropdown-menu'
        };

        $('body').on('click', function (event) {
            var $target = $(event.target);
            var $dropdownMenu = $target.closest(selectors.menu);
            var isDropdownMenu = Boolean($dropdownMenu.length);

            if (isDropdownMenu) {
                var $dropdown = $dropdownMenu.closest(selectors.block);
                var $dropdownTrigger = $dropdown.children(selectors.trigger);
                var isCustomClick = $dropdownTrigger.attr('data-trigger') === 'custom-click';

                if (isDropdownMenu && isCustomClick) {
                    event.stopPropagation();
                    event.preventDefault();
                }
            }
        });
    });

    // Tooltips
    $(function () {
        try {
            $('[data-toggle="tooltip"]').tooltip();
        }
        catch(e) {

        }
    });

    /**
     * Подключается на всех страницах.
     * Перехватывает ответ от ajax и выводит нотисы из API, если таковые были.
     */
    function renderApiNotices() {
            let notices = [];
            axios.interceptors.response.use(function (response) {
                    // Делаем что угодно с поступившими данными
                    if (response.data.notices) {
                        for (var key in response.data.notices) {
                            notices.push('<p>' + key + ' in '  + response.data.notices[key]['file'] + ':' + response.data.notices[key]['line'] + '</p>');
                        }
                    }
                    let html =  '<input class="hide" type="checkbox" id="exception_name_api">';
                        html += '<label class="d-block" for="exception_name_api">PHP Notice('+notices.length+')</label>';
                        html += '<div class="mb-2">' + notices.join("\n") + '</div>';
                    $('#api_notices').html(html);
                    return response;
                }, function (error) {
                    // Обрабатываем ошибку
                    return Promise.reject(error);
                });

    }
    renderApiNotices();
});