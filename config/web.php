<?php

use dektrium\user\Module;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name'=>'brokers.tlap.com',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@logs' => '@app/logs',
        '@uploads' => '@app/resources/uploads',
        '@web' => '@app/web',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => 'xVmLSTJIXk52nKA20Sh5LX5GDHly37j-',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '192.81.221.125',
                'username' => 'info@brokers.tlap.com',
                'password' => 'L4PUP2ebDt',
                'port' => '25',
                'encryption' => '',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 1,
            'targets' => [ // цели логов
                [
                    'class' => 'yii\log\FileTarget', // будем писать в файл
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['info'], // будем писать, когда вызван метод ifno
                    'categories' => ['application'], // будем писать для категории application (передается в метод вторым параметром, 'application' - значение по умолчанию)
                    'logFile' => '@app/logs/success.log', // будем писать в этот файл
                    'logVars' => [], // запишем переменные, перечисленные в этом массиве (например, можно положить сюда $_POST)
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['error', 'warning'],
                    'categories' => ['application'],
                    'logFile' => '@app/logs/error.log',
                    'logVars' => [],
                ],

                //deploy logs
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 10,  // по умолчанию 1000
                    'levels' => ['info'],
                    'categories' => ['deploy'],  // будем писать для категории deploy (если забыть передать категорию, возьмется значение 'application' и логи запишутся не так, как ожидали)
                    'logFile' => '@app/logs/deploy_success.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\EmailTarget', // будем слать email, можно еще писать в БД (yii\log\DbTarget) или писать в syslog (yii\log\SyslogTarget)
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['error', 'warning'],
                    'categories' => ['deploy'],
                    'message' => [
                        'from' => ['avtosmeta.dev@gmail.com'],
                        'to' => ['silentspec84@mail.ru'],
                        'subject' => 'Ошибка деплоя',
                    ],
                ],
            ],
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => 'facebook_client_id',
                    'clientSecret' => 'facebook_client_secret',
                ],
                'twitter' => [
                    'class'          => 'dektrium\user\clients\Twitter',
                    'consumerKey'    => 'CONSUMER_KEY',
                    'consumerSecret' => 'CONSUMER_SECRET',
                ],
                'vkontakte' => [
                    'class'        => 'dektrium\user\clients\VKontakte',
                    'clientId'     => 'vk_client_id',
                    'clientSecret' => 'vk_client_secret',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => 'google_client_id',
                    'clientSecret' => 'google_client_secret',
                ],
                'yandex' => [
                    'class'        => 'dektrium\user\clients\Yandex',
                    'clientId'     => 'yandex_client_id',
                    'clientSecret' => 'yandex_client_secret'
                ],
                'linkedin' => [
                    'class'        => 'dektrium\user\clients\LinkedIn',
                    'clientId'     => 'CLIENT_ID',
                    'clientSecret' => 'CLIENT_SECRET'
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                '/' => 'site/index',
                '<controller:[\w\-]+>/<id:\d+[/]*>' => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+[/]*>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+[/]*>' => '<controller>/<action>',
                '/user/settings/view/<id:\d+>' => '/user/settings/view/',
            ],
        ],
        'db' => $db,
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableFlashMessages' => true,
            'enableRegistration' => true,
            'enableConfirmation' => true,
            'enablePasswordRecovery' => true,
            'emailChangeStrategy' => Module::STRATEGY_SECURE,
            'confirmWithin' => 86400,
            'rememberFor' => 1209600,
            'recoverWithin' => 21600,
            'controllerMap' => [
                'confirm' => 'app\controllers\ConfirmController',
                'registration' => [
                    'class' => 'app\controllers\RegistrationController',
                    'on ' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
                        Yii::$app->response->redirect(array('/'))->send();
                        Yii::$app->end();
                    }
                ],
                'login' => [
                    'class' => \dektrium\user\controllers\SecurityController::className(),
                    'on ' . \dektrium\user\controllers\SecurityController::EVENT_AFTER_LOGIN => function ($e) {
                        Yii::$app->response->redirect(array('/'))->send();
                        Yii::$app->end();
                    }
                ],
            ],
            'modelMap' => [
                'User' => 'app\models\User',
                'LoginForm' => 'app\models\LoginForm',
                'RegistrationForm' => 'app\models\RegistrationForm',
                'RecoveryForm' => 'app\models\RecoveryForm',
                'Profile' => 'app\models\Profile',
                'SettingsForm' => 'app\models\SettingsForm',
            ],
            'mailer' => [
                'sender'                => ['info@brokers.tlap.com' => 'Брокеры tlap.com'],
                'welcomeSubject'        => 'Добро пожаловать!',
                'confirmationSubject'   => 'Подтверждение регистрации',
                'reconfirmationSubject' => 'Изменение email адреса',
                'recoverySubject'       => 'Восстановление пароля',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
