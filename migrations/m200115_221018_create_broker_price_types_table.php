<?php

use yii\db\Migration;

/**
 * m200115_221018_create_broker_price_types_table
 */
class m200115_221018_create_broker_price_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('broker_price_types', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'name' => $this->string(100)->notNull()->unique(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers-broker_price_type_id',
            'brokers',
            'broker_price_type_id',
            'broker_price_types',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('broker_price_types');
    }
}