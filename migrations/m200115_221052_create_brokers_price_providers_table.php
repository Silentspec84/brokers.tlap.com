<?php

use yii\db\Migration;

/**
 * m200115_221052_create_brokers_price_providers_table
 */
class m200115_221052_create_brokers_price_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_price_providers', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'price_provider_id' => $this->integer()->notNull(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_price_providers-broker_id',
            'brokers_price_providers',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_price_providers-price_provider_id',
            'brokers_price_providers',
            'price_provider_id',
            'price_providers',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_price_providers');
    }
}