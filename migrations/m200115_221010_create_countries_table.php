<?php

use yii\db\Migration;

/**
 * m200115_221010_create_countries_table
 */
class m200115_221010_create_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('countries', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'name_iso' => $this->string(100)->notNull()->unique(),
            'flag_img' => $this->string(100)->notNull()->unique(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('countries');
    }
}