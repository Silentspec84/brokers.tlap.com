<?php

use yii\db\Migration;

/**
 * m200115_221056_create_accounts_currencies_table
 */
class m200115_221056_create_accounts_currencies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('accounts_currencies', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer(),
            'currency_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-accounts_currencies-account_id',
            'accounts_currencies',
            'account_id',
            'brokers_account_types',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts_currencies-currency_id',
            'accounts_currencies',
            'currency_id',
            'currencies',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts_currencies');
    }
}