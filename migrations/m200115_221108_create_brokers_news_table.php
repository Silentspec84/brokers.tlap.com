<?php

use yii\db\Migration;

/**
 * m200115_221108_create_brokers_news_table
 */
class m200115_221108_create_brokers_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_news', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'user_id' => $this->integer(),
            'title' => $this->string(255)->notNull(),
            'text' => $this->text(),
            'image' => $this->string(255)->notNull(),
            'key_words' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_news-broker_id',
            'brokers_news',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_news-user_id',
            'brokers_news',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_news');
    }
}