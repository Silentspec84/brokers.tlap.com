<?php

use yii\db\Migration;

/**
 * m200115_221040_create_payment_systems_table
 */
class m200115_221040_create_payment_systems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('payment_systems', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'site' => $this->string(100)->notNull()->unique(),
            'logo' => $this->string(100)->notNull()->unique(),
            'description' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_systems');
    }
}