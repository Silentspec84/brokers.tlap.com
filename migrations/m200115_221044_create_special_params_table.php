<?php

use yii\db\Migration;

/**
 * m200115_221044_create_special_params_table
 */
class m200115_221044_create_special_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('special_params', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->unique(),
            'type' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('special_params');
    }
}