<?php

use yii\db\Migration;

/**
 * m200115_221020_create_brokers_offices_countries_table
 */
class m200115_221020_create_brokers_offices_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_offices_countries', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'office_type' => $this->integer(), // головной/представительство
            'office_address' => $this->string(255),
            'office_phone' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_offices_countries-broker_id',
            'brokers_offices_countries',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_offices_countries-country_id',
            'brokers_offices_countries',
            'country_id',
            'countries',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_offices_countries');
    }
}