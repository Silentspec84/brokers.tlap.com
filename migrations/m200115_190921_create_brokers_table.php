<?php

use yii\db\Migration;

/**
 * m200115_190921_create_brokers_table
 */
class m200115_190921_create_brokers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->unique(),
            'creation_year' => $this->string(20),
            'site' => $this->string(100),
            'broker_price_type_id' => $this->integer()->notNull(),
            'broker_logo' => $this->string(100),
            'partners_program' => $this->string(100),
            'description' => $this->text(),
            'address' => $this->string(255),
            'phone' => $this->string(255),
            'docs_link' => $this->string(255),
            'forum_link' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers');
    }
}