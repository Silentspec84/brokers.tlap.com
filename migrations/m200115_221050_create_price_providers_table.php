<?php

use yii\db\Migration;

/**
 * m200115_221050_create_price_providers_table
 */
class m200115_221050_create_price_providers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('price_providers', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'description' => $this->text()->notNull(),
            'logo' => $this->string(100)->notNull(),
            'country_id' => $this->integer()->notNull(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-price_providers-country_id',
            'price_providers',
            'country_id',
            'countries',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('price_providers');
    }
}