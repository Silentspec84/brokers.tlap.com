<?php

use yii\db\Migration;

/**
 * m200115_221055_create_accounts_trading_platforms_table
 */
class m200115_221055_create_accounts_trading_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('accounts_trading_platforms', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer(),
            'platform_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-accounts_trading_platforms-account_id',
            'accounts_trading_platforms',
            'account_id',
            'brokers_account_types',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts_trading_platforms-platform_id',
            'accounts_trading_platforms',
            'platform_id',
            'trading_platforms',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts_currencies');
    }
}