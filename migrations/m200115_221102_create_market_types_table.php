<?php

use yii\db\Migration;

/**
 * m200115_221102_create_market_types_table
 */
class m200115_221102_create_market_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('market_types', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'name' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('market_types');
    }
}