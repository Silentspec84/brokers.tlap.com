<?php

use yii\db\Migration;

/**
 * m200115_221106_create_accounts_currency_pairs_table
 */
class m200115_221106_create_accounts_currency_pairs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('accounts_currency_pairs', [
            'id' => $this->primaryKey(),
            'currency_pair_id' => $this->integer(),
            'account_id' => $this->integer(),
            'swap' => $this->string(255),
            'leverage' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-accounts_currency_pairs-currency_pair_id',
            'accounts_currency_pairs',
            'currency_pair_id',
            'currency_pairs',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts_currency_pairs-account_id',
            'accounts_currency_pairs',
            'account_id',
            'brokers_account_types',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts_currency_pairs');
    }
}