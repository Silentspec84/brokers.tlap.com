<?php


use app\models\Broker;
use app\models\BrokerAccountType;
use app\models\BrokerInvestingType;
use app\models\BrokerMobileDevice;
use app\models\BrokerOfficeCountry;
use app\models\BrokerRegulator;
use app\models\BrokerSiteLang;
use app\models\BrokerSupportLang;
use app\models\Country;
use app\models\Currency;
use app\models\InvestingType;
use app\models\Lang;
use app\models\MobileDevice;
use app\models\PriceType;
use app\models\Regulator;

class m200115_221112_create_tables_data
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $price_type_mm = new PriceType();
        $price_type_mm->name = 'MM';
        $price_type_mm->title = 'Market Maker';
        $price_type_mm->save();

        $price_type_ndd = new PriceType();
        $price_type_ndd->name = 'NDD';
        $price_type_ndd->title = 'No Dealing Desk';
        $price_type_ndd->save();

        $price_type_ecn = new PriceType();
        $price_type_ecn->name = 'ECN';
        $price_type_ecn->title = 'Electronic Communication Network';
        $price_type_ecn->save();

        $price_type_stp = new PriceType();
        $price_type_stp->name = 'STP';
        $price_type_stp->title = 'Straight-Through Processing';
        $price_type_stp->save();

        $price_type_dma = new PriceType();
        $price_type_dma->name = 'DMA';
        $price_type_dma->title = 'Direct Market Access';
        $price_type_dma->save();

        //https://www.artlebedev.ru/country-list/
        $country_russia = new Country();
        $country_russia->title = 'Россия';
        $country_russia->name_iso = 'RUS';
        $country_russia->flag_img = '';
        $country_russia->save();

        $country_blz = new Country();
        $country_blz->title = 'Белиз';
        $country_blz->name_iso = 'BLZ';
        $country_blz->flag_img = '';
        $country_blz->save();

        $country_blr = new Country();
        $country_blr->title = 'Беларусь';
        $country_blr->name_iso = 'BLR';
        $country_blr->flag_img = '';
        $country_blr->save();

        $country_mus = new Country();
        $country_mus->title = 'Маврикий';
        $country_mus->name_iso = 'MUS';
        $country_mus->flag_img = '';
        $country_mus->save();

        $country_vct = new Country();
        $country_vct->title = 'Сент-Винсент и Гренадины';
        $country_vct->name_iso = 'VCT';
        $country_vct->flag_img = '';
        $country_vct->save();

        $country_vct = new Country();
        $country_vct->title = 'Соединенное Королевство';
        $country_vct->name_iso = 'GBR';
        $country_vct->flag_img = '';
        $country_vct->save();

        $country_vct = new Country();
        $country_vct->title = 'Соединенное Королевство';
        $country_vct->name_iso = 'GBR';
        $country_vct->flag_img = '';
        $country_vct->save();

        $currency = new Currency();
        $currency->title = 'Доллар США';
        $currency->iso = 'USD';
        $currency->symbol = '$';
        $currency->save();

        $currency = new Currency();
        $currency->title = 'Евро';
        $currency->iso = 'EUR';
        $currency->symbol = '€';
        $currency->save();

        $currency = new Currency();
        $currency->title = 'Фунт стерлингов';
        $currency->iso = 'GBP';
        $currency->symbol = '£';
        $currency->save();

        $currency = new Currency();
        $currency->title = 'Российский рубль';
        $currency->iso = 'RUB';
        $currency->symbol = '₽';
        $currency->save();

        $currency = new Currency();
        $currency->title = 'Иена';
        $currency->iso = 'JPY';
        $currency->symbol = '¥';
        $currency->save();

        $lang = new Lang();
        $lang->title = 'Английский';
        $lang->iso = 'en';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Арабский';
        $lang->iso = 'ar';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Вьетнамский';
        $lang->iso = 'vi';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Индонезийский';
        $lang->iso = 'id';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Испанский';
        $lang->iso = 'es';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Китайский';
        $lang->iso = 'zh';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Персидский';
        $lang->iso = 'fa';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Португальский';
        $lang->iso = 'pt';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Русский';
        $lang->iso = 'ru';
        $lang->save();

        $lang = new Lang();
        $lang->title = 'Хинди';
        $lang->iso = 'hi';
        $lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 1;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 2;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 3;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 4;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 5;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 6;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 7;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 8;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 9;
        $broker_site_lang->save();

        $broker_site_lang = new BrokerSiteLang();
        $broker_site_lang->broker_id = 1;
        $broker_site_lang->lang_id = 10;
        $broker_site_lang->save();

        $office = new BrokerOfficeCountry();
        $office->broker_id = 1;
        $office->country_id = 1;
        $office->office_type = 10;
        $office->office_address = 'Россия , Москва, Ракетный бульвар, 16';
        $office->office_phone = '+7 (495) 648-64-37';
        $office->save();

        $office = new BrokerOfficeCountry();
        $office->broker_id = 1;
        $office->country_id = 4;
        $office->office_type = 20;
        $office->office_address = 'Маврикий , Ebene, The Catalyst, 40 Cybercity';
        $office->office_phone = '+44 8458 690980';
        $office->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 1;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 2;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 3;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 4;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 5;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 6;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 7;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 8;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 9;
        $broker_support_lang->save();

        $broker_support_lang = new BrokerSupportLang();
        $broker_support_lang->broker_id = 1;
        $broker_support_lang->lang_id = 10;
        $broker_support_lang->save();

        $regulator = new Regulator();
        $regulator->title = 'IFSC';
        $regulator->creation_year = '1999';
        $regulator->country_id = 2;
        $regulator->text = 'Основной целью Комиссии по международным финансовым услугам Белиза является обеспечение прозрачности и легитимности деятельности международных финансовых организаций, зарегистрированных на территории Белиза. Организация осуществляет контрольно-надзорные мероприятия в отношении провайдеров финансовых услуг, страховых компаний, пенсионных фондов, обществ с ограниченной ответственностью.';
        $regulator->description = 'Описание Комиссии по международным финансовым услугам Белиза - регулятора многих форекс брокеров';
        $regulator->keywords = 'Белиз регуляторы, форекс регуляторы, IFSC, Комиссия по международным финансовым услугам';
        $regulator->logo = '../img/regulators_logo/IFSC.png';
        $regulator->site = 'http://www.ifsc.gov.bz/';
        $regulator->email = 'info@ifsc.gov.bz';
        $regulator->tel = '(501) 822-29-74';
        $regulator->address = 'Sir Edney Cain Building, 2nd Floor,Бельмопан,Белиз';

        $broker_regulator = new BrokerRegulator();
        $broker_regulator->broker_id = 1;
        $broker_regulator->regulator_id = 1;
        $broker_regulator->save();

        $mobile_device = new MobileDevice();
        $mobile_device->title = 'iOS устройство';
        $mobile_device->save();

        $mobile_device = new MobileDevice();
        $mobile_device->title = 'Android';
        $mobile_device->save();

        $broker_mobile_device = new BrokerMobileDevice();
        $broker_mobile_device->broker_id = 1;
        $broker_mobile_device->mobile_device_id = 1;
        $broker_mobile_device->save();

        $broker_mobile_device = new BrokerMobileDevice();
        $broker_mobile_device->broker_id = 1;
        $broker_mobile_device->mobile_device_id = 2;
        $broker_mobile_device->save();

        $acc_type = new BrokerAccountType();
        $acc_type->broker_id = 1;
        $acc_type->title = 'Стандартный';
        $acc_type->type = 'standard';
        $acc_type->server_name = 'standard.mt4';
        $acc_type->min_depo = '100 $';
        $acc_type->stopout = '20 %';
        $acc_type->min_lot = '0.01 лотов';
        $acc_type->max_lot = '100 лотов';
        $acc_type->max_deals = '500';
        $acc_type->execution_tec = 'Instant Execution';
        $acc_type->swap_free = BrokerAccountType::NO_SWAP_FREE_TYPE;
        $acc_type->spread_type = BrokerAccountType::NON_FIX_SPREAD_TYPE;
        $acc_type->symbols_type = BrokerAccountType::FIVE_DIGITS_TYPE;
        $acc_type->margin_call = '80 %';
        $acc_type->execution_speed = '10 ms';
        $acc_type->commission = 'Нет';
        $acc_type->phone_dealing = BrokerAccountType::NO_PHONE_DEALING_TYPE;
        $acc_type->lock_margin = '50 %';
        $acc_type->percent_income = 'Нет';
        $acc_type->save();

        $invest_type = new InvestingType();
        $invest_type->title = 'PAMM';
        $invest_type->description = 'Percent Allocation Management Module';
        $invest_type->save();

        $invest_type = new InvestingType();
        $invest_type->title = 'LAMM';
        $invest_type->description = 'Lot allocation management module';
        $invest_type->save();

        $broker_invest_type = new BrokerInvestingType();
        $broker_invest_type->broker_id = 1;
        $broker_invest_type->investing_type_id = 1;
        $broker_invest_type->save();

        $alpari = new Broker();
        $alpari->title = 'Alpari';
        $alpari->creation_year = '1998 год';
        $alpari->site = 'www.alpari.com';
        $alpari->broker_price_type_id = 3;
        $alpari->broker_logo = '../img/broker_logo/Alpari.png';
        $alpari->partners_program = 'https://alpari.com/ru/company/partnership/';
        $alpari->description = '<div>
        <h2>
                            Обзор Альпари (Alpari)                    </h2>
        <div>
            <p>Компания Alpari&nbsp; - российский Форекс брокер, предоставляющий качественные брокерские услуги с 1998 года. 
            За время своего существования компания заслужила всеобщее признание и массу положительных отзывов от своих клиентов, 
            которые вы сможете почитать в разделе <strong>отзывы об Альпари</strong>. Кроме всего прочего Альпари вполне 
            обоснованно считается одним из главных инициаторов создания НАФД (ЦРФИН), ведущей отечественной ассоциации форекс дилеров.&nbsp;</p>
<p>Компания предлагает 2 метода котирования: стандартный и ECN. Стандартный метод котирования подразумевает вывод на рынок 
только совокупной позиции по всем счетам, при этом клиринг осуществляется внутри компании. Для счетов ECN все ордера клиентов 
автоматически выводятся на внешний рынок. Плюсом счета ECN является отсутствие реквот, а минусом – наличие комиссии.&nbsp;</p>
<p style="text-align: justify;">Следует отметить, что Альпари имеет три международные лицензии авторитетных финансовых регуляторов. Данная 
брокерская компания признана лучшим брокером по версии агентства «Интерфакс». Более детально с основными событиями из жизни 
рассматриваемого брокера Вы можете ознакомиться на этой странице. Alpari постоянно находится в процессе развития и стремится 
предложить своим клиентам максимально выгодные условия для действительно успешной торговли с большой прибылью.&nbsp;</p>
<p style="text-align: justify;">Компания Alpari предоставляет пользователям достаточно большое количество приятных бонусов. Среди них 
особого внимания заслуживает специальная программа лояльности Альпари Cashback, <strong>VIP-клуб для крупных клиентов</strong>, 
начисление процентов на депозит и т.д. Кроме этого данный брокер постоянно приятно радует пользователей специальными 
предложениями и акциями, ограниченными по времени. Многих трейдеров привлекают регулярные конкурсы и розыгрыши с ценными 
призами. Примечательно, что подобные мероприятия проводятся не только среди трейдеров, но и среди инвесторов и управляющих 
ПАММ-счетами.&nbsp;<em><br></em></p>
<p>Среди отличительных преимуществ компании можно отметить:</p>
<ul>
<li>минимальный размер счета 1$;</li>
<li>плавающий спред от 0.3 пункта;</li>
<li>кредитное плечо до 1:500;</li>
<li>скальпинг;</li>
<li>автоматическая торговля;</li>
<li>хеджинг;</li>
<li>большое количество удобных способов снятия/пополнения счета;</li>
<li>безупречный уровень обслуживания;</li>
<li>торговля в один клик.</li>
</ul>
<p>Начинающие трейдеры однозначно положительно смогут оценить возможность записаться и пройти курсы обучения торговле на рынке форекс. 
Важно подчеркнуть, что участие в большинстве семинаров является абсолютно бесплатным. Плюс ко всему Вашему вниманию представлено 
огромное количество тематической обучающей литературы и статей.</p>
<p>Особого внимания заслуживает Альпари&nbsp;аналитика, которую предоставляет своим клиентам компания Alpari. Так, например, Вы 
можете изучить свежие обзоры финансовых рынков от признанных профессионалов своего дела, воспользоваться торговыми сигналами, 
пообщаться и поделиться информацией с коллегами в чате трейдеров, воспользоваться календарем экономических событий и множеством 
других аналитических инструментов.&nbsp;</p>
<p>Компания предлагает своим клиентам удобные торговые платформы MetaTrader 4 и MetaTrader 5, а также веб-платформу OptionTrader. 
В качестве торговых инструментов доступны 62 валютные пары, металлы спот, CFD и бинарные опционы. Если Вы хотите инвестировать 
собственные средства, то стоит отметить, что Alpari поддерживает сервис ПАММ-счетов. Благодаря большому количеству действительно 
надежных управляющих Вы имеете возможность преумножить собственные вложения.</p>        </div>
    </div>';
        $alpari->address = 'Россия , Москва, Ракетный бульвар, 16';
        $alpari->phone = '+7 (495) 648-64-37';
        $alpari->docs_link = 'https://alpari.com/ru/company/regulatory_documents/';
        $alpari->forum_link = 'http://tlap.com/forum/foreks-brokery/16/alpari-limited-ru/639/';
        $alpari->save();


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}