<?php

use yii\db\Migration;

/**
 * m200115_221048_create_accounts_special_params_table
 */
class m200115_221048_create_accounts_special_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('accounts_special_params', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer(),
            'special_param_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-accounts_special_params-account_id',
            'accounts_special_params',
            'account_id',
            'brokers_account_types',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-accounts_special_params-special_param_id',
            'accounts_special_params',
            'special_param_id',
            'special_params',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts_special_params');
    }
}