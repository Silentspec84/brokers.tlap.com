<?php

use yii\db\Migration;

/**
 * m200115_221032_create_brokers_mobile_devices_table
 */
class m200115_221032_create_brokers_mobile_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_mobile_devices', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'mobile_device_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_mobile_devices-broker_id',
            'brokers_mobile_devices',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_mobile_devices-mobile_device_id',
            'brokers_mobile_devices',
            'mobile_device_id',
            'mobile_devices',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_mobile_devices');
    }
}