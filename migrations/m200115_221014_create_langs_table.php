<?php

use yii\db\Migration;

/**
 * m200115_221014_create_langs_table
 */
class m200115_221014_create_langs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('langs', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'iso' => $this->string(100)->notNull()->unique(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('langs');
    }
}