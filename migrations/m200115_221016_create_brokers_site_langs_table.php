<?php

use yii\db\Migration;

/**
 * m200115_221016_create_brokers_site_langs_table
 */
class m200115_221016_create_brokers_site_langs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_site_langs', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_site_langs-broker_id',
            'brokers_site_langs',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_site_langs-lang_id',
            'brokers_site_langs',
            'lang_id',
            'langs',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_site_langs');
    }
}