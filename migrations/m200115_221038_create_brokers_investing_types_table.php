<?php

use yii\db\Migration;

/**
 * m200115_221038_create_brokers_investing_types_table
 */
class m200115_221038_create_brokers_investing_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_investing_types', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'investing_type_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_investing_types-broker_id',
            'brokers_investing_types',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_investing_types-investing_type_id',
            'brokers_investing_types',
            'investing_type_id',
            'investing_types',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_investing_types');
    }
}