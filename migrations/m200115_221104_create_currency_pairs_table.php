<?php

use yii\db\Migration;

/**
 * m200115_221104_create_currency_pairs_table
 */
class m200115_221104_create_currency_pairs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('currency_pairs', [
            'id' => $this->primaryKey(),
            'symbol' => $this->string(100),
            'title' => $this->string(255),
            'market_type_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-currency_pairs-market_type_id',
            'currency_pairs',
            'market_type_id',
            'market_types',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency_pairs');
    }
}