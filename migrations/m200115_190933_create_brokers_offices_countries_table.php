<?php

use yii\db\Migration;

/**
 * m200115_190933_create_brokers_offices_countries_table
 */
class m200115_190933_create_brokers_offices_countries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_offices_countries', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'country_id' => $this->integer(),
            'office_type' => $this->integer(), // головной/представительство
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_offices_countries');
    }
}