<?php

use yii\db\Migration;

/**
 * m200115_221028_create_brokers_regulators_table
 */
class m200115_221028_create_brokers_regulators_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_regulators', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'regulator_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_regulators-broker_id',
            'brokers_regulators',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_regulators-regulator_id',
            'brokers_regulators',
            'regulator_id',
            'regulators',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_regulators');
    }
}