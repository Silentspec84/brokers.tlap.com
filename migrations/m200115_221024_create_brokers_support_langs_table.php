<?php

use yii\db\Migration;

/**
 * m200115_221024_create_brokers_support_langs_table
 */
class m200115_221024_create_brokers_support_langs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_support_langs', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_support_langs-broker_support_id',
            'brokers_support_langs',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_support_langs-lang_id',
            'brokers_support_langs',
            'lang_id',
            'langs',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_support_langs');
    }
}