<?php

use yii\db\Migration;

/**
 * m200115_221042_create_brokers_payment_systems_table
 */
class m200115_221042_create_brokers_payment_systems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_payment_systems', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'payment_system_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_payment_systems-broker_id',
            'brokers_payment_systems',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_payment_systems-payment_system_id',
            'brokers_payment_systems',
            'payment_system_id',
            'payment_systems',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_payment_systems');
    }
}