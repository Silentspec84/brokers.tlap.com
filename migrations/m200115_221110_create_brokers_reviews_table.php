<?php

use yii\db\Migration;

/**
 * m200115_221110_create_brokers_reviews_table
 */
class m200115_221110_create_brokers_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_reviews', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'user_id' => $this->integer(),
            'rating' => $this->integer(),
            'comment' => $this->text(),
            'pluses' => $this->text(),
            'minuses' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_reviews-broker_id',
            'brokers_reviews',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_reviews-user_id',
            'brokers_reviews',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_reviews');
    }
}