<?php

use yii\db\Migration;

/**
 * m200115_221046_create_brokers_special_params_table
 */
class m200115_221046_create_brokers_special_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_special_params', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'special_param_id' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_special_params-broker_id',
            'brokers_special_params',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-brokers_special_params-special_param_id',
            'brokers_special_params',
            'special_param_id',
            'special_params',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_special_params');
    }
}