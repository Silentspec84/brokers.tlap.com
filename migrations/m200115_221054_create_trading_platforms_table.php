<?php

use yii\db\Migration;

/**
 * m200115_221054_create_trading_platforms_table
 */
class m200115_221054_create_trading_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('trading_platforms', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'description' => $this->text()->notNull(),
            'logo' => $this->string(100)->notNull(),
            'year' => $this->string(100)->notNull(),
            'full_price' => $this->string(100)->notNull(),
            'month_price' => $this->string(100)->notNull(),
            'site' => $this->string(100)->notNull(),
            'screen' => $this->string(255)->notNull(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('price_providers');
    }
}