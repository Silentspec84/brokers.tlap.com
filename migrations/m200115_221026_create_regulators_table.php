<?php

use yii\db\Migration;

/**
 * m200115_221026_create_regulators_table
 */
class m200115_221026_create_regulators_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('regulators', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->unique(),
            'creation_year' => $this->string(20),
            'country_id' => $this->integer(),
            'text' => $this->text(),
            'description' => $this->string(255),
            'keywords' => $this->string(255),
            'logo' => $this->string(255),
            'site' => $this->string(100),
            'email' => $this->string(100),
            'tel' => $this->string(100),
            'address' => $this->string(100),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-regulators-country_id',
            'regulators',
            'country_id',
            'countries',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('regulators');
    }
}