<?php

use yii\db\Migration;

/**
 * m200115_221034_create_brokers_account_types_table
 */
class m200115_221034_create_brokers_account_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('brokers_account_types', [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer(),
            'title' => $this->string(100)->notNull(),
            'type' => $this->string(100)->notNull(),
            'server_name' => $this->string(100)->notNull(),
            'min_depo' => $this->string(100)->notNull(),
            'stopout' => $this->string(100)->notNull(),
            'min_lot' => $this->string(100)->notNull(),
            'max_lot' => $this->string(100)->notNull(),
            'max_deals' => $this->string(100)->notNull(),
            'execution_tec' => $this->string(100)->notNull(),
            'swap_free' => $this->integer(),
            'spread_type' => $this->integer(),
            'symbols_type' => $this->integer(),
            'margin_call' => $this->string(100),
            'execution_speed' => $this->string(100),
            'commission' => $this->string(100),
            'phone_dealing' => $this->integer(),
            'lock_margin' => $this->string(100),
            'percent_income' => $this->string(100),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-brokers_account_types-broker_id',
            'brokers_account_types',
            'broker_id',
            'brokers',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('brokers_account_types');
    }
}