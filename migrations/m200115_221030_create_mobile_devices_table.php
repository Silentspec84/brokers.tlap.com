<?php

use yii\db\Migration;

/**
 * m200115_221030_create_mobile_devices_table
 */
class m200115_221030_create_mobile_devices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('mobile_devices', [
            'id' => $this->primaryKey(),
            'title' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('mobile_devices');
    }
}